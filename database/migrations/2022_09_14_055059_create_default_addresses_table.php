<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('default_addresses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->text('address');
            $table->char('town')->nullable();
            $table->char('province')->nullable();
            $table->text('postal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('default_addresses');
    }
};
