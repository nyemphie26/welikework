<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class AboutUsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            [
                'setting_name' => 'Contact Image' ,
                'setting_link' => 'contact_img',
                'value'        => '',
                'group'        => 'about'
            ],
            [
                'setting_name' => 'Contact Title' ,
                'setting_link' => 'contact_title',
                'value'        => 'Contact Information',
                'group'        => 'about'
            ],
            [
                'setting_name' => 'Contact Desc' ,
                'setting_link' => 'contact_desc',
                'value'        => 'Our Team always get back to you within 24 hours.',
                'group'        => 'about'
            ],
            [
                'setting_name' => 'Contact Phone' ,
                'setting_link' => 'contact_phone',
                'value'        => '(+62)555-3456',
                'group'        => 'about'
            ],
            [
                'setting_name' => 'Contact Mail' ,
                'setting_link' => 'contact_mail',
                'value'        => 'contact@welikework.ca',
                'group'        => 'about'
            ],
            [
                'setting_name' => 'Contact Location' ,
                'setting_link' => 'contact_loc',
                'value'        => '123 Main st. Amherst, Canada.',
                'group'        => 'about'
            ],
            [
                'setting_name' => 'About Title' ,
                'setting_link' => 'about_title',
                'value'        => 'About Title',
                'group'        => 'about'
            ],
            [
                'setting_name' => 'About Sub Title' ,
                'setting_link' => 'about_subtitle',
                'value'        => 'About Title',
                'group'        => 'about'
            ],
            [
                'setting_name' => 'About Paragraph' ,
                'setting_link' => 'about_paragraph',
                'value'        => Str::random(200),
                'group'        => 'about'
            ],
        ];
        
        Setting::insert($settings);
    }
}
