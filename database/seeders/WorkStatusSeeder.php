<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class WorkStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            [
                'status' => 'Incoming'
            ],
            [
                'status' => 'Accepted'
            ],
            [
                'status' => 'Pending'
            ],
            [
                'status' => 'Rejected'
            ],
            [
                'status' => 'On Duty'
            ],
            [
                'status' => 'Rescheduled'
            ],
            [
                'status' => 'Finish'
            ],
        ];

        Status::insert($statuses);
    }
}
