<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SiteSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::truncate();

        $settings = [
            [
                'setting_name' => 'Site Title' ,
                'setting_link' => 'site_title',
                'value'        => 'Site Title',
                'group'        => 'header'
            ],
            [
                'setting_name' => 'Image Header' ,
                'setting_link' => 'site_img_header',
                'value'        => '',
                'group'        => 'header'
            ],
            [
                'setting_name' => 'Hero Title' ,
                'setting_link' => 'site_hero_title',
                'value'        => 'Hero Title',
                'group'        => 'header'
            ],
            [
                'setting_name' => 'Hero Desc' ,
                'setting_link' => 'site_hero_desc',
                'value'        => 'Hero Desc',
                'group'        => 'header'
            ],
            [
                'setting_name' => '1st Features Title' ,
                'setting_link' => 'site_1stfeatures_title',
                'value'        => '1st Features Title',
                'group'        => 'features'
            ],
            [
                'setting_name' => '1st Features Description' ,
                'setting_link' => 'site_1stfeatures_desc',
                'value'        => '1st Features Description',
                'group'        => 'features'
            ],
            [
                'setting_name' => '2nd Features Title' ,
                'setting_link' => 'site_2ndfeatures_title',
                'value'        => '2nd Features Title',
                'group'        => 'features'
            ],
            [
                'setting_name' => '2nd Features Description' ,
                'setting_link' => 'site_2ndfeatures_desc',
                'value'        => '2nd Features Description',
                'group'        => 'features'
            ],
            [
                'setting_name' => '3rd Features Title' ,
                'setting_link' => 'site_3rdfeatures_title',
                'value'        => '3rd Features Title',
                'group'        => 'features'
            ],
            [
                'setting_name' => '3rd Features Description' ,
                'setting_link' => 'site_3rdfeatures_desc',
                'value'        => '3rd Features Description',
                'group'        => 'features'
            ],
            [
                'setting_name' => '4th Features Title' ,
                'setting_link' => 'site_4thfeatures_title',
                'value'        => '4th Features Title',
                'group'        => 'features'
            ],
            [
                'setting_name' => '4th Features Description' ,
                'setting_link' => 'site_4thfeatures_desc',
                'value'        => '4th Features Description',
                'group'        => 'features'
            ],
            [
                'setting_name' => 'Card Image' ,
                'setting_link' => 'site_card_img',
                'value'        => '',
                'group'        => 'features'
            ],
            [
                'setting_name' => 'Card Title' ,
                'setting_link' => 'site_card_title',
                'value'        => 'Card Title',
                'group'        => 'features'
            ],
            [
                'setting_name' => 'Card Desc' ,
                'setting_link' => 'site_card_desc',
                'value'        => 'Card Desc',
                'group'        => 'features'
            ],
            [
                'setting_name' => '1st Counter Value' ,
                'setting_link' => 'site_1st_counter_val',
                'value'        => '9999',
                'group'        => 'counter'
            ],
            [
                'setting_name' => '1st Counter Title' ,
                'setting_link' => 'site_1st_counter_title',
                'value'        => '1st Counter Title',
                'group'        => 'counter'
            ],
            [
                'setting_name' => '1st Counter Desc' ,
                'setting_link' => 'site_1st_counter_desc',
                'value'        => '1st Counter Desc',
                'group'        => 'counter'
            ],
            [
                'setting_name' => '2nd Counter Value' ,
                'setting_link' => 'site_2nd_counter_val',
                'value'        => '9999',
                'group'        => 'counter'
            ],
            [
                'setting_name' => '2nd_ Counter Title' ,
                'setting_link' => 'site_2nd_counter_title',
                'value'        => '2nd_ Counter Title',
                'group'        => 'counter'
            ],
            [
                'setting_name' => '2nd_ Counter Desc' ,
                'setting_link' => 'site_2nd_counter_desc',
                'value'        => '2nd_ Counter Desc',
                'group'        => 'counter'
            ],
            [
                'setting_name' => '3rd Counter Value' ,
                'setting_link' => 'site_3rd_counter_val',
                'value'        => '9999',
                'group'        => 'counter'
            ],
            [
                'setting_name' => '3rd_ Counter Title' ,
                'setting_link' => 'site_3rd_counter_title',
                'value'        => '3rd_ Counter Title',
                'group'        => 'counter'
            ],
            [
                'setting_name' => '3rd_ Counter Desc' ,
                'setting_link' => 'site_3rd_counter_desc',
                'value'        => '3rd_ Counter Desc',
                'group'        => 'counter'
            ],
            [
                'setting_name' => 'Footer Image' ,
                'setting_link' => 'site_footer_img',
                'value'        => '',
                'group'        => 'footer'
            ],
        ];
        
        Setting::insert($settings);
    }
}
