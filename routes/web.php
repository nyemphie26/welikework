<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {\

Route::get('/', [App\Http\Controllers\LandingPageController::class, 'index'])->name('landingPage');
Route::get('/about', [App\Http\Controllers\LandingPageController::class, 'about'])->name('aboutUs');

Route::get('/login', function () {
    return view('signin');
});
Route::get('/register', function () {
    return view('signup');
})->name('Sign Up');


Auth::routes();

Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');
Route::get('/dashboard/{work}', [App\Http\Controllers\DashboardController::class, 'show'])->name('details');
Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'index'])->name('profile.index');
Route::put('/profile/{user}', [App\Http\Controllers\ProfileController::class, 'update'])->name('profile.update');
Route::put('/profile/{user}/address', [App\Http\Controllers\ProfileController::class, 'updateAddress'])->name('profile.address');
Route::put('/profile/{user}/password', [App\Http\Controllers\ProfileController::class, 'updatePassword'])->name('profile.updatePassword');
// Route::resource('profile', App\Http\Controllers\ProfileController::class )->only('index','update');
Route::get('/workImages/{id}', [\App\Http\Controllers\WorkImagesController::class, 'show'])->name('workImages.show');
Route::put('/workImages/{id}', [\App\Http\Controllers\WorkImagesController::class, 'update'])->name('workImages.update');
Route::delete('/workImages', [\App\Http\Controllers\WorkImagesController::class, 'destroy'])->name('workImages.destroy');

Route::resource('works', App\Http\Controllers\WorkController::class )->except('show','edit');
Route::get('works/accepted', [App\Http\Controllers\WorkController::class, 'accepted'])->name('works.accepted');
Route::get('works/finished', [App\Http\Controllers\WorkController::class, 'finishedWorks'])->name('works.finishedWorks');
Route::get('works/{id}', [App\Http\Controllers\WorkController::class, 'getDetails'])->name('getDetails'); //used by all to show details modal
Route::get('/works/fetch-service/{id}', [App\Http\Controllers\WorkController::class, 'fetchService']); //used by admin and cust to req new works

Route::get('/sendMail', [App\Http\Controllers\DashboardController::class, 'sendEmail']); //test sending new Work to admin
Route::get('/assignEmail', [App\Http\Controllers\DashboardController::class, 'assignEmail']); //test sending assigned Work to employee


Route::group(['middleware' => ['role:admin|employee']], function(){
    Route::get('/works/getSavedAddress/{id}', [App\Http\Controllers\WorkController::class, 'getSavedAddress']);
    Route::get('/works/status/{id}', [App\Http\Controllers\WorkController::class, 'changeWorkStatus'])->name('works.status');
    Route::put('/works/{id}/edit', [App\Http\Controllers\WorkController::class, 'edit'])->name('works.edit');
    Route::put('/works/{id}/finish', [App\Http\Controllers\WorkController::class, 'finish'])->name('works.finish'); //used by employee to finish works
    Route::post('/works/{id}/add-photo', [App\Http\Controllers\WorkController::class, 'addPhoto']); 

    Route::get('/exportToday', [App\Http\Controllers\DashboardController::class, 'exportToday'])->name('export.today');
    Route::get('/exportWork/{id}', [App\Http\Controllers\DashboardController::class, 'exportWork'])->name('export.work');
});

Route::group(['middleware' => ['role:admin']], function () {

    Route::get('/landing', [App\Http\Controllers\SettingController::class, 'index'])->name('admin.landing');
    Route::post('/landing', [App\Http\Controllers\SettingController::class, 'update'])->name('admin.landing.update');
    Route::post('/landing/service', [App\Http\Controllers\SettingController::class, 'storeService'])->name('admin.service.store');
    Route::get('/landing/service/{id}', [App\Http\Controllers\SettingController::class, 'edit'])->name('admin.service.edit');
    Route::post('/landing/service/{id}', [App\Http\Controllers\SettingController::class, 'updateService'])->name('admin.service.update');
    // Route::post('/landing/service/{id}', [App\Http\Controllers\SettingController::class, 'updateService'])->name('admin.service.update');
    Route::delete('/landing/{id}', [App\Http\Controllers\SettingController::class, 'destroy'])->name('admin.service.delete');
    
    Route::resource('services', App\Http\Controllers\ServiceController::class );
    Route::post('/service', [App\Http\Controllers\ServiceController::class, 'storeCategory'])->name('categories.store');
    Route::post('/service/{id}', [App\Http\Controllers\ServiceController::class, 'updateCategory'])->name('categories.update');
    // Route::put('/service', [App\Http\Controllers\ServiceController::class, 'update'])->name('admin.service.update');
    
    // Route::resource('employees', App\Http\Controllers\EmployeeController::class);
    Route::get('/employees', [App\Http\Controllers\EmployeeController::class, 'index'])->name('employees.index');
    Route::post('/employees', [App\Http\Controllers\EmployeeController::class, 'store'])->name('employees.store');
    Route::put('/employees/{id}', [App\Http\Controllers\EmployeeController::class, 'update'])->name('employees.update');
    Route::get('/employees/status/{id}', [App\Http\Controllers\EmployeeController::class, 'changeStatus'])->name('employees.status');
    
    Route::get('/customers', [App\Http\Controllers\CustomersController::class, 'index'])->name('customers.index');
    Route::post('/customers', [App\Http\Controllers\CustomersController::class, 'store'])->name('customers.store');
    Route::put('/customers/{id}', [App\Http\Controllers\CustomersController::class, 'update'])->name('customers.update');

    Route::get('/reporting', [App\Http\Controllers\ReportController::class, 'index'])->name('admin.reporting');
    Route::get('/reporting/{start}/{end}', [App\Http\Controllers\ReportController::class, 'getReport']);
    Route::get('/export/{start}/{end}', [App\Http\Controllers\ReportController::class, 'exportExcel']);
});

Route::get('migrate',function(){
    Artisan::call('migrate', ['--force' => true]);
});

Route::get('rollback',function(){
    Artisan::call('migrate:rollback', ['--force' => true]);
 });

 Route::get('reboot',function(){
    Artisan::call('view:clear', ['--force' => true]);
    Artisan::call('route:clear', ['--force' => true]);
    Artisan::call('config:clear', ['--force' => true]);
    Artisan::call('cache:clear', ['--force' => true]);
    Artisan::call('key:generate', ['--force' => true]);
  });

  Route::get('seed', function(){
    Artisan::call('db:seed', array('--class' => 'PermissionsAssign', '--force' => true));
  });