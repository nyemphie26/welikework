<nav
    class="navbar navbar-expand-lg navbar-dark bg-gradient-dark z-index-3 py-3">
    <div class="container">
      <a class="navbar-brand text-white d-lg-block d-none" href="/" rel="tooltip" title="Home Page">
        <i class="material-icons">home</i>
      </a>
      <a class="navbar-brand text-white d-lg-block d-none" href="/dashboard" rel="tooltip" title="My Dashboard" data-placement="bottom" target="_blank">
        {{ $show['site_title']->value }}
      </a>
      <a class="navbar-brand text-white d-lg-block d-none mx-auto" href="/about" rel="tooltip" title="About Us" data-placement="bottom">
        About Us
      </a>
      <a class="mx-auto"></a>
      <button class="navbar-toggler shadow-none ms-2" type="button" data-bs-toggle="collapse" data-bs-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon mt-2">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </span>
      </button>
      <div class="collapse navbar-collapse w-100 pt-3 pb-2 py-lg-0" id="navigation">
        <ul class="navbar-nav d-lg-none d-block">
          <li class="nav-item mx-2">
            <a href="/" class="btn btn-sm mb-0 me-1 mt-2 mt-md-0">
              <p class="d-inline text-sm z-index-1 text-white font-weight-bold">Home</p>
            </a>
          </li>
          <li class="nav-item mx-2">
            <a href="/dashboard" class="btn btn-sm mb-0 me-1 mt-2 mt-md-0">
              <p class="d-inline text-sm z-index-1 text-white font-weight-bold">{{ $show['site_title']->value }}</p>
            </a>
          </li>
          <li class="nav-item mx-2">
            <a href="/about" class="btn btn-sm mb-0 me-1 mt-2 mt-md-0">
              <p class="d-inline text-sm z-index-1 text-white font-weight-bold">About Us</p>
            </a>
          </li>
        </ul>
        <ul class="navbar-nav d-lg-block d-none mx-auto">
          <li class="nav-item mx-2">
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-hover">
          <li class="nav-item mx-2">
            <a href="/#register" class="btn btn-sm mb-0 me-1 mt-2 mt-md-0">
              <p class="d-inline text-sm z-index-1 text-white font-weight-bold">Register</p>
            </a>
          </li>
          <li class="nav-item mx-2">
            <a href="/login" class="btn btn-sm mb-0 me-1 mt-2 mt-md-0">
              <p class="d-inline text-sm z-index-1 text-white font-weight-bold">Login</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>