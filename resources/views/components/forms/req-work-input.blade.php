<div>
    <div class="row">
        <div class="col-lg-6">
          <div class="input-group input-group-static mb-4">
            <label for="service" class="ms-0">Service Category</label>
            <select class="form-control" id="categories" name="category_id" required>
                <option value="">-- Select Service Category --</option>
                @foreach ($categories as $item)
                    <option value="{{ $item->id }}">{{ $item->category_name }}</option>
                @endforeach
            </select>
          </div>
          <div class="input-group input-group-static mb-4">
            <label for="service" class="ms-0">Service Type</label>
            <select class="form-control" id="service" name="service_id">
            </select>
          </div>
          <div class="input-group input-group-static my-3">
            <label for="req_date">Request Date</label>
            <input type="datetime-local" class="form-control" name="req_date" id="req_date">
          </div>
          <div class="input-group input-group-static">
            <textarea class="form-control" rows="3" placeholder="Customer's Notes" spellcheck="false" name="cust_notes" id="cust_notes"></textarea>
          </div>
          <div class="input-group input-group-static">
            <textarea class="form-control" rows="3" placeholder="Known Preference" spellcheck="false" name="known_pref" id="konwn_pref"></textarea>
          </div>
        </div>
        <div class="col-lg-6">
          <ul class="list-group">
            <li class="list-group-item border-0 p-4 mb-2 bg-gray-100 border-radius-lg">
            <div class="form-check use-default">
              <input class="form-check-input" type="checkbox" name="defaultAddress" id="defaultAddress" value="usingDefaultAddressforPick">
              <label class="form-check-label" for="defaultAddress">
                Use Default Address
              </label>
            </div>
            <div class="form-check save-default" style="display: none">
              <input class="form-check-input" type="checkbox" name="saveAddress" id="saveAddress">
              <label class="form-check-label" for="saveAddress">
                Save as Default Address
              </label>
            </div>
              <div class="input-group input-group-static mb-4">
                <label  for="pickUpLoc">Works / Pick Up Address</label>
                <input type="text" class="form-control pickUp" name="pickUpLoc" id="pickUpLoc">
              </div>
              <div class="input-group input-group-static mb-4">
                <label  for="pickUpTown">Town</label>
                <input type="text" class="form-control pickUp" name="pickUpTown" id="pickUpTown">
              </div>
              <div class="input-group input-group-static mb-4">
                <label  for="pickUpProv">Province</label>
                <input type="text" class="form-control pickUp" name="pickUpProv" id="pickUpProv">
              </div>
              <div class="input-group input-group-static mb-4">
                <label  for="pickUpPostal">Postal Code</label>
                <input type="text" class="form-control pickUp" name="pickUpPostal" id="pickUpPostal">
              </div>
            </li>
            <li class="list-group-item border-0 p-4 mb-2 bg-gray-100 border-radius-lg drop-off" style="display: none">
              <div class="form-check use-default">
                <input class="form-check-input" type="checkbox" name="defaultAddress2" id="defaultAddress2" value="usingDefaultAddressforDrop">
                <label class="form-check-label" for="defaultAddress2">
                  Use Default Address
                </label>
              </div>
              <div class="form-check save-default" style="display: none">
                <input class="form-check-input" type="checkbox" name="saveAddress2" id="saveAddress2">
                <label class="form-check-label" for="saveAddress2">
                  Save as Default Address
                </label>
              </div>
              <div class="input-group input-group-static mb-4">
                <label  for="dropOffLoc">Drop Off Location</label>
                <input type="text" class="form-control drop-off" name="dropOffLoc" id="dropOffLoc" disabled>
              </div>
              <div class="input-group input-group-static mb-4">
                <label  for="dropOffTown">Town</label>
                <input type="text" class="form-control drop-off" name="dropOffTown" id="dropOffTown" disabled>
              </div>
              <div class="input-group input-group-static mb-4">
                <label  for="dropOffProv">Province</label>
                <input type="text" class="form-control drop-off" name="dropOffProv" id="dropOffProv" disabled>
              </div>
              <div class="input-group input-group-static mb-4">
                <label  for="dropOffPostal">Postal Code</label>
                <input type="text" class="form-control drop-off" name="dropOffPostal" id="dropOffPostal" disabled>
              </div>
            </li>
          </ul>
        </div>
    </div>
</div>