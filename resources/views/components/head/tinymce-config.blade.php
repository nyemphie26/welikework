<div>
    <!-- Do what you can, with what you have, where you are. - Theodore Roosevelt -->
    <script src="https://cdn.tiny.cloud/1/9q8k62htmm2hjhshmdhpri1kzwzsr88hulr0e69admd8vyky/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
    tinymce.init({
        selector: 'textarea#richtextbox', // Replace this CSS selector to match the placeholder element for TinyMCE
        plugins: 'code table lists',
        toolbar: 'undo redo | blocks | bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code | table'
    });
    </script>
</div>