@extends('layouts.template')

@section('content')
<div class="container-fluid px-2 px-md-4">
  <div class="row p-4">
      <div class="col-lg-4">
          <div class="card">
            <div class="card-header pb-0 p-3">
              <div class="row">
                <h6 class="mb-0">Customer's Data</h6>
              </div>
            </div>
            <div class="card-body p-3 pb-0">
              <form>
                  <div class="input-group input-group-dynamic mb-4">
                    <label class="form-label">Phone Number</label>
                    <input type="text" class="form-control">
                  </div>
                  <div class="input-group input-group-dynamic mb-4">
                    <label class="form-label">Email</label>
                    <input type="text" class="form-control">
                  </div>
                  <div class="input-group input-group-dynamic mb-4">
                    <label class="form-label">First Name</label>
                    <input type="text" class="form-control">
                  </div>
                  <div class="input-group input-group-dynamic mb-4">
                    <label class="form-label">Last Name</label>
                    <input type="text" class="form-control">
                  </div>
              </form>
            </div>
          </div>
      </div>
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header">
            <div class="row">
              <h6 class="mb-0">Works Order</h6>
            </div>
          </div>
          <div class="card-body">
            {{-- <x-head.req-work-input/> --}}
          </div>
        </div>
      </div>
  </div>
  <div class="row p-4">
    <div class="card">
      <div class="card-body p-3 pb-0">
        <button class="btn bg-gradient-success w-100" type="button">Save Order</button>
      </div>
    </div>
  </div>
</div>
@endsection