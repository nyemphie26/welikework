@extends('layouts.template')

@section('content')
    
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-lg-4">
            <div class="card my-4">
              <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-success shadow-success border-radius-lg pt-4 pb-3">
                  <h6 class="text-white text-capitalize ps-3">Categories</h6>
                </div>
              </div>
              <div class="card-body px-0 pb-2">
                <div class="row p-3">
                  <div class="col-6"></div>
                  <div class="col-6 text-end">
                      <button class="btn bg-gradient-primary" type="button" id="newCategory" data-header="New Category">New Category</button>
                  </div>
                </div>
                <div class="table-responsive p-0">
                  <table class="table align-items-center mb-0">
                    <thead>
                      <tr>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-center">#</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Category Name</th>
                        <th class="text-secondary opacity-7"></th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $category)
                            <tr>
                                <td class="align-middle text-center">
                                    {{ $loop->iteration }}
                                </td>
                                <td>
                                    <div class="d-flex px-2 py-1">
                                        <div class="d-flex flex-column justify-content-center">
                                            <h6 class="mb-0 text-sm">{{ $category->category_name }}</h6>
                                        </div>
                                    </div>
                                </td>
                                <td class="align-middle">
                                    <button type="button" class="btn btn-link pe-3 ps-0 mb-0 ms-auto w-25 w-md-auto" id="editCategory" data-id="{{ $category->id }}" data-name="{{ $category->category_name }}" data-header="Edit Category">
                                    Edit
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="card my-4">
              <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-success shadow-success border-radius-lg pt-4 pb-3">
                  <h6 class="text-white text-capitalize ps-3">Services</h6>
                </div>
              </div>
              <div class="card-body px-0 pb-2">
                <div class="row p-3">
                  <div class="col-6"></div>
                  <div class="col-6 text-end">
                      <button class="btn bg-gradient-primary" type="button" data-bs-toggle="modal" data-bs-target="#modalService">New Service</button>
                  </div>
                </div>
                <div class="table-responsive p-0">
                  <table class="table align-items-center mb-0">
                    <thead>
                      <tr>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-center">#</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Category</th>
                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Service Name</th>
                        <th class="text-secondary opacity-7"></th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($services as $service)
                            <tr>
                                <td class="align-middle text-center">
                                    {{ $loop->iteration }}
                                </td>
                            <td>
                                <div class="d-flex px-2 py-1">
                                <div class="d-flex flex-column justify-content-center">
                                    <p class="text-xs font-weight-bold mb-0">{{ $service->category->category_name }}</p>
                                </div>
                                </div>
                            </td>
                            <td class="align-middle text-center">
                                <h6 class="mb-0 text-sm">{{ $service->service_name }}</h6>
                            </td>
                            <td class="align-middle text-center">
                                <a href="{{ route('services.show', $service->id) }}" type="button" class="btn btn-link pe-3 ps-0 mb-0 ms-auto w-25 w-md-auto">
                                Edit
                                </a>
                            </td>
                            </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal modal-lg fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-normal" id="exampleModalLabel">John Michael - Mowing - Req Date ( 8/18/2022 )</h5>
        <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-lg-3">
                    <div class="input-group input-group-static mb-4">
                        <label >Service Type</label>
                        <input type="text" class="form-control" value="Mowing" disabled>
                    </div>
            </div>
            <div class="col-lg-3">
                <div class="input-group input-group-static mb-4">
                    <label >Work Type</label>
                    <input type="text" class="form-control" value="Bi Weekly" disabled>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="input-group input-group-static mb-4">
                    <label >Works / Pick Up Location</label>
                    <input type="text" class="form-control" value="123 Main st" disabled>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="input-group input-group-static mb-4">
                    <label >Drop Off Location</label>
                    <input type="text" class="form-control" disabled>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="input-group input-group-static mb-4">
                    <label >Customer's Note</label>
                    <input type="text" class="form-control" value="Call me when arrives home" disabled>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="input-group input-group-static mb-4">
                    <label >Known Preference</label>
                    <input type="text" class="form-control" value="Muddy Land" disabled>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="input-group input-group-dynamic">
                  <textarea class="form-control" rows="5" placeholder="Job Description" spellcheck="false"></textarea>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="input-group input-group-static my-3">
                    <label>Scheduling Date</label>
                    <input type="datetime-localr" class="form-control" autofocus>
                </div>
                <div class="input-group input-group-static">
                    <label for="exampleFormControlSelect2" class="ms-0">Assign to (hold ctrl+click to choose multiple)</label>
                    <select multiple class="form-control" id="exampleFormControlSelect2">
                      <option>Nic</option>
                      <option>Roger</option>
                      <option>Jason</option>
                    </select>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Cancel</button>
        <button type="button" class="btn bg-gradient-success">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal New Category -->
<div class="modal modal-sm fade" id="modalCategory" tabindex="-1" role="dialog" aria-labelledby="modalNewCategory" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title font-weight-normal" id="modalHeaderCategory"></h5>
          <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="formCategory" method="POST">
            @csrf
            <div class="input-group input-group-static mb-4">
                <label >Category Name</label>
                <input type="text" class="form-control" name="category_name" id="categoryName">
            </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="reset" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Cancel</button>
          <button type="submit" class="btn bg-gradient-success">Save</button>
        </div>
            </form>
      </div>
    </div>
</div>
<!-- Modal New Service -->
<div class="modal modal-sm fade" id="modalService" tabindex="-1" role="dialog" aria-labelledby="modalNewService" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title font-weight-normal" id="modalNewService">New Service</h5>
          <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('services.store') }}" method="POST">
            @csrf
            <div class="input-group input-group-static mb-4">
                <label >Service Type</label>
                <input type="text" class="form-control" name="service_name" id="serviceName">
            </div>
            <div class="input-group input-group-static mb-4">
                <label >Category<br>(type to search or input new category)</label>
                <input class="form-control" list="datalistOptions" name="category_name" autocomplete="off" id="serviceCategory">
                <datalist id="datalistOptions">
                    @foreach ($categories as $category)
                        <option value="{{ $category->category_name }}"></option>
                    @endforeach
                </datalist>
            </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="reset" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Cancel</button>
          <button type="submit" class="btn bg-gradient-success">Save</button>
        </div>
            </form>
      </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
  $('div').on('click', '#newCategory', function (e) {
    console.log('yes');
    $('#formCategory')[0].action = '{{ route('categories.'.'store') }}';
    document.getElementById('modalHeaderCategory').innerHTML = $(this).data('header');
    $('#modalCategory').modal('show');
  });

  $('div').on('click', '#editCategory', function (e) {
    console.log('yes');
    $('#formCategory')[0].action = '{{ route('categories.'.'update','__id') }}'.replace('__id',$(this).data('id'));
    $('#categoryName').val($(this).data('name'));
    document.getElementById('modalHeaderCategory').innerHTML = $(this).data('header');
    $('#modalCategory').modal('show');
  });
</script>
@endsection