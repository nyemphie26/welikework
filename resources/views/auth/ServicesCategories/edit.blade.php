@extends('layouts.template')

@section('content')
    
<div class="container-fluid py-4">
    <div class="row justify-content-center">
        <div class="col-lg-4">
            <div class="card my-4">
              <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-success shadow-success border-radius-lg pt-4 pb-3">
                  <h6 class="text-white text-capitalize ps-3">Edit Service</h6>
                </div>
              </div>
              <div class="card-body pb-2">
                  <form action="{{ route('services.update',$service->id) }}" method="post">
                    @method('PUT')
                    @csrf
                    <div class="input-group input-group-static mb-4">
                        <label>Service Name</label>
                        <input type="text" class="form-control" name="service_name" id="name" value="{{ $service->service_name }}">
                    </div>
                    <div class="input-group input-group-static mb-4">
                        <label >Category<br>(type to search or input new category)</label>
                        <input class="form-control" list="datalistOptions" name="category_name" value="{{ $service->category->category_name }}">
                        <datalist id="datalistOptions">
                            @foreach ($categories as $category)
                                <option value="{{ $category->category_name }}"></option>
                            @endforeach
                        </datalist>
                    </div>
                    <button type="submit" class="btn btn-sm bg-gradient-success w-100">Update</button>
                    <a href="{{ route('services.index') }}" class="btn btn-sm btn-outline-secondary w-100">Cancel</a>
                  </form>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection