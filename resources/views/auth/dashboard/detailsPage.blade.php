@extends('layouts.template')

@section('css')
<link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/owl.theme.green.min.css') }}" rel="stylesheet" />

<style>
  .owl-prev span, .owl-next span{
    display: none;
  }
  .image-area {
    position: relative;
    width: 50%;
    background: #333;
}
.image-area img{
  max-width: 100%;
  height: auto;
}
.remove-image {
    display: none;
    position: absolute;
    top: -10px;
    right: -10px;
    border-radius: 10em;
    padding: 2px 6px 3px;
    text-decoration: none;
    font: 700 21px/20px sans-serif;
    background: #555;
    border: 3px solid #fff;
    color: #FFF;
    box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
    text-shadow: 0 1px 2px rgba(0,0,0,0.5);
    -webkit-transition: background 0.5s;
    transition: background 0.5s;
}
.remove-image:hover {
 background: #E54E4E;
  padding: 3px 7px 5px;
  top: -6px;
right: -6px;
}
.remove-image:active {
    background: #E54E4E;
    top: -5px;
    right: -6px;
}
</style>
@endsection

@section('content')
    <div class="container-fluid py-4">
        <div class="row justify-content-center">
            <div class="card my-4 col-lg-8">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-success shadow-success border-radius-lg pt-4 pb-3">
                        <h5 class="text-white text-capitalize ps-3">{{ $work->reff_no." -- ".$work->cust->name." ".$work->cust->last_name." -- (".date('M d, Y -- H:i', strtotime($work->schedule->sch_date)).")" }}</h5>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="row row-cols-sm-6">
                        <div class="col-lg-5 col-sm-5">
                          <div class="input-group input-group-static mb-4">
                              <label >Category</label>
                              <input type="text" class="form-control" id="category" value="{{ $work->service->category->category_name }}" disabled>
                          </div>
                        </div>
                        <div class="col-lg-5 col-sm-5">
                          <div class="input-group input-group-static mb-4">
                              <label >Service</label>
                              <input type="text" class="form-control" id="service" value="{{ $work->service->service_name }}" disabled>
                          </div>
                        </div>
                        <div class="col-lg-2 col-sm-2">
                            <div class="input-group input-group-static mb-4">
                              <label >Tipping Fee</label>
                              <input type="text" class="form-control" id="tipping" value="$ {{ $work->billing != null ? $work->billing->tipping : '0' }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                          <div class="input-group input-group-static mb-4">
                              <label >Works / Pick Up Location</label>
                              <input type="text" class="form-control" id="pickUpLoc" value="{{ $pickUpLoc->address }}" disabled>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="input-group input-group-static mb-4">
                              <label >Drop Off Location</label>
                              <input type="text" class="form-control" id="dropOffLoc" value="{{ $dropOffLoc != null ? $dropOffLoc->address : '-' }}" disabled>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group input-group-static mb-4">
                                <label >Customer's Note</label>
                                <input type="text" class="form-control" id="cust_notes" value="{{ $work->cust_notes }}" disabled>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group input-group-static mb-4">
                                <label >Known Preference</label>
                                <input type="text" class="form-control" id="known_pref" value="{{ $work->known_pref }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                          <div class="input-group input-group-static mb-4">
                              <label >Employee</label>
                              <input type="text" class="form-control" id="employees" value="{{ $asgEmployees != null ? $asgEmployees : 'Employee has not been assigned' }}" disabled />
                          </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-group input-group-static mb-4">
                              <label >Actual Start</label>
                              <input type="text" class="form-control" id="actual_start" value="{{$work->schedule->actual_start ? date('M d, Y -- H:i', strtotime($work->schedule->actual_start)) : 'Work not been started' }}" disabled>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-group input-group-static mb-4">
                              <label >Actual End</label>
                              <input type="text" class="form-control" id="actual_end" value="{{$work->schedule->actual_start ? date('M d, Y -- H:i', strtotime($work->schedule->actual_stop)) : 'Work not been started' }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="input-group input-group-static mb-4">
                          <label>Job Description</label>
                          <textarea class="form-control" rows="3" id="job_desc" spellcheck="false" disabled>{{ $work->job_desc }}</textarea>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="input-group input-group-static mb-4">
                          <label>Employee Notes</label>
                          <textarea class="form-control" rows="3" id="employee_notes" spellcheck="false" disabled>{{ $work->employee_notes }}</textarea>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="owl-carousel owl-theme">
                            @foreach ($work->images as $item)
                                <div class="item thumbnail">
                                    <a href="{{ asset($item->img_path) }}">
                                        <img class="img-responsive" src="{{ asset('storage/'.$item->img_path) }}" alt="{{ $item->img_desc }}" />
                                        <div class="caption">
                                            <p class="text-center">{{ $item->img_desc }}</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="card-footer justify-content-between d-lg-flex">
                    @canany(['access admin page','access employee page'])
                    <a class="btn bg-gradient-info w-100 w-lg-auto order-lg-last" type="button" id="exportWork">
                        Print to PDF
                    </a>
                    <a href="{{ route('workImages.show', $work) }}" class="btn bg-gradient-secondary w-100 w-lg-auto">
                        <span class="btn-inner--icon"><i class="material-icons">photo_camera_back</i></span>
                        <span class="btn-inner--text">Manage Photos</span>
                    </a>
                    @endcan
                    <a class="btn btn-link text-info block w-100 w-lg-auto order-lg-first" type="button" href="{{ route('dashboard') }}">
                        back
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('javascript')
<script type="text/javascript" src="{{ asset('js/plugins/owl.carousel.min.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $('.owl-carousel').owlCarousel({
            autoplay: false,
            autoplayhoverpause: true,
            loop: true,
            margin: 25,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:2,
                    nav:false
                },
                1000:{
                    items:4,
                    nav:true,
                    loop:false
                }
            }
        });
    });
  </script>

@endsection