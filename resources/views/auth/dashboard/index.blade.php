@extends('layouts.template')

@section('css')
<link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/owl.theme.green.min.css') }}" rel="stylesheet" />

<style>
  .owl-prev span, .owl-next span{
    display: none;
  }
  
</style>
@endsection

@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="card my-4">
              <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                  <div class="nav-wrapper position-relative end-0">
                      <ul class="nav nav-pills nav-fill p-1" role="tablist">
                          <li class="nav-item">
                          <a class="nav-link mb-0 px-0 py-1 active " data-bs-toggle="tab" data-bs-target="#todays" aria-controls="todays" aria-selected="true" href="javascript:;" role="tab">
                              <i class="material-icons text-lg position-relative">home</i>
                              <span class="ms-1">Today's Work</span>
                              {{-- <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Home</button> --}}
                          </a>
                          </li>
                          <li class="nav-item">
                          <a class="nav-link mb-0 px-0 py-1 " data-bs-toggle="tab" data-bs-target="#upcoming" aria-controls="awaiting" href="javascript:;" role="tab" aria-selected="false">
                              <i class="material-icons text-lg position-relative">email</i>
                              <span class="ms-1">All Works</span>
                          </a>
                          </li>
                          <li class="nav-item">
                          <a class="nav-link mb-0 px-0 py-1 " data-bs-toggle="tab" data-bs-target="#finished" aria-controls="awaiting" href="javascript:;" role="tab" aria-selected="false">
                              <i class="material-icons text-lg position-relative">done_all</i>
                              <span class="ms-1">Finished Works</span>
                          </a>
                          </li>
                      </ul>
                  </div>
              </div>
              <div class="card-body px-0 pb-2">
                <div class="tab-content">
                  {{-- Tab Pane 1 --}}
                  <div class="tab-pane fade show active" id="todays" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
                    <div class="col-lg-12 text-end">
                      <a class="btn bg-gradient-success w-lg-20 w-100" type="button" id="export" href="{{ route('export.today') }}">Export Today's Work To PDF</a>
                    </div>
                    <div class="table-responsive p-0">
                      <table class="table align-items-center mb-0">
                        <thead>
                          <tr>
                            <th class="text-secondary opacity-7"></th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Service Type</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Works / Pick Up Location</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Drop Off Location</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Scheduled Time</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Reffeence No.</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Client Name</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($todayWorks as $work)
                          <tr>
                            <td class="align-middle">
                              <button type="button" class="btn btn-link p-0 m-0" onclick="window.location.href='{{ route('details',$work->work) }}';">
                                Details
                              </button>
                              <button type="button" class="btn btn-link p-0 m-0 text-info" id="finishWork" data-id="{{ $work->work_id }}">
                                Finishing Work
                              </button>
                            </td>
                            <td>
                              <p class="text-xs font-weight-bold mb-0">{{ $work->work->service->service_name }}</p>
                            </td>
                            <td class="align-middle text-center">
                              <span class="text-secondary text-xs font-weight-bold">{{ $work->work->location->address }}</span>
                            </td>
                            <td class="align-middle text-center">
                              <span class="text-secondary text-xs font-weight-bold">{{ $work->work->dropOffLocation ? $work->work->dropOffLocation->address : '-' }}</span>
                            </td>
                            <td class="align-middle text-center">
                              <span class="text-secondary text-xs font-weight-bold">{{ date('H:i', strtotime($work->work->schedule->sch_date)) }}</span>
                            </td>
                            <td class="align-middle text-center">
                              <span class="text-secondary text-xs font-weight-bold">{{ $work->work->reff_no }}</span>
                            </td>
                            <td>
                              <div class="d-flex px-2 py-1">
                                <div class="d-flex flex-column justify-content-center">
                                  <h6 class="mb-0 text-sm">{{ $work->work->cust->name }}&nbsp;{{ $work->work->cust->last_name }}</h6>
                                  <p class="text-xs text-secondary mb-0">{{ $work->work->cust->phone_number }}</p>
                                </div>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                  {{-- Tab Pane 2 --}}
                  <div class="tab-pane fade" id="upcoming" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
                    <div class="table-responsive p-0">
                      <table class="table align-items-center mb-0">
                        <thead>
                          <tr>
                            <th class="text-secondary opacity-7"></th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Service Type</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Work / Pick Up Location</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Drop Off Location</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Sceduled Date</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Reffeence No</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Client Name</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($works as $work)
                            <tr>
                              <td class="align-middle">
                                <button type="button" class="btn btn-link p-0 m-0" onclick="window.location.href='{{ route('details',$work->work) }}';">
                                  Details
                                </button>
                                <button type="button" class="btn btn-link p-0 m-0 text-secondary" id="finishWork" data-id="{{ $work->work_id }}">
                                  Finish Now
                                </button>
                              </td>
                              <td>
                                <p class="text-xs font-weight-bold mb-0">{{ $work->work->service->service_name }}</p>
                                @if (date('Y-m-d', strtotime($work->work->schedule->sch_date)) < date('Y-m-d'))
                                  <p class="text-xs text-primary mb-0">Overdue</p>
                                @elseif (date('Y-m-d', strtotime($work->work->schedule->sch_date)) > date('Y-m-d'))
                                  <p class="text-xs text-info mb-0">Upcoming</p>
                                @else
                                  <p class="text-xs text-success mb-0">Today</p>
                                @endif
                              </td>
                              <td class="align-middle text-center">
                                <span class="text-secondary text-xs font-weight-bold">{{ $work->work->location ? $work->work->location->address : '-' }}</span>
                              </td>
                              <td class="align-middle text-center">
                                <span class="text-secondary text-xs font-weight-bold">{{ $work->work->dropOffLocation ? $work->work->dropOffLocation->address : '-' }}</span>
                              </td>
                              <td class="align-middle text-center">
                                <span class="text-secondary text-xs font-weight-bold">{{ date('M d, Y -- H:i', strtotime($work->work->schedule->sch_date)) }}</span>
                              </td>
                              <td class="align-middle text-center">
                                <span class="text-secondary text-xs font-weight-bold">{{ $work->work->reff_no }}</span>
                              </td>
                              <td>
                                <div class="d-flex px-2 py-1">
                                  <div class="d-flex flex-column justify-content-center">
                                    <h6 class="mb-0 text-sm">{{ $work->work->cust->name }}&nbsp;{{ $work->work->cust->last_name }}</h6>
                                    <p class="text-xs text-secondary mb-0">{{ $work->work->cust->phone_number }}</p>
                                  </div>
                                </div>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                  {{-- Tab Pane 3 --}}
                  <div class="tab-pane fade" id="finished" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
                    <div class="table-responsive p-0">
                      <table class="table align-items-center mb-0">
                        <thead>
                          <tr>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Reffence No.</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Client Name</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Service Type</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Actual Start</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Actual End</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Tipping Fee</th>
                            <th class="text-secondary opacity-7"></th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($finishedWorks as $work)
                          <tr>
                            <td>
                              <p class="text-xs font-weight-bold mb-0">{{ $work->work->reff_no }}</p>
                            </td>
                            <td>
                              <div class="d-flex px-2 py-1">
                                <div class="d-flex flex-column justify-content-center">
                                  <h6 class="mb-0 text-sm">{{ $work->work->cust->name }}&nbsp;{{ $work->work->cust->last_name }}</h6>
                                  <p class="text-xs text-secondary mb-0">{{ $work->work->cust->phone_number }}</p>
                                </div>
                              </div>
                            </td>
                            <td>
                              <p class="text-xs font-weight-bold mb-0">{{ $work->work->service->service_name }}</p>
                            </td>
                            <td class="align-middle text-center">
                              <span class="text-secondary text-xs font-weight-bold">{{ date('M d, Y -- H:i', strtotime($work->work->schedule->actual_start)) }}</span>
                            </td>
                            <td class="align-middle text-center">
                              <span class="text-secondary text-xs font-weight-bold">{{ date('M d, Y -- H:i', strtotime($work->work->schedule->actual_stop)) }}</span>
                            </td>
                            <td class="align-middle text-center">
                              <span class="text-secondary text-xs font-weight-bold">${{ $work->work->billing->tipping }}</span>
                            </td>
                            <td class="align-middle">
                              <button type="button" class="btn btn-link p-0 m-0" onclick="window.location.href='{{ route('details',$work->work) }}';">
                                Details
                              </button>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal modal-lg fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title font-weight-normal" id="detailsHeader"></h5>
            <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-lg-5">
                  <div class="input-group input-group-static mb-4">
                      <label >Category</label>
                      <input type="text" class="form-control" id="category" disabled>
                  </div>
                </div>
                <div class="col-lg-5">
                  <div class="input-group input-group-static mb-4">
                      <label >Service</label>
                      <input type="text" class="form-control" id="service" disabled>
                  </div>
                </div>
                <div class="col-lg-2">
                    <div class="input-group input-group-static mb-4">
                      <label >Tipping Fee</label>
                      <input type="text" class="form-control" id="tipping" disabled>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                  <div class="input-group input-group-static mb-4">
                      <label >Works / Pick Up Location</label>
                      <input type="text" class="form-control" id="pickUpLoc" disabled>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="input-group input-group-static mb-4">
                      <label >Drop Off Location</label>
                      <input type="text" class="form-control" id="dropOffLoc" disabled>
                  </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group input-group-static mb-4">
                        <label >Customer's Note</label>
                        <input type="text" class="form-control" id="cust_notes" disabled>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="input-group input-group-static mb-4">
                        <label >Known Preference</label>
                        <input type="text" class="form-control" id="known_pref" disabled>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                  <div class="input-group input-group-static mb-4">
                      <label >Employee</label>
                      <input type="text" class="form-control" id="employees" disabled>
                  </div>
                </div>
                <div class="col-lg-3">
                    <div class="input-group input-group-static mb-4">
                      <label >Actual Start</label>
                      <input type="text" class="form-control" id="actual_start" disabled>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="input-group input-group-static mb-4">
                      <label >Actual End</label>
                      <input type="text" class="form-control" id="actual_end" disabled>
                    </div>
                </div>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <div class="input-group input-group-static mb-4">
                  <label>Job Description</label>
                  <textarea class="form-control" rows="3" id="job_desc" spellcheck="false" disabled></textarea>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="input-group input-group-static mb-4">
                  <label>Employee Notes</label>
                  <textarea class="form-control" rows="3" id="employee_notes" spellcheck="false" disabled></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="owl-carousel owl-theme img-row">
                {{-- <div class="item"><img src="{{ asset('storage/workImages/F46419460/GzX8ikEtyeFkdGageEAM4rsSav0mNir6qxjzqIaX.png') }}" alt="a" class="img-thumbnail img-fluid"/></div>
                <div class="item"><img src="{{ asset('storage/workImages/F46419460/XH3fK5hn5YLyQOScPaY9GDICaNUfgSk42m1QvMij.png') }}" alt="b" class="img-thumbnail img-fluid"/></div> --}}
            </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <a class="btn btn-link text-info" type="button" id="exportWork">
              Print as PDF
            </a>
            <button type="button" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal modal-lg fade" id="finishModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title font-weight-normal" id="detailsHeader"></h5>
            <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-4">
                <div class="input-group input-group-static mb-4">
                    <label >Actual Start</label>
                    <input type="datetime-local" class="form-control" id="actual_start_inp">
                </div>
              </div>
              <div class="col-lg-4">
                <div class="input-group input-group-static mb-4">
                    <label >Actual End</label>
                    <input type="datetime-local" class="form-control" id="actual_stop_inp">
                </div>
              </div>
              <div class="col-lg-4">
                <div class="input-group input-group-static mb-4">
                    <label >Tipping Fee</label>
                    <input type="number" class="form-control" id="tipping_inp" step="0.1">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="input-group input-group-static mb-4">
                    <label >Employee Notes</label>
                    <textarea class="form-control" id="empNotes" cols="30" rows="3"></textarea>
                  </div>
                </div>
              </div>
            </div>
          <div class="modal-footer justify-content-between">
            <input type="hidden" class="form-control" id="workId">
            @csrf
            <button type="button" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" class="btn bg-gradient-success" id="saveWork">Save</button>
          </div>
        </div>
      </div>
    </div>


    <!-- Modal Photos -->
    <div class="modal modal-sm fade" id="photosModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title font-weight-normal" id="photosModalHeader"></h5>
            <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-12">
                <img src="" class="form-control col-sm-1 img-fluid workImagesPrw">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <div class="input-group input-group-static mb-4 employee">
                  <input type="file" class="form-control" id="images" name="images" onchange="previewImg(this.id,'workImagesPrw')" multiple>
                  {{-- <input type="file" name="images" id="images" class="form-control" multiple> --}}
                </div>
                <div class="input-group input-group-static mb-4">
                  <label >Description</label>
                  <input type="text" name="imageDesc" id="imageDesc" class="form-control">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <input type="hidden" class="form-control" id="workId">
            @csrf
            <button type="button" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="button" class="btn bg-gradient-success act-photo" data-acc="assign">Save</button>
          </div>
        </div>
      </div>
    </div>

@endsection


@section('javascript')
  <script src="{{ asset('js/plugins/formPreviewImage.js') }}"></script>
  
  <script type="text/javascript" src="{{ asset('js/plugins/owl.carousel.min.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      
    });
  </script>
  <script>

    $(document).ready(function () {
      $('.table').dataTable({
          "info":false,
          "lengthChange": false,
          "pageLength": 10,
          language: {
            paginate: {
              next: '<i class="fa fa-angle-right"></i>', // or '→'
              previous: '<i class="fa fa-angle-left"></i>' // or '←' 
            }
          },
          "drawCallback": function () {
            $('.dataTables_paginate ul.pagination').addClass('pagination-success');
          }
      });
      
      $('body').on('click', '#showDetails', function (event) {

          event.preventDefault();
          var id = $(this).data('id');
          $.ajax({
            type: 'GET',
            dataType: 'JSON',
            url: '/works/'+id,
            success:function(data){
              var header = data.data.reff_no+' -- '+data.user.name+' '+data.user.last_name+' -- ('+data.sch_date+' -- '+data.sch_time+')';
              $('#detailsModal').modal('show');
              $('#req_date').val(data.date);
              $('#req_time').val(data.time);
              $('#job_desc').val(data.data.job_desc);
              $('#actual_start').val(data.start);
              $('#actual_end').val(data.stop);
              $('#employee_notes').val(data.data.employee_notes);
              if (data.sch_date) {
                  $('#sch_date').val(data.sch_date);
                  $('#sch_time').val(data.sch_time);
              }
              $('#pickUpLoc').val(data.address);
              if (data.dropOff) {
                $('#dropOffLoc').val(data.dropOff);
              }
              if (data.employees) {
                $('#employees').val(data.employees);
              }
              document.getElementById('detailsHeader').innerHTML = header;
              $('#cust_notes').val(data.data.cust_notes);
              $('#known_pref').val(data.data.known_pref);
              $('#category').val(data.cat);
              $('#service').val(data.serv);
              if (data.billing) {
                $('#tipping').val('$'+data.billing.tipping);
              }
              $('#exportWork').attr('href','/exportWork/'+id);
              data.images.forEach(element => {
                $('.img-row').append('<div class="item"><img src="storage/' + element.img_path + '" class="img-thumbnail img-fluid" /></div>');
              });
              $('.owl-carousel').owlCarousel({
                autoplay: true,
                autoplayhoverpause: true,
                loop: true,
                margin: 25,
                responsive:{
                    0:{
                        items:1,
                        nav:true
                    },
                    600:{
                        items:2,
                        nav:false
                    },
                    1000:{
                        items:4,
                        nav:true,
                        loop:false
                    }
                }
              });
            }
          })
          
      });

      $('body').on('click', '#finishWork', function (event) {
        $('#finishModal').modal('show');
        $('#workId').val($(this).data('id'));
      });

      $('body').on('click', '#saveWork', function (event) {
          event.preventDefault();
          var token = $('input[name="_token"]').val()
           id = $('#workId').val()
           tipping = $('#tipping_inp').val()
           start = $('#actual_start_inp').val()
           stop = $('#actual_stop_inp').val()
           notes = $('#empNotes').val()
           $.ajax({
              type: 'PUT',
              dataType: 'JSON',
              url: '/works/'+id+'/finish',
              data: {
                  _token: token,
                  'id': id,
                  'tipping': tipping,
                  'start': start,
                  'stop': stop,
                  'notes': notes
              },
              success:function(data){
                  if (data.success){
                    $('#finishModal').modal('hide');
                    $(".tab-content").load(" .tab-content > *");
                  }
              }
          })
      });

      $('body').on('click', '#photosDetails', function (event) {
        event.preventDefault();
        $('#workId').val($(this).data('id'));
        var header = 'Work Images';
        $('#photosModal').modal('show');
        document.getElementById('photosModalHeader').innerHTML = header;
      });

      $('body').on('click', '.act-photo', function(e) {
        e.preventDefault();
        id = $('#workId').val();
        var token = $('input[name="_token"]').val();
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: '/works/'+id+'/add-photo',
            data: {
                _token: token,
            },
            success:function(data){
                if (data.success){
                  console.log(data.message)
                }
            }
        })

      });

      $('#detailsModal').on('hidden.bs.modal', function (e) {
        $('.item').remove();
        $(this).find("input,textarea").val('').end();
      });

      $('#finishModal').on('hidden.bs.modal', function (e) {
        $(this).find("input,texarea,input[type=datetime-local],input[type=number]").val('').end();
      });
      
      $('#photosModal').on('hidden.bs.modal', function (e) {
        $('.workImagesPrw').removeAttr('src');
        $(this).find("input,input[type=file]").val('').end();
      });
      
    }); 

  </script>
@endsection