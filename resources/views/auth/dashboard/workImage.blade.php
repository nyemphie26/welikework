@extends('layouts.template')

@section('css')
<style>
  .image-area {
    position: relative;
    /* width: 50%; */
    /* background: #333; */
}
.image-area img{
  max-width: 100%;
  height: auto;
}
.remove-image {
    position: absolute;
    top: -5px;
    right: -5px;
    border-radius: 10em;
    padding: 2px 6px 3px;
    text-decoration: none;
    font: 700 21px/20px sans-serif;
    background: #555;
    border: 3px solid #fff;
    color: #FFF;
    box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
    text-shadow: 0 1px 2px rgba(0,0,0,0.5);
    -webkit-transition: background 0.5s;
    transition: background 0.5s;
}
.remove-image:hover {
 background: #E54E4E;
  padding: 3px 7px 5px;
  top: -6px;
right: -6px;
}
.remove-image:active {
    background: #E54E4E;
    top: -5px;
    right: -6px;
}
</style>
@endsection

@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-success shadow-success border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize ps-3">Work {{ $work->reff_no }}</h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="row">
                        <div class="col-lg-8 order-lg-2">
                            <div class="row">
                                @foreach ($work->images as $item)
                                    <div class="column image-area thumbnail" style="width: 25%; padding: 10px; float:left; cursor: pointer; ">
                                        <img src="{{ asset('storage/'.$item->img_path) }}" class="img-thumbnail img-fluid" alt="{{ $item->img_desc }}" onclick="myFunction(this);">
                                        <button type="submit" class="remove-image" id="deleteImage" data-imgid="{{ $item->id }}" data-user="{{ Auth::id() }}" data-userid="{{ $item->user_id }}">&#215;</button>
                                        <div class="caption">
                                            <p class="text-center">{{ $item->img_desc }} by {{ $item->user->last_name }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <form action="{{ route('workImages.update', $work->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <img src="" class="img-thumbnail form-control workImagesPrw">
                                <div class="input-group input-group-static mb-4 employee">
                                    <input type="file" class="form-control" id="images" name="images" onchange="previewImg(this.id,'workImagesPrw')" multiple>
                                    {{-- <input type="file" name="images" id="images" class="form-control" multiple> --}}
                                </div>
                                <div class="input-group input-group-static mb-4">
                                    <label>Photo Description</label>
                                    <input type="text" name="imageDesc" id="imageDesc" class="form-control">
                                </div>
                                <div class="col justify-content-between d-lg-flex">
                                    <button type="submit" class="btn bg-gradient-success act-photo w-100 w-lg-auto order-lg-2" data-acc="assign">Save Photo</button>
                                    {{-- <a class="btn btn-link text-info block w-100 w-lg-auto order-lg-1" type="button" href="{{ route('details',$work->id) }}">
                                        back
                                    </a> --}}
                                    <a class="btn btn-link text-info block w-100 w-lg-auto order-lg-1" type="button" href="{{ URL::previous() }}">
                                        back
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal modal-sm fade" id="confirmDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title font-weight-normal" id="detailsHeader"></h5>
              <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h5 id="message"></h5>
            </div>
            <div class="modal-footer justify-content-center">
                <input type="hidden" name="imgs" id="imgs" />
                <button type="submit" class="btn btn-link txt-danger act-yes w-75 py-0 m-1" data-acc="accept" style="display: none">Yes</button>
              <button type="button" class="btn bg-gradient-info act-no w-85 py-1 m-1" data-acc="reject" data-bs-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div>
      </div>
@endsection


@section('javascript')

<script src="{{ asset('js/plugins/formPreviewImage.js') }}"></script>

<script>

    $(document).ready(function () {

        $('body').on('click', '#deleteImage', function (event) {
            event.preventDefault();
            var msg
            var owner = $(this).data('userid')
            var user = $(this).data('user')
            var img = $(this).data('imgid')
                if (owner != user) {
                    msg = 'This is not your photo!</br> You are only allowed to delete your own photo.'
                } else {
                    msg = 'Are you sure to delete this photo?'
                    $('button.act-yes').css('display', '');
                }
            document.getElementById('message').innerHTML = msg;
            $('#imgs').val(img);
            $('#confirmDelete').modal('show');
            
        });
        
        $('#confirmDelete').on('hidden.bs.modal', function (e) {
            $(this).find("input,textarea").val('').end();
            $('button.act-yes').css('display', 'none');
        })

        $('body').on('click', '.act-yes', function(event){
          event.preventDefault();
          var token = $('input[name="_token"]').val();
          var img = $('#imgs').val();
            $.ajax({
                type: 'DELETE',
                dataType: 'JSON',
                url: '{{ route('workImages.destroy', ['img',4]) }}',
                data: {
                    _token: token,
                    'id': img
                },
                success:function(data){
                    if (data.success){
                        console.log(data.message)
                        $('#confirmDelete').modal('hide');
                        location.reload();
                        // return true;
                    }
                }
            })
        });

    }); 

  </script>
@endsection