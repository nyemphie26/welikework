<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link id="pagestyle" href="{{ asset('css/material-dashboard.css?v=3.0.4') }}" rel="stylesheet" />
    <style>
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
    <h3 class="modal-title font-weight-normal text-center mb-0">Today's Work List - {{ date('F d, Y') }}</h3>
    <h3 class="modal-title text-xs font-weight-normal text-center">{{ Auth::user()->name }}&nbsp;{{ Auth::user()->last_name }}</h3><br>
    <table class="table" border="1px" width="100%">
        <thead>
            <tr>
            {{-- <th class="text-secondary opacity-7"></th> --}}
            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Scheduled Time</th>
            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Service Type</th>
            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Works / Pick Up Location</th>
            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Drop Off Location</th>
            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Client Name</th>
            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Reffeence No.</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($works as $work)
            <tr>
                {{-- <td class="align-middle">
                    <button type="button" class="btn btn-link p-0 m-0" id="showDetails" href="#{{ $work->work->reff_no }}">
                    Details
                    </button>
                </td> --}}
                <td class="align-middle">
                    <span class="text-secondary text-xs font-weight-bold px-3">{{ date('H:i', strtotime($work->work->schedule->sch_date)) }}</span>
                </td>
                <td class="align-middle">
                    <p class="text-xs font-weight-bold mb-0">{{ $work->work->service->service_name }}</p>
                </td>
                <td class="align-middle text-center">
                    <span class="text-secondary text-xs font-weight-bold">{{ $work->work->location->address }}</span>
                </td>
                <td class="align-middle text-center">
                    <span class="text-secondary text-xs font-weight-bold">{{ $work->work->dropOffLocation ? $work->work->dropOffLocation->address : '-' }}</span>
                </td>
                <td>
                    <div class="d-flex px-2 py-1">
                        <div class="d-flex flex-column justify-content-center">
                            <h6 class="mb-0 text-sm">{{ $work->work->cust->name }}&nbsp;{{ $work->work->cust->last_name }}</h6>
                            <p class="text-xs text-secondary mb-0">{{ $work->work->cust->phone_number }}</p>
                        </div>
                    </div>
                </td>
                <td class="align-middle text-center">
                    <span class="text-secondary text-xs font-weight-bold">{{ $work->work->reff_no }}</span>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="page-break"></div>
    @foreach ($works as $item)
        <div class="card">
            <div class="card-header">
                <h5 class="modal-title font-weight-normal">Reference No. -- {{ $item->work->reff_no }}</h5>
            </div>
            <div class="card-body">
                <table class="table" border="1px" width="100%">
                    <tbody>
                        <tr>
                            <td>
                                <div class="input-group input-group-static mb-4">
                                    <label >Customer Name</label>
                                    <input type="text" class="form-control" id="category" value="{{ $item->work->cust->name }}&nbsp;{{ $item->work->cust->last_name }}">
                                </div>
                            </td>
                            <td>
                                <div class="input-group input-group-static mb-4">
                                    <label >Schedule Date</label>
                                    <input type="text" class="form-control" id="category" value="{{ date('M d, Y -- H:i', strtotime($item->work->schedule->sch_date)) }}">
                                </div>
                            </td>
                            <td>
                                <div class="input-group input-group-static mb-4">
                                    <label >Category</label>
                                    <input type="text" class="form-control" id="category" value="{{ $item->work->service->category->category_name }}">
                                </div>
                            </td>
                            <td>
                                <div class="input-group input-group-static mb-4">
                                    <label >Service</label>
                                    <input type="text" class="form-control" id="service" value="{{ $item->work->service->service_name }}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="input-group input-group-static mb-4">
                                    <label >Works / Pick Up Location</label>
                                    <input type="text" class="form-control" id="pickUpLoc" value="{{ $item->work->location ? $item->work->location->address.', '.$item->work->location->town.', '.$item->work->location->province.' ( '.$item->work->location->postal.' )' : '' }}">
                                </div>
                            </td>
                            <td colspan="2">
                                <div class="input-group input-group-static mb-4">
                                    <label >Drop Off Location</label>
                                    <input type="text" class="form-control" id="dropOffLoc" value="{{ $item->work->dropOffLocation ? $item->work->dropOffLocation->address.', '.$item->work->dropOffLocation->town.', '.$item->work->dropOffLocation->province.' ( '.$item->work->dropOffLocation->postal.' )' : '' }}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="input-group input-group-static mb-4">
                                    <label >Customer's Note</label>
                                    <input type="text" class="form-control" id="cust_notes" value="{{ $item->work->cust_notes }}">
                                </div>
                            </td>
                            <td colspan="2">
                                <div class="input-group input-group-static mb-4">
                                    <label >Known Preference</label>
                                    <input type="text" class="form-control" id="known_pref" value="{{ $item->work->known_pref }}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div class="input-group input-group-static mb-4">
                                    <label >Employee</label>
                                    <textarea class="form-control" rows="3" id="job_desc" spellcheck="false">@foreach ($item->work->assigns as $employee){{ $employee->user->name }}&nbsp;{{ $employee->user->last_name }},&nbsp;@endforeach</textarea>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="input-group input-group-static mb-4">
                                  <label>Job Description</label>
                                  <textarea class="form-control" rows="3" id="job_desc" spellcheck="false">{{ $item->work->job_desc }}</textarea>
                                </div>
                            </td>
                            <td colspan="2">
                                <div class="input-group input-group-static mb-4">
                                  <label>Employee Notes</label>
                                  <textarea class="form-control" rows="3" id="employee_notes" spellcheck="false" disabled>{{ $item->work->employee_notes }}</textarea>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="page-break"></div>
    @endforeach
</body>
</html>