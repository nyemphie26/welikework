<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link id="pagestyle" href="{{ asset('css/material-dashboard.css?v=3.0.4') }}" rel="stylesheet" />
    <style>
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
    <h3 class="modal-title font-weight-normal text-center mb-0">Work - {{ $work->reff_no }}</h3>
    <h3 class="modal-title text-xs font-weight-normal text-center">{{ Auth::user()->name }}&nbsp;{{ Auth::user()->last_name }}</h3><br>
    <div class="card">
        <div class="card-body">
            <table class="table" border="1px" width="100%">
                <tbody>
                    <tr>
                        <td>
                            <div class="input-group input-group-static mb-4">
                                <label >Customer Name</label>
                                <input type="text" class="form-control" id="category" value="{{ $work->cust->name }}&nbsp;{{ $work->cust->last_name }}">
                            </div>
                        </td>
                        <td>
                            <div class="input-group input-group-static mb-4">
                                <label >Schedule Date</label>
                                <input type="text" class="form-control" id="category" value="{{ date('M d, Y -- H:i', strtotime($work->schedule->sch_date)) }}">
                            </div>
                        </td>
                        <td>
                            <div class="input-group input-group-static mb-4">
                                <label >Category</label>
                                <input type="text" class="form-control" id="category" value="{{ $work->service->category->category_name }}">
                            </div>
                        </td>
                        <td>
                            <div class="input-group input-group-static mb-4">
                                <label >Service</label>
                                <input type="text" class="form-control" id="service" value="{{ $work->service->service_name }}">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="input-group input-group-static mb-4">
                                <label >Works / Pick Up Location</label>
                                <input type="text" class="form-control" id="pickUpLoc" value="{{ $work->location ? $work->location->address.', '.$work->location->town.', '.$work->location->province.' ( '.$work->location->postal.' )' : '' }}">
                            </div>
                        </td>
                        <td colspan="2">
                            <div class="input-group input-group-static mb-4">
                                <label >Drop Off Location</label>
                                <input type="text" class="form-control" id="dropOffLoc" value="{{ $work->dropOffLocation ? $work->dropOffLocation->address.', '.$work->dropOffLocation->town.', '.$work->dropOffLocation->province.' ( '.$work->dropOffLocation->postal.' )' : '' }}">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="input-group input-group-static mb-4">
                                <label >Customer's Note</label>
                                <input type="text" class="form-control" id="cust_notes" value="{{ $work->cust_notes }}">
                            </div>
                        </td>
                        <td colspan="2">
                            <div class="input-group input-group-static mb-4">
                                <label >Known Preference</label>
                                <input type="text" class="form-control" id="known_pref" value="{{ $work->known_pref }}">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div class="input-group input-group-static mb-4">
                                <label >Employee</label>
                                <textarea class="form-control" rows="3" id="job_desc" spellcheck="false">@foreach ($work->assigns as $employee){{ $employee->user->name }}&nbsp;{{ $employee->user->last_name }},&nbsp;@endforeach</textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="input-group input-group-static mb-4">
                              <label>Job Description</label>
                              <textarea class="form-control" rows="3" id="job_desc" spellcheck="false">{{ $work->job_desc }}</textarea>
                            </div>
                        </td>
                        <td colspan="2">
                            <div class="input-group input-group-static mb-4">
                              <label>Employee Notes</label>
                              <textarea class="form-control" rows="3" id="employee_notes" spellcheck="false" disabled>{{ $work->employee_notes }}</textarea>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>