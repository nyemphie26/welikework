@extends('layouts.template')

@section('content')
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-lg-12">
            <div class="card my-4">
              <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-success shadow-success border-radius-lg pt-4 pb-3">
                  <h6 class="text-white text-capitalize ps-3">Accepted Works</h6>
                </div>
              </div>
              <div class="card-body px-0 pb-2">
                <div class="table-responsive sch p-0">
                  <table class="table table-acc align-items-center mb-0">
                    <thead>
                      <tr>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Client Name</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Service Type</th>
                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Requested Date</th>
                        <th class="text-secondary opacity-7"></th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($works as $work)
                        <tr>
                          <td>
                            <div class="d-flex px-2 py-1">
                              <div class="d-flex flex-column justify-content-center">
                                <h6 class="text-xs text-secondary mb-0">{{ $work->reff_no }}</h6>
                                <h6 class="mb-0 text-sm">{{ $work->cust->name }}&nbsp;{{ $work->cust->last_name }}</h6>
                                <p class="text-xs text-secondary mb-0">{{ $work->cust->email }}</p>
                              </div>
                            </div>
                          </td>
                          <td>
                            <p class="text-xs font-weight-bold mb-0">{{ $work->service->service_name }}</p>
                            {{-- <p class="text-xs text-secondary mb-0">Bi Weekly</p> --}}
                          </td>
                          <td class="align-middle text-center">
                            <span class="text-secondary text-xs font-weight-bold">{{ date('M d, Y', strtotime($work->schedule->req_date)) }}</span>
                          </td>
                          <td class="align-middle">
                            <button type="button" class="btn btn-link p-0 m-0" id="showDetails" data-id="{{ $work->id }}">
                              Assign
                            </button>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
              <div class="bg-gradient-success shadow-success border-radius-lg pt-4 pb-3">
                <h6 class="text-white text-capitalize ps-3">Assigned Works</h6>
              </div>
            </div>
            <div class="card-body px-0 pb-2">
              <div class="table-responsive asg p-0">
                <table class="table table-asg align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Client Name</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Service Type</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Scheduled Date</th>
                      {{-- <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Assignee</th> --}}
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Status</th>
                      <th class="text-secondary opacity-7"></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($assignedWorks as $item)
                      <tr>
                        <td>
                          <div class="d-flex px-2 py-1">
                            <div class="d-flex flex-column justify-content-center">
                              <h6 class="text-xs text-secondary mb-0">{{ $item->reff_no }}</h6>
                              <h6 class="mb-0 text-sm">{{ $item->cust->name }}&nbsp;{{ $item->cust->last_name }}</h6>
                              <p class="text-xs text-secondary mb-0">{{ $item->cust->email }}</p>
                            </div>
                          </div>
                        </td>
                        <td>
                          <p class="text-xs font-weight-bold mb-0">{{ $item->service->service_name }}</p>
                        </td>
                        <td class="align-middle text-center">
                          <span class="text-secondary text-xs font-weight-bold">{{ date('M d, Y', strtotime($item->schedule->sch_date)) }}</span>
                        </td>
                        <td>
                          <p class="text-xs text-secondary mb-0 text-center">{{ Helper::generateWorkStatus($item->status) }}</p>
                        </td>
                        <td class="align-middle text-center">
                          <button type="button" class="btn btn-link p-0 m-0" id="assignDetails" data-id="{{ $item->id }}">
                            Edit
                          </button>
                          <button type="button" class="btn btn-link text-secondary p-0 m-0" onclick="window.location.href='{{ route('workImages.show',$item->id) }}';" >
                            Manage Photos
                          </button>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Details -->
    <div class="modal modal-lg fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title font-weight-normal" id="detailsHeader"></h5>
            <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-6">
                <ul class="list-group">
                  <li class="list-group-item border-0 p-4 mb-2 bg-gray-100 border-radius-lg">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="input-group input-group-static mb-4">
                            <label >Requested Date</label>
                            <input type="text" class="form-control" id="req_date" readonly>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="input-group input-group-static mb-4">
                            <label >Requested Time</label>
                            <input type="text" class="form-control" id="req_time" readonly>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="list-group-item border-0 p-4 mb-2 bg-gray-100 border-radius-lg">
                    <div class="input-group input-group-static mb-4">
                        <label >Work / Pick Up Location</label>
                        <input type="text" class="form-control" id="pickUpLoc" readonly>
                    </div>
                    <div class="input-group input-group-static mb-4">
                        <label >Drop Off Location</label>
                        <input type="text" class="form-control" id="dropOffLoc" readonly>
                    </div>
                    <div class="input-group input-group-static mb-4">
                        <label >Customer's Note</label>
                        <input type="text" class="form-control" id="cust_notes" readonly>
                    </div>
                    <div class="input-group input-group-static mb-4">
                        <label >Known Preference</label>
                        <input type="text" class="form-control" id="known_pref" readonly>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul class="list-group">
                  {{-- List group if details clicked from accepted works card --}}
                  <li class="list-group-item border-0 p-4 mb-2 border-radius-lg assign" style="display: none">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="input-group input-group-static mb-4">
                          <label >Scheduling Date</label>
                          <input type="text" class="form-control" id="sch_date_asg" readonly>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="input-group input-group-static mb-4">
                          <label>Scheduling Time</label>
                          <input type="text" class="form-control" id="sch_time_asg" readonly>
                        </div>
                      </div>
                    </div>
                  </li>
                  {{-- end --}}
                  {{-- List group if details clicked from assigned works card --}}
                  <li class="list-group-item border-0 p-4 mb-2 border-radius-lg">
                    <div class="input-group input-group-static mb-4 employee">
                        <label id="asgLbl">Scheduling Date</label>
                        <input type="datetime-local" class="form-control" id="sch_date" name="sch_date" autofocus>
                    </div>
                    <div class="input-group input-group-static mb-4 employee">
                        <label for="employees" class="ms-0">Assign to (hold ctrl+click to choose multiple)</label>
                        <select multiple class="form-control employee" id="employees" name="employees">
                          @foreach ($employees as $employee)
                            <option value="{{ $employee->id }}">{{ $employee->name }}&nbsp;{{ $employee->last_name }}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="input-group input-group-static mb-4">
                      <label for="job_desc">Job Description</label>
                      <textarea class="form-control" rows="5" id="job_desc" spellcheck="false"></textarea>
                    </div>
                  </li>
                  {{-- end --}}
                </ul>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            @csrf
            <button type="button" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="button" class="btn bg-gradient-info act-ready" data-acc="assign" style="display: none"></button>
            <button type="button" class="btn bg-gradient-success act-edit" data-acc="assign" style="display: none">Save Edit</button>
            <button type="button" class="btn bg-gradient-success act-assign" data-acc="assign">Save</button>
          </div>
        </div>
      </div>
    </div>

@endsection

@section('javascript')
    <script>

      $('.table').dataTable({
          "info":false,
          "lengthChange": false,
          "pageLength": 10,
          language: {
            paginate: {
              next: '<i class="fa fa-angle-right"></i>', // or '→'
              previous: '<i class="fa fa-angle-left"></i>' // or '←' 
            }
          },
          "drawCallback": function () {
            $('.dataTables_paginate ul.pagination').addClass('pagination-success');
          }
      });

      $('body').on('click', '#showDetails', function (event) {

          event.preventDefault();
          var id = $(this).data('id');
          $.ajax({
            type: 'GET',
            dataType: 'JSON',
            url: '/works/'+id,
            success:function(data){
              var header = data.user.name+' '+data.user.last_name+' -- '+data.serv;
              
              $('#detailsModal').modal('show');
              $('#req_date').val(data.date);
              $('#req_time').val(data.time);
              $('#pickUpLoc').val(data.address);
              if (data.dropOff) {
                $('#dropOffLoc').val(data.dropOff);
              }
              document.getElementById('detailsHeader').innerHTML = header;
              $('#cust_notes').val(data.data.cust_notes);
              $('#known_pref').val(data.data.known_pref);
              $('button.act-assign').attr('data-id',data.data.id);
            }
          })
      });
        
      $('body').on('click', '#assignDetails', function (event) {
        
        event.preventDefault();
        var id = $(this).data('id');
        $('.assign').addClass('bg-gray-100').css('display', '');
        $('button.act-ready').css('display','');
        $('button.act-edit').css('display','');
        $('#asgLbl').html('New Scheduling Date');
        $('button.act-assign').css('display', 'none' );
        $('button.act-edit').data('id',id);
        $('button.act-ready').data('id',id);
        $.ajax({
          type: 'GET',
          dataType: 'JSON',
          url: '/works/'+id,
          success:function(data){
            var header = data.user.name+' '+data.user.last_name+' -- '+data.serv;
            
            $('#detailsModal').modal('show');
            $('#req_date').val(data.date);
            $('#req_time').val(data.time);
            $('#pickUpLoc').val(data.address);
            if (data.dropOff) {
              $('#dropOffLoc').val(data.dropOff);
            }
            if (data.sch_date) {
              $('#sch_date_asg').val(data.sch_date);
              $('#sch_time_asg').val(data.sch_time);
              $('#sch_date').val(data.data.schedule.sch_date);
            }
            if (data.employees) {
              $('#assigned_to').val(data.employees);
            }
            var values=data.employeesId;
            $.each(values.split(","), function(i,e){
                $("#employees option[value='" + e + "']").prop("selected", true);
            });
            if (data.data.job_desc) {
              $('#job_desc').val(data.data.job_desc);
            }
            if(data.data.status)
            {
              if (data.data.status == 'Ready') {
                $('button.act-ready').data('status','1');
                $('button.act-ready').html('Mark As Not Ready');
              }
              else{
                $('button.act-ready').data('status','0');
                $('button.act-ready').html('Mark As Ready');
              }
            }
            document.getElementById('detailsHeader').innerHTML = header;
            $('#cust_notes').val(data.data.cust_notes);
            $('#known_pref').val(data.data.known_pref);
            $('button.act-assign').data('id',data.data.id);
          }
        })
      });
      

      $('#detailsModal').on('hidden.bs.modal', function (e) {
          $(this).find("input,textarea,select").val('').end();
          $('button.act-assign').css('display', '' );
          $('.assign').removeClass('bg-gray-100').css('display', 'none');
          $('#asgLbl').html('Scheduling Date');
          $('button.act-edit').css('display','none');
          $('button.act-ready').css('display','none');
      })

      
      $('body').on('click', '.act-assign', function(event){
        event.preventDefault();
        var token = $('input[name="_token"]').val();
        var sch = $('#sch_date').val();
        var employees = $('#employees').val();
        var job_desc = $('#job_desc').val();
        var work = $(this).data('id');
        var status = $(this).data('acc');
        $.ajax({
            type: 'PUT',
            dataType: 'JSON',
            url: '/works/'+work,
            data: {
                _token: token,
                'sch_date': sch,
                'employees': employees,
                'job_desc': job_desc,
                'status': status
            },
            success:function(data){
                if (data.success){
                  $('#detailsModal').modal('hide');
                  location.reload();
                }
            }
        })
      });
      
      $('body').on('click', '.act-ready', function(event){
        event.preventDefault();
        //check status first
        //then
        //change status to ready or not ready
          var idx = $(this).data('id');
          var status = $(this).data('status');
          
          $.ajax({
              type: 'GET',
              dataType: 'JSON',
              url: '{{ route('works.status','idx') }}',
              data: {
                  'status': status,
                  'id' : idx,
              },
              success:function(data){
                $('#detailsModal').modal('hide');
                location.reload();
              }
          })
      });

      $('body').on('click', '.act-edit', function(event) {
        event.preventDefault();
        var token = $('input[name="_token"]').val();
        var sch = $('#sch_date').val();
        var employees = $('#employees').val();
        var job_desc = $('#job_desc').val();
        var work = $(this).data('id');
        $.ajax({
            type: 'PUT',
            dataType: 'JSON',
            url: '/works/'+work+'/edit',
            data: {
                _token: token,
                'sch_date': sch,
                'employees': employees,
                'job_desc': job_desc,
                'status': status,
                'work': work
            },
            success:function(data){
                if (data.success){
                  $('#detailsModal').modal('hide');
                  location.reload();
                }
            }
        })
      });

    </script>
@endsection