@extends('layouts.template')

@section('css')
  <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid px-2 px-md-4">

  <form action="{{ route('works.store') }}" method="POST">
    @csrf
    <div class="row p-4 justify-content-center">
      @can('access admin page')
      <div class="col-lg-4">
        <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="nav-wrapper position-relative end-0">
                    <ul class="nav nav-pills nav-fill p-1" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link mb-0 px-0 py-1 active " data-bs-toggle="tab" data-bs-target="#savedCust" aria-controls="todays" aria-selected="true" href="javascript:;" role="tab">
                                <i class="material-icons text-lg position-relative">person</i>
                                <span class="ms-1">Saved Cust</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mb-0 px-0 py-1 " data-bs-toggle="tab" data-bs-target="#newCust" aria-controls="awaiting" href="javascript:;" role="tab" aria-selected="false">
                                <i class="material-icons text-lg position-relative">person_add</i>
                                <span class="ms-1">New Cust</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card-body border card-plain border-radius-lg d-flex mt-2">
              <div class="tab-content">
                {{-- Tab Pane 1 --}}
                <div class="tab-pane fade show active" id="savedCust" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="input-group input-group-static mb-4">
                          <label for="cust_id" class="ms-0">Cust Name</label>
                          <select class="form-control" id="cust_id" name="cust_id" style="width: 100%" required>
                                  <option value="">-- Select Cust --</option>
                              @foreach ($users as $item)
                                  <option value="{{ $item->id }}">{{ $item->name }}&nbsp;{{ $item->last_name }}</option>
                              @endforeach
                          </select>
                        </div>
                      <input type="hidden" class="form-control savedAddress" name="savedLoc" id="savedLoc" disabled>
                      <input type="hidden" class="form-control savedAddress" name="savedTown" id="savedTown" disabled>
                      <input type="hidden" class="form-control savedAddress" name="savedProv" id="savedProv" disabled>
                      <input type="hidden" class="form-control savedAddress" name="savedPostal" id="savedPostal" disabled>
                    </div>
                  </div>
                </div>
                {{-- Tab Pane 2 --}}
                <div class="tab-pane fade" id="newCust" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
                    <div class="row">
                        <div class="col-md-6">
                          <div class="input-group input-group-static mb-4">
                            <label for="name">First Name</label>
                            <input type="text" class="form-control new-cust" name="name" id="name" disabled>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="input-group input-group-static mb-4">
                            <label for="last_name">Last Name</label>
                            <input type="text" class="form-control new-cust" name="last_name" id="last_name" disabled>
                          </div>
                        </div>
                        <div class="input-group input-group-static mb-4">
                          <label for="email">Email</label>
                          <input type="email" class="form-control new-cust" name="email" id="email" disabled>
                        </div>
                        <div class="input-group input-group-static mb-4">
                          <label for="phone_number">Phone Number</label>
                          <input type="text" class="form-control new-cust" name="phone_number" id="phone_number" disabled>
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </div>
      </div>
      @endcan
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header">
            <div class="row">
              <h6 class="mb-0">Works Order</h6>
            </div>
          </div>
          <div class="card-body">
            @can('access customer page')
              @if ($savedAddress)
              <input type="hidden" class="form-control savedAddress" name="savedLoc" id="savedLoc" value="{{ $savedAddress->address }}">
              <input type="hidden" class="form-control savedAddress" name="savedTown" id="savedTown" value="{{ $savedAddress->town }}">
              <input type="hidden" class="form-control savedAddress" name="savedProv" id="savedProv" value="{{ $savedAddress->province }}">
              <input type="hidden" class="form-control savedAddress" name="savedPostal" id="savedPostal" value="{{ $savedAddress->postal }}">
              @endif
            @endcan
            <x-forms.req-work-input :categories="$categories"/>
          </div>
        </div>
      </div>
    </div>
    <div class="row p-4">
      <div class="card">
          <div class="card-body p-3 pb-0">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" id="newCustToggle" name="newCust" style="display: none" >
            </div>
            <button class="btn bg-gradient-success w-100" type="submit">Save Order</button>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection

@section('javascript')
<script src="{{ asset('js/plugins/select2.min.js') }}"></script>

<script>
  //select2 init
  $('#cust_id').select2({
    placeholder: 'Choose Customer Name',
    dropdownCssClass: "form-control",
    allowClear: true
  });

  $("ul.nav-pills > li > a").click(function() {
    let check = $(this).attr("data-bs-target").replace("#","");
    if (check == 'newCust') {
      //new cust
      $( "#newCustToggle" ).prop("checked", true );
      $( "#cust_id" ).prop("disabled", true );
      $( "#cust_id" ).attr("required", false );
      $( 'input.new-cust' ).attr("required", true);
      $( 'input.new-cust' ).prop("disabled", false );
      $('.pickUp').val('');
      $('.drop-off').val('');
      $('input.defaultAddress').val('');
      $('.use-default').hide();
      $('.save-default').show();
    }
    else
    {
      //existing cust
      $( "#newCustToggle" ).prop("checked", false );
      $( "#cust_id" ).prop("disabled", false );
      $( "#cust_id" ).attr("required", true );
      $( 'input.new-cust' ).attr("required", false );
      $( 'input.new-cust' ).prop("disabled", true );
      $('input.defaultAddress').val('');
      $('.use-default').show();
      $('.save-default').hide();
    }
  });
  
  $('#cust_id').on('change', function() {
    event.preventDefault();
      $('.pickUp').val('');
      $('.drop-off').val('');
      $('input.savedAddress').val('');
      $('#defaultAddress2').prop("checked", false)
      $('#defaultAddress').prop("checked", false)
          var id = $('#cust_id option:selected').val();
          $.ajax({
              type: 'GET',
              dataType: 'JSON',
              url: '/works/getSavedAddress/'+id,
              data: {
                  'id': id
              },
              success:function(data){
                if (data.data == null) {
                  // $('input:checkbox.use-default').prop("disabled", true);
                } 
                else {
                  // $('input:checkbox.use-default').prop("disabled", false);
                  $('#savedLoc').val(data.data.address);
                  $('#savedTown').val(data.data.town);
                  $('#savedProv').val(data.data.province);
                  $('#savedPostal').val(data.data.postal);
                }
              }
          })
  });

  //toggle use default address
  $('#defaultAddress').on('change', function() {
    if ($('#defaultAddress').prop("checked") == true ) {
      $('#defaultAddress2').prop("checked", false)
      $('#defaultAddress').prop("checked", true)
      $('.drop-off').val('');
      $('#pickUpLoc').val($('#savedLoc').val());
      $('#pickUpTown').val($('#savedTown').val());
      $('#pickUpProv').val($('#savedProv').val());
      $('#pickUpPostal').val($('#savedPostal').val());
    } 
    else {
      $('#defaultAddress').prop("checked", false)
      $('.pickUp').val('');
    }
    
  });
  $('#defaultAddress2').on('change', function() {
    if ($('#defaultAddress2').prop("checked")== true) {
      $('#defaultAddress').prop("checked", false)
      $('#defaultAddress2').prop("checked", true)
      $('.pickUp').val('');
      $('#dropOffLoc').val($('#savedLoc').val());
      $('#dropOffTown').val($('#savedTown').val());
      $('#dropOffProv').val($('#savedProv').val());
      $('#dropOffPostal').val($('#savedPostal').val());
    } 
    else {
      $('#defaultAddress2').prop("checked", false)
      $('.drop-off').val('');
    }
    
  });

  //toggle save as default address
  $('#saveAddress').on('change', function() {
    if ($('#saveAddress').prop("checked") == true ) {
      $('#saveAddress2').prop("checked", false)
      $('#saveAddress').prop("checked", true)
    } 
    else {
      $('#saveAddress').prop("checked", false)
    }
    
  });

  $('#saveAddress2').on('change', function() {
    if ($('#saveAddress2').prop("checked") == true ) {
      $('#saveAddress').prop("checked", false)
      $('#saveAddress2').prop("checked", true)
    } 
    else {
      $('#saveAddress2').prop("checked", false)
    }
    
  });

  $('#categories').on('change', function() {
    let serv = $( "#categories option:selected" ).text();
    let result = serv.match(/transport/gi);
    if (result != null) {
      $('.drop-off').show();
      $('input.drop-off:text').prop('disabled', false);
      $('#saveAddress2').prop('disabled', false);
    }
    else{
      //unselect transport 
      $('.drop-off').hide();
      $('input.drop-off:text').prop('disabled', true);
      $('#saveAddress2').prop('disabled', true);
    }

    var id = this.value;
    $.ajax({
        type: 'GET',
        dataType: 'JSON',
        url: '/works/fetch-service/'+id,
        data: {
            'categoryId': id
        },
        success:function(data){
          if (data.services.length > 0) {
            $('#service').html('<option value="">-- Select Service --</option>');
          } else {
            $('#service').html('<option value="">-- Coming Soon --</option>');
          }
          $.each(data.services, function (key, value) {
              $("#service").append('<option value="' + value.id + '">' + value.service_name + '</option>');
          });
        }
    })
  });
</script>
@endsection