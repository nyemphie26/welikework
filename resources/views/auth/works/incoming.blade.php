@extends('layouts.template')

@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="card my-4">
              <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-success shadow-success border-radius-lg pt-4 pb-3">
                  <h6 class="text-white text-capitalize ps-3">Incoming Works</h6>
                </div>
              </div>
              <div class="card-body px-0 pb-2">
                <div class="row p-3">
                  <div class="col-6"></div>
                  <div class="col-6 text-end">
                    <a href="{{ route('works.create') }}">
                      <button class="btn bg-gradient-primary" type="button">New Order</button>
                    </a>
                  </div>
                </div>
                <div class="table-responsive p-0">
                  <table class="table align-items-center mb-0">
                    <thead>
                      <tr>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Reffence No.</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Client Name</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Service Type</th>
                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Requested Date</th>
                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Works / Pick Up Location</th>
                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Drop Off Location</th>
                        <th class="text-secondary opacity-7"></th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($works as $work)
                        <tr>
                          <td class="align-middle text-center">
                            <span class="text-secondary text-xs font-weight-bold">{{ $work->reff_no }}</span>
                          </td>
                          <td>
                            <div class="d-flex px-2 py-1">
                              <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">{{ $work->cust->name }}&nbsp;{{ $work->cust->last_name }}</h6>
                                <p class="text-xs text-secondary mb-0">{{ $work->cust->email }}</p>
                              </div>
                            </div>
                          </td>
                          <td>
                            <p class="text-xs font-weight-bold mb-0">{{ $work->service->service_name }}</p>
                            {{-- <p class="text-xs text-secondary mb-0">Bi Weekly</p> --}}
                          </td>
                          <td class="align-middle text-center">
                            <span class="text-secondary text-xs font-weight-bold">{{ date('M d, Y -- H:i',strtotime($work->schedule->req_date)) }}</span>
                          </td>
                          <td class="align-middle text-center text-sm">
                            {{-- <span class="badge badge-sm bg-gradient-success">Online</span> --}}
                            <p class="text-xs font-weight-bold mb-0">{{ $work->location->address }}</p>
                          </td>
                          <td class="align-middle text-center text-sm">
                            <p class="text-xs font-weight-bold mb-0">{{ $work->dropOffLocation ? $work->dropOffLocation->address : '-' }}</p>
                          </td>
                          <td class="align-middle">
                            <button type="button" class="btn btn-link p-0 m-0" id="showDetails" data-id="{{ $work->id }}">
                              Details
                            </button>
                            {{-- <a class="btn btn-link pe-3 ps-0 mb-0 ms-auto w-25 w-md-auto" href="{{ route('works.show',$work) }}">Details</a> --}}
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
    </div>

    
    <!-- Modal -->
    <div class="modal modal-lg fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title font-weight-normal" id="detailsHeader"></h5>
            <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="formDetails">
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-4">
                <div class="input-group input-group-static mb-4">
                    <label >Customer's Name</label>
                    <input type="text" class="form-control" id="name" readonly>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="input-group input-group-static mb-4">
                    <label >Email</label>
                    <input type="text" class="form-control" id="email" readonly>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="input-group input-group-static mb-4">
                    <label >Phone Number</label>
                    <input type="text" class="form-control" id="phone" readonly>
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="input-group input-group-static mb-4">
                        <label >Service Type</label>
                        <input type="text" class="form-control" id="service" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                  <div class="input-group input-group-static mb-4">
                      <label >Requested Date</label>
                      <input type="text" class="form-control" id="reqDate" readonly>
                  </div>
                </div>
                <div class="col-lg-3">
                    <div class="input-group input-group-static mb-4">
                        <label >Requested Time</label>
                        <input type="text" class="form-control" id="reqTime" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="input-group input-group-static mb-4">
                      <label>Total Fees</label>
                      <span class="input-group-text">$</span>
                      <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" name="total_fees" id="total_fees" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group input-group-static mb-4">
                        <label >Works / Pick Up Address</label>
                        <input type="text" class="form-control" id="pickUp" readonly>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="input-group input-group-static mb-4">
                        <label >Drop Off Address</label>
                        <input type="text" class="form-control" id="dropOff" readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group input-group-static mb-4">
                        <label >Customer's Note</label>
                        <textarea class="form-control" rows="3" spellcheck="false" id="custNotes" readonly></textarea>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="input-group input-group-static mb-4">
                        <label >Known Preference</label>
                        <textarea class="form-control" rows="3" spellcheck="false" id="knownPref" readonly></textarea>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            @csrf
            <button type="button" class="btn bg-gradient-danger action" data-acc="reject">Reject</button>
            <button type="button" class="btn bg-gradient-secondary action" data-acc="pending" data-bs-dismiss="modal">Pending</button>
            <button type="button" class="btn bg-gradient-success action" data-acc="accept">Accept</button>
          </div>
          </form>
        </div>
      </div>
    </div>
@endsection

@section('javascript')
    <script>

      $(document).ready(function () {
        $('.table').dataTable({
          "info":false,
          language: {
            paginate: {
              next: '<i class="fa fa-angle-right"></i>', // or '→'
              previous: '<i class="fa fa-angle-left"></i>' // or '←' 
            }
          },
          "drawCallback": function () {
            $('.dataTables_paginate ul.pagination').addClass('pagination-success');
          }
        });

        $('body').on('click', '#showDetails', function (event) {

            event.preventDefault();
            var id = $(this).data('id');
            $.get('works/'+id, function (data) {
                var header = data.data.reff_no+' -- '+data.serv+' ('+data.date+' '+data.time+')';
                $('#detailsModal').modal('show');
                $('#name').val(data.user.name+' '+data.user.last_name);
                $('#email').val(data.user.email);
                $('#phone').val(data.user.phone_number);
                $('#service').val(data.serv);
                $('#reqDate').val(data.date);
                $('#reqTime').val(data.time);
                $('#pickUp').val(data.address);
                if (data.dropOff) {
                  $('#dropOff').val(data.dropOff);
                }
                document.getElementById('detailsHeader').innerHTML = header;
                $('#custNotes').val(data.data.cust_notes);
                $('#knownPref').val(data.data.known_pref);
                $('button.action').attr('data-id',data.data.id);
                // $('.formDetails').attr('action','/works/'+id+'?act='+id);
            })
        });

        $('#detailsModal').on('hidden.bs.modal', function (e) {
          $(this).find("input,textarea").val('').end();
        })

        
        $('body').on('click', '.action', function(event){
          event.preventDefault();
          var token = $('input[name="_token"]').val();
          var fees = $('#total_fees').val();
          var work = $(this).data('id');
          var status = $(this).data('acc');
          if (fees == '') {
            alert('Input Fees!');
          } else {
            $.ajax({
                type: 'PUT',
                dataType: 'JSON',
                url: '/works/'+work,
                data: {
                    _token: token,
                    'fees': fees,
                    'status': status
                },
                success:function(data){
                    if (data.success){
                      $('#detailsModal').modal('hide');
                      location.reload();
                    }
                }
            })
          }
        });
        
      }); 

    </script>
@endsection