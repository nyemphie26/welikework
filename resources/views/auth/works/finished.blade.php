@extends('layouts.template')
@section('content')
    <div class="container-fluid py-4">
        {{-- <div class="row">
            <div class="page-header min-height-300 border-radius-xl mt-4" style="background-image: url('https://images.unsplash.com/photo-1531512073830-ba890ca4eba2?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1920&q=80');">
                <span class="mask  bg-gradient-primary  opacity-6"></span>
              </div>
        </div> --}}
        <div class="row">
            <div class="col-lg-12">
                <div class="card my-4">
                  <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-success shadow-success border-radius-lg pt-4 pb-3">
                      <h6 class="text-white text-capitalize ps-3">Finished Works</h6>
                    </div>
                  </div>
                  <div class="card-body px-0 pb-2">
                    <div class="row p-3">
                      <div class="col-6"></div>
                      <div class="col-6 text-end">
                        {{-- <a href="{{ route('Incoming Works Edit Add') }}">
                          <button class="btn bg-gradient-primary" type="button">New Order</button>
                        </a> --}}
                      </div>
                    </div>
                    <div class="table-responsive p-1">
                      <table class="table align-items-center mb-0">
                        <thead>
                          <tr>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Reffeence No.</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Client Name</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Service Type</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Requested Date</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Actual Start</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Actual End</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Grand Total</th>
                            <th class="text-secondary opacity-7"></th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($works as $work)
                                <tr>
                                <td>
                                    <p class="text-xs font-weight-bold mb-0">{{ $work->reff_no }}</p>
                                </td>
                                <td>
                                    <div class="d-flex px-2 py-1">
                                    <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0 text-sm">{{ $work->cust->name }}&nbsp;{{ $work->cust->last_name }}</h6>
                                        <p class="text-xs text-secondary mb-0">{{ $work->cust->email }}</p>
                                    </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="text-xs font-weight-bold mb-0">{{ $work->service->service_name }}</p>
                                </td>
                                <td class="align-middle text-center">
                                    <span class="text-secondary text-xs font-weight-bold">{{ date('M d, Y - H:i', strtotime($work->schedule->sch_date)) }}</span>
                                </td>
                                <td class="align-middle text-center">
                                    <span class="text-secondary text-xs font-weight-bold">{{ date('M d, Y - H:i', strtotime($work->schedule->actual_start)) }}</span>
                                </td>
                                <td class="align-middle text-center">
                                    <span class="text-secondary text-xs font-weight-bold">{{ date('M d, Y - H:i', strtotime($work->schedule->actual_stop)) }}</span>
                                </td>
                                <td class="align-middle text-center">
                                    <span class="text-secondary text-xs font-weight-bold">${{ Helper::grandTotal($work->id) }}</span>
                                </td>
                                <td class="align-middle">
                                    <button type="button" class="btn btn-link p-0 m-0" id="showDetails" data-id="{{ $work->id }}">
                                    Details
                                    </button>
                                    <button type="button" class="btn btn-link text-secondary p-0 m-0" onclick="window.location.href='{{ route('workImages.show',$work->id) }}';" >
                                      Manage Photos
                                    </button>
                                </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal modal-lg fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title font-weight-normal" id="detailsHeader">John Michael - Mowing - Req Date ( 8/18/2022 )</h5>
            <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-lg-3">
                        <div class="input-group input-group-static mb-4">
                            <label >Request Date</label>
                            <input type="text" class="form-control" id="req_date" readonly>
                        </div>
                </div>
                <div class="col-lg-3">
                    <div class="input-group input-group-static mb-4">
                        <label >Schedule Date</label>
                        <input type="text" class="form-control" id="sch_date" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="input-group input-group-static mb-4">
                        <label >Actual Start</label>
                        <input type="text" class="form-control" id="start_date" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="input-group input-group-static mb-4">
                        <label >Actual End</label>
                        <input type="text" class="form-control" id="end_date" readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group input-group-static mb-4">
                        <label >Works / Pick Up Location</label>
                        <input type="text" class="form-control" id="pickUpLoc" readonly>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="input-group input-group-static mb-4">
                        <label >Drop Off Location</label>
                        <input type="text" class="form-control" id="dropOffLoc" readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="input-group input-group-static mb-4">
                        <label >Customer's Note</label>
                        <textarea class="form-control" rows="3" id="cust_notes" spellcheck="false" readonly></textarea>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="input-group input-group-static mb-4">
                        <label >Known Preference</label>
                        <textarea class="form-control" rows="3" id="known_pref" spellcheck="false" readonly></textarea>
                    </div>
                </div>
                <div class="col-lg-4">
                  <div class="input-group input-group-static mb-4">
                    <label>Job Description</label>
                    <textarea class="form-control" rows="3" id="job_desc" spellcheck="false" readonly></textarea>
                  </div>
                </div>
            </div>
            <div class="row">
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group input-group-static mb-4">
                        <label >Assigne</label>
                        <textarea class="form-control" rows="3" id="employee" spellcheck="false" readonly></textarea>
                    </div>
                </div>
                <div class="col-lg-6">
                  <div class="input-group input-group-static mb-4">
                    <label>Employee Notes</label>
                    <textarea class="form-control" rows="3" id="employee_notes" spellcheck="false" readonly></textarea>
                  </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                  <div class="input-group input-group-static mb-4">
                      <label>Total</label>
                      <input type="text" class="form-control" id="total_fees" readonly>
                  </div>
                </div>
                <div class="col-lg-3">
                    <div class="input-group input-group-static mb-4">
                      <label >Tax</label>
                      <input type="text" class="form-control" id="tax" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="input-group input-group-static mb-4">
                      <label >Tipping Fee</label>
                      <input type="text" class="form-control" id="tipping" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="input-group input-group-static mb-4">
                      <label >Grand Total</label>
                      <input type="text" class="form-control" id="grand_total" readonly>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer justify-content-center">
            <button type="button" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('javascript')
    <script>

      $(document).ready(function () {
        $('.table').dataTable({
          "info":false,
          "lengthChange": false,
          "pageLength": 10,
          language: {
            paginate: {
              next: '<i class="fa fa-angle-right"></i>', // or '→'
              previous: '<i class="fa fa-angle-left"></i>' // or '←' 
            }
          },
          "drawCallback": function () {
            $('.dataTables_paginate ul.pagination').addClass('pagination-success');
          }
        });
        
        $('body').on('click', '#showDetails', function (event) {

            event.preventDefault();
            var id = $(this).data('id');
            $.ajax({
              type: 'GET',
              dataType: 'JSON',
              url: '/works/'+id,
              success:function(data){
                var header = data.user.name+' '+data.user.last_name+' -- '+data.serv;
                $('#detailsModal').modal('show');
                $('#req_date').val(data.date+' - '+data.time);
                $('#sch_date').val(data.sch_date+' - '+data.sch_time);
                $('#start_date').val(data.start);
                $('#end_date').val(data.stop);
                $('#pickUpLoc').val(data.address);
                if (data.dropOff) {
                  $('#dropOffLoc').val(data.dropOff);
                }
                document.getElementById('detailsHeader').innerHTML = header;
                $('#cust_notes').val(data.data.cust_notes);
                $('#known_pref').val(data.data.known_pref);
                $('#job_desc').val(data.data.job_desc);
                $('#employee_notes').val(data.data.employee_notes);
                $('#employee').val(data.employees);
                $('#total_fees').val('$'+data.billing.total_fees);
                $('#tax').val('$'+data.billing.tax);
                $('#tipping').val('$'+data.billing.tipping);
                $('#grand_total').val('$'+data.grandTotal);
                $('button.act-assign').attr('data-id',data.data.id);
              }
            })
          });
          
        $('#detailsModal').on('hidden.bs.modal', function (e) {
            $(this).find("input,textarea,select").val('').end();
            $('button.act-assign').css('display', '' );
            $('.assign').removeClass('bg-gray-100').css('display', 'none');
            $('#asgLbl').html('Scheduling Date');
            $('button.act-edit').css('display','none');
            $('button.act-ready').css('display','none');
        })

      }); 

    </script>
@endsection