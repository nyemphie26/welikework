<table class="table align-items-center mb-0">
    <thead>
      <tr>
        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Reffeence No.</th>
        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Client Name</th>
        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Service Type</th>
        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Total Fees</th>
        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Tax</th>
        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Tipping Fees</th>
        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Grand Total</th>
        <th class="text-secondary opacity-7"></th>
      </tr>
    </thead>
    <tbody>
        @php
            $grandFees = 0;
            $grandTax = 0;
            $grandTip = 0;
            $grandTotal = 0;
        @endphp
        @foreach ($works as $item)
            <tr>
                <td>{{ $item->reff_no }}</td>
                <td>{{ $item->cust->name }}&nbsp;{{ $item->cust->last_name }}</td>
                <td>{{ $item->service->service_name }}</td>
                <td>{{ $item->billing->total_fees }}</td>
                <td>{{ $item->billing->tax }}</td>
                <td>{{ $item->billing->tipping }}</td>
                <td>{{ $total = Helper::grandTotal($item->id) }}</td>
            </tr>
            @php
                $grandFees = $grandFees + $item->billing->total_fees;
                $grandTax = $grandTax + $item->billing->tax;
                $grandTip = $grandTip + $item->billing->tipping;
                $grandTotal = $grandTotal + $total;
            @endphp
        @endforeach
            <tr>
                <td colspan="3">Grand Total</td>
                <td>{{ $grandFees }}</td>
                <td>{{ $grandTax }}</td>
                <td>{{ $grandTip }}</td>
                <td>{{ $grandTotal }}</td>
            </tr>
    </tbody>
  </table>