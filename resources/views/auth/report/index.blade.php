@extends('layouts.template')

@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="card my-4">
              <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-success shadow-success border-radius-lg pt-4 pb-3">
                  <h6 class="text-white text-capitalize ps-3">Incoming Works</h6>
                </div>
              </div>
              <div class="card-body px-0 pb-2">
                <div class="row p-3">
                  <div class="col-4">
                    <div class="input-group input-group-static my-3">
                        <label for="req_date">Start Date</label>
                        <input type="date" class="form-control" name="start_date" id="start_date">
                      </div>
                  </div>
                  <div class="col-4">
                    <div class="input-group input-group-static my-3">
                        <label for="req_date">End Date</label>
                        <input type="date" class="form-control" name="end_date" id="end_date">
                      </div>
                  </div>
                  <div class="col-4">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="status" id="finished" value="finished" checked>
                        <label class="form-check-label" for="finished">
                          Finished
                        </label>
                      </div>
                      {{-- <div class="form-check">
                        <input class="form-check-input" type="radio" name="status" id="unfinished" value="unfinished">
                        <label class="form-check-label" for="unfinished">
                          Unfinished
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="status" id="rejected" value="rejected">
                        <label class="form-check-label" for="rejected">
                          Rejected
                        </label>
                      </div> --}}
                  </div>
                </div>
                <div class="row p-3">
                    <div class="col-12">
                      <a href="">
                        <button class="btn bg-gradient-primary w-100" type="button" id="showReport">Show Reporting</button>
                      </a>
                    </div>
                </div>
                
              </div>
            </div>
        </div>
        <div class="row report-card" style="display: none">
            <div class="card my-4">
                <div class="card-header">
                    <a class="btn bg-gradient-success w-100" type="button" id="export" href="">Export This to Excel</a>
                </div>
                <div class="card-body report-tabs">
                    <table class="table align-items-center mb-0">
                        <thead>
                          <tr>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Reffeence No.</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Client Name</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Service Type</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Total Fees</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Tax</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Tipping Fees</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Grand Total</th>
                            <th class="text-secondary opacity-7"></th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')
    <script>

    $(document).ready(function () {
        
        $("#allStatus").click(function () {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
        

        $('input.status').on('change', function (e) {
            // console.log($('input.status:checked').length);
            if ($('input.status:checked').length == $('input.status').length) {
                $('#allStatus').prop('checked', true);
            } 
            else {
                $('#allStatus').prop('checked', false);
            }
        });

        
        
    }); 

    $('#showReport').on('click', function (e) {
        e.preventDefault();
        var start_date = $('#start_date').val(),
            end_date = $('#end_date').val(),
            diff = Date.parse(end_date) - Date.parse(start_date),
            days = (diff/1000/60/60/24)+1,
            status = $('input:checked').val();
        $("table tbody").html("");

        if (days < 1 ) {
            alert('End Date Should Greater than Start Date');
        } 
        else if (days > 31) {
            alert('Date Range Should No More than 31 days');
        }
        else {
            $.ajax({
                type: 'GET',
                dataType: 'JSON',
                url: '/reporting/'+start_date+'/'+end_date,
                data:{
                    'start': start_date,
                    'end': end_date,
                    'status': status
                },
                success:function(data){
                    console.log(data);
                    $('.report-card').show();
                    grandTotal_fees = 0, grandTotal_tax = 0, grandTotal_tipping = 0, grandTotal = 0;
                    $.each(data, function (key, value) {
                        var tr = $("<tr class='text-center' />"),
                            total = parseFloat(value.billing.total_fees) + parseFloat(value.billing.tax) + parseFloat(value.billing.tipping);
                            grandTotal_fees += parseFloat(value.billing.total_fees),
                            grandTotal_tax += parseFloat(value.billing.tax),
                            grandTotal_tipping += parseFloat(value.billing.tipping),
                            grandTotal += total;
                        tr.append(
                            $("<td />", {
                                html: value.reff_no
                                })[0].outerHTML,
                            $("<td />", {
                                html: value.cust.name+' '+value.cust.last_name
                                })[0].outerHTML,
                            $("<td />", {
                                html: value.service.service_name
                                })[0].outerHTML,
                            $("<td />", {
                                html: '$'+value.billing.total_fees
                                })[0].outerHTML,
                            $("<td />", {
                                html: '$'+value.billing.tax
                                })[0].outerHTML,
                            $("<td />", {
                                html: '$'+value.billing.tipping
                                })[0].outerHTML,
                            $("<td />", {
                                html: '$'+total
                                })[0].outerHTML,
                        );
                        $("table tbody").append(tr)
                    });
                    var tr = $("<tr class='text-center' />")
                    tr.append(
                        $("<td colspan=3 />", {
                            html: "Grand Total"
                        })[0].outerHTML,
                        $("<td />", {
                            html: '$'+grandTotal_fees
                        })[0].outerHTML,
                        $("<td />", {
                            html: '$'+grandTotal_tax
                        })[0].outerHTML,
                        $("<td />", {
                            html: '$'+grandTotal_tipping
                        })[0].outerHTML,
                        $("<td />", {
                            html: '$'+grandTotal
                        })[0].outerHTML,
                    );
                    $("table tbody").append(tr);
                    $("#export").prop("href","/export/"+start_date+"/"+end_date);
                }
            })
        }

    });


    </script>
      
      
@endsection