@extends('layouts.template')

@section('css')
<x-head.tinymce-config/>
{{-- <style>
.body-card {
  overflow: hidden;
  text-overflow: ellipsis;
  word-wrap: break-word;
  display: block;
  line-height: 1em; /* a */
  max-height: 5em; /* a x number of line to show (ex : 2 line)  */
}
</style> --}}
@endsection
@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="nav-wrapper position-relative end-0">
                        <ul class="nav nav-pills nav-fill p-1" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-0 px-0 py-1 active " data-bs-toggle="tab" data-bs-target="#header" aria-controls="todays" aria-selected="true" href="javascript:;" role="tab">
                                    <i class="material-icons text-lg position-relative">home</i>
                                    <span class="ms-1">Page Header</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-0 px-0 py-1 " data-bs-toggle="tab" data-bs-target="#features" aria-controls="awaiting" href="javascript:;" role="tab" aria-selected="false">
                                    <i class="material-icons text-lg position-relative">email</i>
                                    <span class="ms-1">Features Section</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-0 px-0 py-1 " data-bs-toggle="tab" data-bs-target="#services" aria-controls="services" href="javascript:;" role="tab" aria-selected="false">
                                    <i class="material-icons text-lg position-relative">settings</i>
                                    <span class="ms-1">Services Section</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-0 px-0 py-1 " data-bs-toggle="tab" data-bs-target="#counter" aria-controls="scheduled" href="javascript:;" role="tab" aria-selected="false">
                                    <i class="material-icons text-lg position-relative">settings</i>
                                    <span class="ms-1">Counter Section</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-0 px-0 py-1 " data-bs-toggle="tab" data-bs-target="#footer" aria-controls="scheduled" href="javascript:;" role="tab" aria-selected="false">
                                    <i class="material-icons text-lg position-relative">settings</i>
                                    <span class="ms-1">Footer Section</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-0 px-0 py-1 " data-bs-toggle="tab" data-bs-target="#about" aria-controls="scheduled" href="javascript:;" role="tab" aria-selected="false">
                                    <i class="material-icons text-lg position-relative">settings</i>
                                    <span class="ms-1">About Section</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="tab-content">
                        {{-- Tab Pane Header --}}
                        <div class="tab-pane fade show active" id="header" role="tabpanel" aria-labelledby="header-tab" tabindex="0">
                            <form action="{{ route('admin.landing.update') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-6">
                                        @foreach ($tabpills['header'] as $item)
                                        <div class="input-group input-group-static mb-3">
                                            <label >{{ $item->setting_name }}</label>
                                            @if (strpos($item->setting_link, '_img') !== false)
                                                <img src="{{ asset('storage/'.$item->value) }}" class="form-control col-sm-5 {{ $item->setting_link }}">
                                                <input type="hidden" value="{{ $item->value }}" name="old_{{ $item->setting_link }}">
                                                <input type="file" class="form-control" id="{{ $item->setting_link }}" name="{{ $item->setting_link }}" onchange="previewImg(this.id,'{{ $item->setting_link }}')">
                                            @else
                                                <input type="text" class="form-control" name="{{ $item->setting_link }}" value="{{ $item->value }}">
                                            @endif
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="col-lg-6">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-lg bg-gradient-success w-100 mt-4 mb-1">save</button>
                            </form>
                        </div>
                        {{-- Tab Pane Features --}}
                        <div class="tab-pane fade" id="features" role="tabpanel" aria-labelledby="features-tab" tabindex="0">
                            <form action="{{ route('admin.landing.update') }}" method="post" enctype="multipart/form-data">
                                @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    @foreach ($tabpills['features'] as $item)
                                    <div class="input-group input-group-static mb-3">
                                        <label >{{ $item->setting_name }}</label>
                                        {{-- <input type="{{ strpos($item->setting_link, '_img') !== false ? 'file' : 'text' }}" class="form-control" name="{{ $item->setting_link }}" value="{{ $item->value }}"> --}}
                                        @if (strpos($item->setting_link, '_img') !== false)
                                            <img src="{{ asset('storage/'.$item->value) }}" class="form-control col-sm-5 {{ $item->setting_link }}">
                                            <input type="hidden" value="{{ $item->value }}" name="old_{{ $item->setting_link }}">
                                            <input type="file" class="form-control" id="{{ $item->setting_link }}" name="{{ $item->setting_link }}" onchange="previewImg(this.id,'{{ $item->setting_link }}')">
                                        @else
                                            <input type="text" class="form-control" name="{{ $item->setting_link }}" value="{{ $item->value }}">
                                        @endif
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <button type="submit" class="btn btn-lg bg-gradient-success w-100 mt-4 mb-1">save</button>
                            </form>
                        </div>
                        {{-- Tab Pane Counter --}}
                        <div class="tab-pane fade" id="counter" role="tabpanel" aria-labelledby="counter-tab" tabindex="0">
                            <form action="{{ route('admin.landing.update') }}" method="post">
                                @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    @foreach ($tabpills['counter'] as $item)
                                    <div class="input-group input-group-static mb-3">
                                        <label >{{ $item->setting_name }}</label>
                                        <input type="text" class="form-control" name="{{ $item->setting_link }}" value="{{ $item->value }}">
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <button type="submit" class="btn btn-lg bg-gradient-success w-100 mt-4 mb-1">save</button>
                            </form>
                        </div>
                        {{-- Tab Pane Footer --}}
                        <div class="tab-pane fade" id="footer" role="tabpanel" aria-labelledby="footer-tab" tabindex="0">
                            <form action="{{ route('admin.landing.update') }}" method="post" enctype="multipart/form-data">
                                @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    @foreach ($tabpills['footer'] as $item)
                                    <div class="input-group input-group-static mb-3">
                                        <label >{{ $item->setting_name }}</label>
                                        @if (strpos($item->setting_link, '_img') !== false)
                                            <img src="{{ asset('storage/'.$item->value) }}" class="form-control col-sm-5 {{ $item->setting_link }}">
                                            <input type="hidden" value="{{ $item->value }}" name="old_{{ $item->setting_link }}">
                                            <input type="file" class="form-control" id="{{ $item->setting_link }}" name="{{ $item->setting_link }}" onchange="previewImg(this.id,'{{ $item->setting_link }}')">
                                        @else
                                            <input type="text" class="form-control" name="{{ $item->setting_link }}" value="{{ $item->value }}">
                                        @endif
                                        {{-- <input type="{{ strpos($item->setting_link, '_img') !== false ? 'file' : 'text' }}" class="file form-control" name="{{ $item->setting_link }}" value="{{ $item->value }}"> --}}
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <button type="submit" class="btn btn-lg bg-gradient-success w-100 mt-4 mb-1">save</button>
                            </form>
                        </div>
                        {{-- Tab Pane Service --}}
                        <div class="tab-pane fade" id="services" role="tabpanel" aria-labelledby="services-tab" tabindex="0">
                            <div class="row p-3">
                                <div class="col-6"></div>
                                <div class="col-6 text-end">
                                    <button type="button" class="btn btn-link bg-gradient-primary" data-bs-toggle="modal" data-bs-target="#modalService">New Service</button>
                                </div>
                            </div>
                            <div class="row">
                                @foreach ($services as $service)
                                    <div class="col-lg-3">
                                        <div class="card card-body border card-plain border-radius-lg mb-2 ">
                                            <div class="row">
                                                <div class="input-group input-group-static mb-3">
                                                    {{-- <label>Uploade Imaged<br>&nbsp;</label> --}}
                                                    {{-- <input type="text" class="form-control" name="service_name" value="{{ $service->title }}" readonly> --}}
                                                    <img class="form-control" src="{{ asset('storage/'.$service->image) }}" alt="{{ $service->title }}" style="width: 75px; height: auto;">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-group input-group-static mb-3">
                                                    <label>Title<br>&nbsp;</label>
                                                    <input type="text" class="form-control" name="service_name" value="{{ $service->title }}" readonly>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="body-card input-group input-group-static mb-3">
                                                    {{-- <textarea name="body" class="form-control" id="body" cols="30" rows="3">{!! $service->body !!}</textarea> --}}
                                                    <p>{!! $service->body !!}</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-group input-group-static mb-4">
                                                    <label >Footer</label>
                                                    <input class="form-control" name="category_name" value="{{ $service->footer }}" readonly>
                                                </div>
                                            </div>
                                            <div class="row justify-content-between">
                                                <a type="button" id="editService" class="btn btn-info" href="{{ route('admin.service.edit', $service->id) }}">Edit Service</a>
                                                <form action="{{ route('admin.service.delete',$service->id) }}" method="post">
                                                    @method('delete')
                                                    @csrf
                                                    <button type="submit" id="editService" class="btn btn-outline-secondary w-100" onclick="return confirm('Are you sure DELETE {{ $service->title }} Service?')">Delete Service</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        {{-- Tab Pane Footer --}}
                        <div class="tab-pane fade" id="about" role="tabpanel" aria-labelledby="footer-tab" tabindex="0">
                            <form action="{{ route('admin.landing.update') }}" method="post" enctype="multipart/form-data">
                                @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    @foreach ($tabpills['about'] as $item)
                                    <div class="input-group input-group-static mb-3">
                                        <label >{{ $item->setting_name }}</label>
                                        @if (strpos($item->setting_link, '_img') !== false)
                                            <img src="{{ asset('storage/'.$item->value) }}" class="form-control col-sm-5 {{ $item->setting_link }}">
                                            <input type="hidden" value="{{ $item->value }}" name="old_{{ $item->setting_link }}">
                                            <input type="file" class="form-control" id="{{ $item->setting_link }}" name="{{ $item->setting_link }}" onchange="previewImg(this.id,'{{ $item->setting_link }}')">
                                        @elseif (strpos($item->setting_link, '_paragraph') !== false)
                                            {{-- <input type="text" class="form-control" name="{{ $item->setting_link }}" value="{{ $item->value }}"> --}}
                                            <textarea class="form-control" name="{{ $item->setting_link }}" cols="50" rows="10">{{ $item->value }}</textarea>
                                        @else
                                            <input type="text" class="form-control" name="{{ $item->setting_link }}" value="{{ $item->value }}">
                                        @endif
                                        {{-- <input type="{{ strpos($item->setting_link, '_img') !== false ? 'file' : 'text' }}" class="file form-control" name="{{ $item->setting_link }}" value="{{ $item->value }}"> --}}
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <button type="submit" class="btn btn-lg bg-gradient-success w-100 mt-4 mb-1">save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- Modal New Service in Landing Page -->
<div class="modal modal-lg fade" id="modalService" tabindex="-1" role="dialog" aria-labelledby="modalNewService" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title font-weight-normal" id="modalNewService">New Service</h5>
          <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('admin.service.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <div class="input-group input-group-static mb-4">
                        <label >Title</label>
                        <input type="text" class="form-control" name="title" id="serviceName">
                    </div>
                    <div class="input-group input-group-static mb-4">
                        <label >Body</label><br>
                    </div>
                    <div class="input-group input-group-static mb-4">
                        <textarea id="richtextbox" name="body"></textarea>
                    </div>
                    <div class="input-group input-group-static mb-4">
                        <label >Footer</label>
                        <input type="text" class="form-control" name="footer" id="serviceName">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group input-group-static mb-4">
                        <label >Image</label>
                        <input type="file" class="form-control" name="image" id="serviceImage" onchange="previewImg(this.id,'preview-img')">
                    </div>
                </div>
                <div class="col-lg-6">
                    <img class="preview-img img-fluid col-sm-5">
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="reset" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Cancel</button>
          <button type="submit" class="btn bg-gradient-success">Save</button>
        </div>
            </form>
      </div>
    </div>
</div>
@endsection

@section('javascript')
    <script src="{{ asset('js/plugins/formPreviewImage.js') }}"></script>
@endsection


