@extends('layouts.template')

@section('content')
    <div class="container-fluid py-4">
        <div class="row justify-content-center">
          <div class="col-lg-8">
            <div class="card my-4">
              <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-success shadow-success border-radius-lg pt-4 pb-3">
                  <h6 class="text-white text-capitalize ps-3">Employees</h6>
                </div>
              </div>
              <div class="card-body px-0 pb-2">
                <div class="row p-3">
                  <div class="col-6"></div>
                  <div class="col-6 text-end">
                    <button type="button" class="btn bg-gradient-primary" id="newEmployee" data-header="New Employee">New Employee</button>
                  </div>
                </div>
                @if ($employees->isNotEmpty())
                  <div class="table-responsive p-0">
                    <table class="table align-items-center mb-0">
                      <thead>
                        <tr>
                          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-center">#</th>
                          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Name</th>
                          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Phone Number</th>
                          <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Role</th>
                          <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">is Admin</th>
                          <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2"></th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach ($employees as $employee)
                              <tr>
                                  <td class="align-middle text-center">
                                      {{ $loop->iteration }}
                                  </td>
                                  <td>
                                      <div class="d-flex px-2 py-1">
                                        <div class="d-flex flex-column justify-content-center">
                                            <h6 class="mb-0 text-sm">{{ $employee->name }}&nbsp;{{ $employee->last_name }}</h6>
                                            <p class="text-xs text-secondary mb-0">{{ $employee->email }}</p>
                                        </div>
                                      </div>
                                  </td>
                                  <td>
                                      <div class="d-flex px-2 py-1">
                                        <div class="d-flex flex-column justify-content-center">
                                            <p class="text-xs font-weight-bold mb-0">{{ $employee->phone_number }}</p>
                                        </div>
                                      </div>
                                  </td>
                                  <td class="align-middle text-center">
                                    @foreach ($employee->getRoleNames() as $role)
                                      <p class="text-xs font-weight-bold mb-0">{{ $role }},</p>
                                    @endforeach
                                  </td>
                                  <td class="align-middle text-center">
                                      <div class="form-check form-switch">
                                        <input class="toggle-class form-check-input ms-auto" type="checkbox" id="flexSwitchCheckDefault" {{ $employee->hasRole('admin') ? 'checked' : '' }} data-id="{{ $employee->id }}">
                                      </div>
                                  </td>
                                  <td class="text-center">
                                    <button type="button" class="btn btn-link pe-3 ps-0 mb-0 ms-auto w-25 w-md-auto" id="editEmployee" data-id="{{ $employee->id }}" data-name="{{ $employee->name }}" data-lastname="{{ $employee->last_name }}" data-email="{{ $employee->email }}" data-phone="{{ $employee->phone_number }}" data-header="Edit Employee">
                                    Edit
                                    </button>
                                  </td>
                              </tr>
                          @endforeach
                      </tbody>
                    </table>
                  </div>
                @else
                  <h3 class="text-center">No employees registered</h3>
                @endif
              </div>
            </div>
          </div>
        </div>
    </div>

<!-- Modal New Category -->
<div class="modal modal-sm fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalNewCategory" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-normal" id="modalHeaderCategory"></h5>
        <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form role="form text-left" method="POST" id="formEmployee">
          @csrf
          <input type="hidden" name="_method" id="method">
            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group input-group-static mb-3">
                        <label for="first_name">First Name</label>
                        <input type="text" class="form-control" id="first_name" name="name" value="{{ old('name') }}" required>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="input-group input-group-static mb-3">
                        <label for="last_name">Last Name</label>
                        <input type="text" class="form-control" id="last_name" name="last_name" value="{{ old('last_name') }}" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="input-group input-group-static mb-3">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}" required>
                    </div>
                    <div class="input-group input-group-static mb-3">
                        <label for="phone">Phone Number</label>
                        <input type="text" class="form-control" id="phone" name="phone_number" value="{{ old('phone_number') }}" required>
                    </div>
                </div>
            </div>
            <div class="text-center">
              <button type="submit" class="btn btn-round bg-gradient-success w-100 mt-4 mb-0">Save</button>
              <button type="button" class="btn btn-round btn-outline-secondary w-100 mt-2 mb-0" id="cancels">Clear Content</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('javascript')
    <script>
        $('.toggle-class').on('change', function(){
            var idx = $(this).data('id');
            var status = $(this).prop('checked') == true ? 1 : 0;
            
            $.ajax({
                type: 'GET',
                dataType: 'JSON',
                url: '{{ route('employees.status','idx') }}',
                data: {
                    'status': status,
                    'user' : idx,
                },
                success: function() {
                  location.reload();
                }
            })
        });

        $('div').on('click', '#editEmployee', function (e) {
          $('#formEmployee')[0].action = '{{ route('employees.'.'update','__id') }}'.replace('__id',$(this).data('id'));
          document.getElementById('modalHeaderCategory').innerHTML = $(this).data('header');
          $('#method').val('PUT');
          $('#first_name').val($(this).data('name'));
          $('#last_name').val($(this).data('lastname'));
          $('#email').val($(this).data('email'));
          $('#phone').val($(this).data('phone'));
          $('#modal').modal('show');
        });

        $('div').on('click', '#newEmployee', function (e) {
          $('#formEmployee')[0].action = '{{ route('employees.'.'store') }}';
          document.getElementById('modalHeaderCategory').innerHTML = $(this).data('header');
          // $(this).closest('formEmployee').find("input[type=text]").old("");
          $('#modal').modal('show');
        });

        $('.modal').on('hidden.bs.modal', function (e) {
            $(this).find("input,textarea,select").val('').end();
        });
    </script>
@endsection