<!--
=========================================================
* Material Kit 2 - v3.0.2
=========================================================

* Product Page:  https://www.creative-tim.com/product/material-kit 
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<!DOCTYPE html>
<html lang="en" itemscope itemtype="http://schema.org/WebPage">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/logo-ct.png') }}">
  <link rel="icon" type="image/png" href="{{ asset('img/logo-ct.png') }}">
  <title>
    {{ config('app.name') }}
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+sans-serif:400,700" />
  <!-- Nucleo Icons -->
  <link href="{{ asset('css/nucleo-icons.css') }}"  rel="stylesheet"/>
  <link href="{{ asset('css/nucleo-svg.css') }}"  rel="stylesheet"/>
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="{{ asset('css/material-dashboard.css?v=3.0.4') }}" rel="stylesheet" />
  
  <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('css/owl.theme.green.min.css') }}" rel="stylesheet" />
  <style>
    html {
      scroll-behavior: smooth;
    }
    .owl-prev span, .owl-next span{
      display: none;
    }
    
  </style>
  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-GSLBZCVHHR"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-GSLBZCVHHR');
  </script>
</head>

<body class="about-us bg-gray-200">
  <!-- Navbar Dark -->
  @include('partials.navbarLand')
  <!-- End Navbar -->
  <!-- -------- START HEADER 7 w/ text and video ------- -->
  <header class="bg-gradient-dark">
    <div class="page-header min-vh-75" style="background-image: url('{{ $show['site_img_header']->value == null ? asset('img/bg3.jpg') : asset('storage/'.$show['site_img_header']->value) }}');">
      <span class="mask bg-gradient-dark opacity-6"></span>
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-8 text-center mx-auto my-auto">
            <h1 class="text-white">{{ $show['site_hero_title']->value }}</h1>
            {{-- <p class="lead mb-4 text-white opacity-8">Getting work done in Cumberland County</p> --}}
            <p class="lead mb-4 text-white opacity-8">{{ $show['site_hero_desc']->value }}</p>
            <a href="#register">
              <button type="submit" class="btn btn-lg bg-gradient-success text-light font-weight-bold" id="shrinkBtn">Got Work?</button>
            </a>
            <h6 class="text-white mb-2 mt-5">Find us on</h6>
            <div class="d-flex justify-content-center">
              <a href="https://www.facebook.com/welikework"><i class="fab fa-facebook text-lg text-white me-4"></i></a>
              <a href="https://www.instagram.com/welikework.cumberland"><i class="fab fa-instagram text-lg text-white me-4"></i></a>
              <a href="https://www.linkedin.com/company/80057086/admin/"><i class="fab fa-linkedin text-lg text-white me-4"></i></a>
              <a href="https://twitter.com/welikework"><i class="fab fa-twitter text-lg text-white me-4"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- -------- END HEADER 7 w/ text and video ------- -->
  <div class="card card-body shadow-xl mx-3 mx-md-4 mt-n6">
    <!-- Section with four info areas left & one card right with image and waves -->
    <section class="py-7">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6">
            <div class="row justify-content-start">
              <div class="col-md-6">
                <div class="info">
                  <i class="material-icons text-3xl text-gradient text-success mb-3">public</i>
                  <h5>{{ $show['site_1stfeatures_title']->value }}</h5>
                  <p>{{ $show['site_1stfeatures_desc']->value }}</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="info">
                  <i class="material-icons text-3xl text-gradient text-success mb-3">3p</i>
                  <h5>{{ $show['site_2ndfeatures_title']->value }}</h5>
                  <p>{{ $show['site_2ndfeatures_desc']->value }}</p>
                </div>
              </div>
            </div>
            <div class="row justify-content-start mt-4">
              <div class="col-md-6">
                <div class="info">
                  <i class="material-icons text-3xl text-gradient text-success mb-3">payment</i>
                  <h5>{{ $show['site_3rdfeatures_title']->value }}</h5>
                  <p>{{ $show['site_3rdfeatures_desc']->value }}</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="info">
                  <i class="material-icons text-3xl text-gradient text-success mb-3">apps</i>
                  <h5>{{ $show['site_4thfeatures_title']->value }}</h5>
                  <p>{{ $show['site_4thfeatures_desc']->value }}</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 ms-auto mt-lg-0 mt-4">
            <div class="card">
              <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <a class="d-block blur-shadow-image">
                  <img src="{{ $show['site_card_img']->value == null ? 'https://images.unsplash.com/photo-1544717302-de2939b7ef71?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80' : asset('storage/'.$show['site_card_img']->value) }}" alt="img-colored-shadow" class="img-fluid border-radius-lg">
                </a>
              </div>
              <div class="card-body text-center">
                <h5 class="font-weight-normal">
                  <a href="javascript:;">{{ $show['site_card_title']->value }}</a>
                </h5>
                <p class="mb-0">
                  {{ $show['site_card_desc']->value }}
                </p>
                <a href="#register">
                  <button type="button" class="btn bg-gradient-success btn-sm mb-0 mt-3">Request Work</button>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END Section with four info areas left & one card right with image and waves -->
    <!-- -------- START Features w/ pattern background & stats & rocket -------- -->
    <section class="pb-5 position-relative bg-gradient-dark mx-n3">
      <div class="container">
        <div class="row">
          <div class="col-md-8 text-start mb-5 mt-5">
            <h3 class="text-white z-index-1 position-relative">What do we do & How Much?</h3>
            <p class="text-white opacity-8 mb-0">We have a talent pool that consists of landscapers, handymen, carpenters and are constantly adding more skilled talent to our team. Below are the general services we provide and typical starting rates.</p>
          </div>
        </div>
        <div class="row">
          <div class="container col-lg-11">
            <div class="owl-carousel owl-theme">
              @foreach ($pricelists as $item)
                <div class="card card-carousel mb-5" data-animation="true">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                      <a class="d-block blur-shadow-image">
                        <img src="{{ $item->image == null ? asset('img/products/product-1-min.jpg') : asset('storage/'.$item->image)  }}" alt="img-blur-shadow" class="img-fluid shadow border-radius-lg">
                      </a>
                    </div>
                    <div class="card-body">
                      <h5 class="font-weight-normal mt-3 text-center">
                        <a href="javascript:;">{{ $item->title }}</a>
                      </h5>
                      <p class="mb-0">
                        {!! $item->body !!}
                      </p>
                    </div>
                    <hr class="dark horizontal my-0">
                    <div class="card-footer text-center">
                      <p class="font-weight-normal my-auto">{{ $item->footer }}</p>
                    </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- -------- END Features w/ pattern background & stats & rocket -------- -->
    <section class="mt-8 pt-4 pb-6" id="count-stats">
      <div class="container">
        <div class="row justify-content-center text-center">
          <div class="col-md-3">
            <h1 class="text-gradient text-info" id="state1" countTo="{{ $show['site_1st_counter_val']->value }}">0</h1>
            <h5>{{ $show['site_1st_counter_title']->value }}</h5>
            <p>{{ $show['site_1st_counter_desc']->value }}</p>
          </div>
          <div class="col-md-3">
            <h1 class="text-gradient text-info"><span id="state2" countTo="{{ $show['site_2nd_counter_val']->value }}">0</span>+</h1>
            <h5>{{ $show['site_2nd_counter_title']->value }}</h5>
            <p>{{ $show['site_2nd_counter_desc']->value }}</p>
          </div>
          <div class="col-md-3">
            <h1 class="text-gradient text-info"><span id="state3" countTo="{{ $show['site_3rd_counter_val']->value }}">0</span>+</h1>
            <h5>{{ $show['site_3rd_counter_title']->value }}</h5>
            <p>{{ $show['site_3rd_counter_desc']->value }}</p>
          </div>
        </div>
      </div>
    </section>
    <!-- -------- START PRE-FOOTER 1 w/ SUBSCRIBE BUTTON AND IMAGE ------- -->
    <section class="my-5 pt-5" id="register">
      <div></div>
      <div class="container">
        <div class="row">
          <div class="col-md-6 m-auto">
            <div class="card card-plain">
              <div class="card-header">
                <h4 class="font-weight-bolder">Got Work? Sign up today to get on the schedule!</h4>
                <p class="mb-0">Enter the following information to create your We Like Work Dashboard to monitor & request work</p>
              </div>
              <div class="card-body">
                <form role="form" method="POST" action="{{ route('register') }}">
                  @csrf
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="input-group input-group-static mb-3">
                        <label for="phone_number">Phone Number</label>
                        {{-- <input type="text" class="form-control"> --}}
                        <input id="phone_number" type="text" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number">

                                @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="input-group input-group-static mb-3">
                        <label for="name">First Name</label>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" >
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="input-group input-group-static mb-3">
                        <label for="last_name">Last Name</label>
                        <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name" >
                        @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="input-group input-group-static mb-3">
                        <label for="email">Email</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" >
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="input-group input-group-static mb-3">
                        <label for="password">Password</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="input-group input-group-static mb-3">
                        <label for="password-confirm">Password Confirmation</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                      </div>
                    </div>
                  </div>
                  <div class="form-check form-check-info text-start ps-0">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked>
                    <label class="form-check-label" for="flexCheckDefault">
                      I agree the <a href="javascript:;" class="text-dark font-weight-bolder">Terms and Conditions</a>
                    </label>
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn btn-lg bg-gradient-success btn-lg w-100 mt-4 mb-0">Sign Up</button>
                  </div>
                </form>
              </div>
              <div class="card-footer text-center pt-0 px-lg-2 px-1">
                <p class="mb-2 text-sm mx-auto">
                  Already have an account?
                  <a href="/login" class="text-success text-gradient font-weight-bold">Sign in</a>
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-5 ms-auto">
            <div class="position-relative">
              <img class="max-width-50 w-100 position-relative z-index-2" src="{{ $show['site_footer_img']->value == null ? asset ('img/illustrations/illustration-lock.jpg') : asset('storage/'.$show['site_footer_img']->value) }}" alt="image">
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- -------- END PRE-FOOTER 1 w/ SUBSCRIBE BUTTON AND IMAGE ------- -->
  </div>
  <footer class="footer pt-5 mt-5">
    <div class="container">
    </div>
  </footer>
  <!--   Core JS Files   -->
  <script type="text/javascript" src="{{ asset('js/core/js.min.js')}}"></script>
  {{-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>  --}}
  <!--   Core JS Files   -->
  <script src="{{ asset('js/core/popper.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset('js/core/bootstrap.min.js') }}" type="text/javascript"></script>
  <!--  Plugin for TypedJS, full documentation here: https://github.com/inorganik/CountUp.js -->
  <script src="{{ asset('js/plugins/countup.min.js') }}"></script>
  {{-- <script src="{{ asset('js/plugins/perfect-scrollbar.min.js') }}"></script> --}}
  <script src="{{ asset('js/plugins/smooth-scrollbar.min.js') }}"></script>
  <!-- Control Center for Material UI Kit: parallax effects, scripts for the example pages etc -->
  <script type="text/javascript" src="{{ asset('js/material-dashboard.min.js?v=3.0.4') }}"></script>
  
  <script type="text/javascript" src="{{ asset('js/plugins/owl.carousel.min.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.owl-carousel').owlCarousel({
        autoplay: true,
        autoplayhoverpause: true,
        loop: true,
        margin: 25,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:2,
                nav:false
            },
            1000:{
                items:4,
                nav:true,
                loop:false
            }
        }
      });
    });
  </script>

  <script>
    $(window).on('hashchange', function () 
    {
        var top = $(window.location.hash).offset().top;
        $(window).scrollTop(top);
    });

    // get the element to animate
    var element = document.getElementById('count-stats');
    var elementHeight = element.clientHeight;

    // listen for scroll event and call animate function

    document.addEventListener('scroll', animate);

    // check if element is in view
    function inView() {
      // get window height
      var windowHeight = window.innerHeight;
      // get number of pixels that the document is scrolled
      var scrollY = window.scrollY || window.pageYOffset;
      // get current scroll position (distance from the top of the page to the bottom of the current viewport)
      var scrollPosition = scrollY + windowHeight;
      // get element position (distance from the top of the page to the bottom of the element)
      var elementPosition = element.getBoundingClientRect().top + scrollY + elementHeight;

      // is scroll position greater than element position? (is element in view?)
      if (scrollPosition > elementPosition) {
        return true;
      }

      return false;
    }

    var animateComplete = true;
    // animate element when it is in view
    function animate() {

      // is element in view?
      if (inView()) {
        if (animateComplete) {
          if (document.getElementById('state1')) {
            const countUp = new CountUp('state1', document.getElementById("state1").getAttribute("countTo"));
            if (!countUp.error) {
              countUp.start();
            } else {
              console.error(countUp.error);
            }
          }
          if (document.getElementById('state2')) {
            const countUp1 = new CountUp('state2', document.getElementById("state2").getAttribute("countTo"));
            if (!countUp1.error) {
              countUp1.start();
            } else {
              console.error(countUp1.error);
            }
          }
          if (document.getElementById('state3')) {
            const countUp2 = new CountUp('state3', document.getElementById("state3").getAttribute("countTo"));
            if (!countUp2.error) {
              countUp2.start();
            } else {
              console.error(countUp2.error);
            };
          }
          animateComplete = false;
        }
      }
    }

    if (document.getElementById('typed')) {
      var typed = new Typed("#typed", {
        stringsElement: '#typed-strings',
        typeSpeed: 90,
        backSpeed: 90,
        backDelay: 200,
        startDelay: 500,
        loop: true
      });
    }
  </script>
  <script>
    if (document.getElementsByClassName('page-header')) {
      window.onscroll = debounce(function() {
        var scrollPosition = window.pageYOffset;
        var bgParallax = document.querySelector('.page-header');
        var oVal = (window.scrollY / 3);
        bgParallax.style.transform = 'translate3d(0,' + oVal + 'px,0)';
      }, 6);
    }
  </script>

</body>

</html>