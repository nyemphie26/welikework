<!--
=========================================================
* Material Kit 2 - v3.0.2
=========================================================

* Product Page:  https://www.creative-tim.com/product/material-kit 
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<!DOCTYPE html>
<html lang="en" itemscope itemtype="http://schema.org/WebPage">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/logo-ct.png') }}">
  <link rel="icon" type="image/png" href="{{ asset('img/logo-ct.png') }}">
  <title>
    {{ config('app.name') }}
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+sans-serif:400,700" />
  <!-- Nucleo Icons -->
  <link href="{{ asset('css/nucleo-icons.css') }}"  rel="stylesheet"/>
  <link href="{{ asset('css/nucleo-svg.css') }}"  rel="stylesheet"/>
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="{{ asset('css/material-kit.css?v=3.0.2') }}" rel="stylesheet" />
  {{-- <link id="pagestyle" href="{{ asset('css/material-dashboard.css?v=3.0.4') }}" rel="stylesheet" /> --}}
</head>

<body class="blog-author bg-gray-200">
  <!-- Navbar Transparent -->
  @include('partials.navbarLand')
  <!-- End Navbar -->
  
  <section class="py-lg-5 mt-5 mt-lg-0">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="card box-shadow-xl overflow-hidden mb-5">
            <div class="row">
              <div class="col-lg-5 position-relative bg-cover px-0" style="background-image: url('{{ $show['contact_img']->value == null ? asset('img/illustrations/illustration-reset.jpg') : asset('storage/'.$show['contact_img']->value) }}')" loading="lazy">
              {{-- <div class="col-lg-5 position-relative bg-cover px-0" style="background-image: url('{{ asset('img/illustrations/illustration-reset.jpg') }}')" loading="lazy"> --}}
                <div class="z-index-2 text-center d-flex h-100 w-100 d-flex m-auto justify-content-center">
                  <div class="mask bg-gradient-dark opacity-6"></div>
                  <div class="p-5 ps-sm-8 position-relative text-start my-auto z-index-2">
                    <h3 class="text-white">{{ $show['contact_title']->value }}</h3>
                    <p class="text-white opacity-8 mb-4">{{ $show['contact_desc']->value }}</p>
                    <div class="d-flex p-2 text-white">
                      <div>
                        <i class="fas fa-phone text-sm"></i>
                      </div>
                      <div class="ps-3">
                        <span class="text-sm opacity-8">{{ $show['contact_phone']->value }}</span>
                      </div>
                    </div>
                    <div class="d-flex p-2 text-white">
                      <div>
                        <i class="fas fa-envelope text-sm"></i>
                      </div>
                      <div class="ps-3">
                        <span class="text-sm opacity-8">{{ $show['contact_mail']->value }}</span>
                      </div>
                    </div>
                    <div class="d-flex p-2 text-white">
                      <div>
                        <i class="fas fa-map-marker-alt text-sm"></i>
                      </div>
                      <div class="ps-3">
                        <span class="text-sm opacity-8">{{ $show['contact_loc']->value }}</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-7">
                <div class="card-header px-4 py-sm-5 py-3">
                  <h2>{{ $show['about_title']->value }}</h2>
                  <p class="lead">{{ $show['about_subtitle']->value }}</p>
                </div>
                <div class="card-body pt-1">
                  <div class="row">
                    <div class="col-md-12 pe-5 mb-3">
                      <p class="text-sm mb-0">{{ $show['about_paragraph']->value }}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- -------- START FOOTER 5 w/ DARK BACKGROUND ------- -->
  
  <!-- -------- END FOOTER 5 w/ DARK BACKGROUND ------- -->
  <!--   Core JS Files   -->
  <script type="text/javascript" src="{{ asset('js/core/js.min.js')}}"></script>
  <!--   Core JS Files   -->
  <script src="{{ asset('js/core/popper.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset('js/core/bootstrap.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/plugins/perfect-scrollbar.min.js') }}"></script>
  <!--  Plugin for Parallax, full documentation here: https://github.com/wagerfield/parallax  -->
  <script src="{{ asset('js/plugins/parallax.min.js') }}"></script>
  <!-- Control Center for Material UI Kit: parallax effects, scripts for the example pages etc -->
  <!--  Google Maps Plugin    -->
  {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTTfWur0PDbZWPr7Pmq8K3jiDp0_xUziI"></script> --}}
  <script type="text/javascript" src="{{ asset('js/material-kit.min.js?v=3.0.2') }}"></script>
  {{-- <script type="text/javascript" src="{{ asset('js/material-dashboard.min.js?v=3.0.4') }}"></script> --}}
</body>

</html>