<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/logo-ct.png') }}">
  <link rel="icon" type="image/png" href="{{ asset('img/logo-ct.png') }}">
  <title>
    We Like Work
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto:400,700" />
  <!-- Nucleo Icons -->
  <link href="{{ asset('css/nucleo-icons.css') }}" rel="stylesheet" />
  <link href="{{ asset('css/nucleo-svg.css') }}" rel="stylesheet" />
  {{-- Datatables --}}
  <link rel="stylesheet" type="text/css" href="{{ asset('css/datatables.min.css') }}"/>
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="{{ asset('css/material-dashboard.css?v=3.0.4') }}" rel="stylesheet" />
  @yield('css')
  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-GSLBZCVHHR"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-GSLBZCVHHR');
  </script>
</head>
  <body class="g-sidenav-show  bg-gray-200">
    @canany(['access admin page'])
      @include('partials.sidebar')
    @endcan

    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
      @include('partials.navbar')
      @yield('content')
      @include('partials.alert')
    </main>

  <!--   Core JS Files   -->
  <script src="{{ asset('js/core/js.min.js') }}"></script> 
  <!--   Core JS Files   -->
  <script src="{{ asset('js/core/popper.min.js') }}"></script>
  <script src="{{ asset('js/core/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/plugins/perfect-scrollbar.min.js') }}"></script>
  <script src="{{ asset('js/plugins/smooth-scrollbar.min.js') }}"></script>
  <script src="{{ asset('js/plugins/chartjs.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/plugins/datatables.min.js') }}"></script>
  
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('js/material-dashboard.min.js?v=3.0.4') }}"></script>
  @yield('javascript')
  </body>
</html>