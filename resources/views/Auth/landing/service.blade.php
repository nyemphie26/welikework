@extends('layouts.template')

@section('css')
<x-head.tinymce-config/>
@endsection
@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                  <div class="bg-gradient-success shadow-success border-radius-lg pt-4 pb-3">
                    <h6 class="text-white text-capitalize ps-3">Edit Service</h6>
                  </div>
                </div>
                <div class="card-body px-2 pb-2">
                    <form action="{{ route('admin.service.update',$service->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="input-group input-group-static mb-4">
                                <label >Title</label>
                                <input type="text" class="form-control" name="title" id="serviceName" value="{{ $service->title }}">
                            </div>
                            <div class="input-group input-group-static mb-4">
                                <label >Footer</label>
                                <input type="text" class="form-control" name="footer" id="serviceName" value="{{ $service->footer }}">
                            </div>
                            <div class="input-group input-group-static mb-4">
                                <label >Image</label>
                                <input type="file" class="form-control" name="image" id="serviceImage" onchange="previewImg(this.id,'preview-img')">
                                <input type="hidden" value="{{ $service->image }}" name="oldImage">
                            </div>
                            @if ($service->image)
                                <img src="{{ asset('storage/'.$service->image) }}" class="preview-img img-fluid col-sm-12"/>
                            @else
                                <img class="preview-img img-fluid col-sm-5">
                            @endif
                        </div>
                        <div class="col-lg-8">
                            <div class="input-group input-group-static mb-4">
                                <label >Body</label><br>
                            </div>
                            <div class="input-group input-group-static mb-4">
                                <textarea id="richtextbox" name="body">{{ $service->body }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <a href="{{ route('admin.landing') }}" class="btn bg-gradient-secondary">Cancel</a>
                  <button type="submit" class="btn bg-gradient-success">Save</button>
                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')
<script src="{{ asset('js/plugins/formPreviewImage.js') }}"></script>
@endsection

