@extends('layouts.template')

@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="card my-4">
              <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-success shadow-success border-radius-lg pt-4 pb-3">
                  <h6 class="text-white text-capitalize ps-3">Customers</h6>
                </div>
              </div>
              <div class="card-body px-0 pb-2">
                <div class="row p-3">
                  <div class="col-6"></div>
                  <div class="col-6 text-end">
                    <button type="button" class="btn bg-gradient-primary" id="addModal" data-header="New Customer">New Customer</button>
                  </div>
                </div>
                <div class="table-responsive p-0">
                  <table class="table align-items-center mb-0">
                    <thead>
                      <tr>
                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Registration Date</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Customer Name</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Phone Number</th>
                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Requested Works</th>
                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Accepted Works</th>
                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Rejected Works</th>
                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"></th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($customers as $cust)
                      <tr>
                        <td class="align-middle text-center">
                          <span class="text-secondary text-xs font-weight-bold">{{ date('M, d Y',strtotime($cust->created_at)) }}</span>
                        </td>
                        <td>
                          <div class="d-flex px-2 py-1">
                            <div class="d-flex flex-column justify-content-center">
                              <h6 class="mb-0 text-sm">{{ $cust->name }}&nbsp;{{ $cust->last_name }}</h6>
                              <p class="text-xs text-secondary mb-0">{{ $cust->email }}</p>
                            </div>
                          </div>
                        </td>
                        <td>
                          <p class="text-xs font-weight-bold mb-0">{{ $cust->phone_number }}</p>
                        </td>
                        <td class="align-middle text-center">
                          <span class="text-secondary text-xs font-weight-bold">{{ $cust->All }} Work</span>
                        </td>
                        <td class="align-middle text-center text-sm">
                          {{-- <span class="badge badge-sm bg-gradient-success">Online</span> --}}
                          <p class="text-xs font-weight-bold mb-0">{{ $cust->Approved }} Works</p>
                        </td>
                        <td class="align-middle text-center text-sm">
                          <p class="text-xs font-weight-bold mb-0">{{ $cust->Rejected }} Works</p>
                        </td>
                        <td class="align-middle">
                          <button type="button" class="btn btn-link pe-3 ps-0 mb-0 ms-auto w-25 w-md-auto" id="editModal" data-header="Edit Customer" data-id="{{ $cust->id }}" data-name="{{ $cust->name }}" data-lastname="{{ $cust->last_name }}" data-email="{{ $cust->email }}" data-phone="{{ $cust->phone_number }}">
                            Edit
                          </button>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
    </div>

    
    <!-- Modal -->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-body p-0">
                <div class="card card-plain">
                  <div class="card-header pb-0 text-left">
                    <h3 class="font-weight-bolder text-success text-gradient" id="modalHeader"></h3>
                  </div>
                  <div class="card-body">
                    <form role="form text-left" id="formCustomer" method="POST">
                      @csrf
                      <input type="hidden" name="_method" id="method">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="input-group input-group-static mb-3">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" id="first_name" name="name" value="{{ old('name') }}" required autofocus>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="input-group input-group-static mb-3">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" value="{{ old('last_name') }}" required autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="input-group input-group-static mb-3">
                                    <label>Email</label>
                                    <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required autofocus>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="input-group input-group-static mb-3">
                                    <label>Phone Number</label>
                                    <input type="text" class="form-control" id="phone" name="phone_number" value="{{ old('phone_number') }}" required autofocus>
                                </div>
                            </div>
                        </div>
                      <div class="text-center justify-content-between">
                        <button type="button" class="btn btn-round bg-gradient-secondary mt-4 mb-0" id="cancels">Clear Content</button>
                        <button type="submit" class="btn btn-round bg-gradient-success mt-4 mb-0">Save</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $('.table').dataTable({
          "info":false,
          "lengthChange": false,
          "pageLength": 20,
          language: {
            paginate: {
              next: '<i class="fa fa-angle-right"></i>', // or '→'
              previous: '<i class="fa fa-angle-left"></i>' // or '←' 
            }
          },
          "drawCallback": function () {
            $('.dataTables_paginate ul.pagination').addClass('pagination-success');
          }
        });

        $('div').on('click', '#editModal', function (e) {
          $('#formCustomer')[0].action = '{{ route('customers.'.'update','__id') }}'.replace('__id',$(this).data('id'));
          document.getElementById('modalHeader').innerHTML = $(this).data('header');
          $('#method').val('PUT');
          $('#first_name').val($(this).data('name'));
          $('#last_name').val($(this).data('lastname'));
          $('#email').val($(this).data('email'));
          $('#phone').val($(this).data('phone'));
          $('#modal-form').modal('show');
        });

        $('div').on('click', '#addModal', function (e) {
          $('#formCustomer')[0].action = '{{ route('customers.'.'store') }}';
          document.getElementById('modalHeader').innerHTML = $(this).data('header');
          $('#modal-form').modal('show');
        });

        $('div').on('click', '#cancels', function (e) {
        $(this).closest('form').find("input[type=text],input[type=email]").val("");
          // $('#modal-form').modal('hide');
        });
    </script>
@endsection