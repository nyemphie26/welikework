@extends('layouts.template')

@section('content')
    
<div class="container-fluid px-2 px-md-4">
    <div class="page-header min-height-300 border-radius-xl mt-4" style="background-image: url('https://i.ibb.co/D9DF9fr/285103713-131268889528987-8857143269623988680-n.jpg');">
      {{-- <span class="mask  bg-gradient-success  opacity-6"></span> --}}
    </div>
    <div class="card card-body mx-3 mx-md-4 mt-n6">
      <div class="row gx-4 mb-2">
        <div class="col-auto">
            <div class="row gx-4 mb-4">
                <div class="col-auto">
                  <div class="avatar avatar-xxl position-relative">
                    <img src="{{ asset('img/bruce-mars.jpg') }}" alt="profile_image" class="w-100 border-radius-lg shadow-sm">
                  </div>
                </div>
                <div class="col-auto">
                    <div class="row mb-4">
                        <div class="h-100">
                          <h5 class="mb-1">{{ Auth::user()->name }}&nbsp;<span class="mb-0 font-weight-normal text-sm">{{ Auth::user()->last_name }}</span>
                          </h5>
                          <p class="mb-0 font-weight-normal text-sm">
                            {{ Auth::user()->email }}
                          </p>
                        </div>
                    </div>
                    <div class="row">
                        <a href="{{ route('dashboard') }}">
                            <div class="h-100">
                              <h5 class="mb-1"><i class="material-icons text-lg position-relative">undo</i></h5>
                              <p class="mb-0 font-weight-normal text-sm">
                                back to Dashboard
                              </p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-lg-4 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="nav-wrapper position-relative end-0">
                        <ul class="nav nav-pills nav-fill p-1" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-0 px-0 py-1 active " data-bs-toggle="tab" data-bs-target="#todays" aria-controls="todays" aria-selected="true" href="javascript:;" role="tab">
                                    <i class="material-icons text-lg position-relative">person</i>
                                    <span class="ms-1">Profile Data</span>
                                </a>
                            </li>
                            @can('access customer page')
                                <li class="nav-item">
                                    <a class="nav-link mb-0 px-0 py-1 " data-bs-toggle="tab" data-bs-target="#address" aria-controls="address" href="javascript:;" role="tab" aria-selected="false">
                                        <i class="material-icons text-lg position-relative">maps_home_work</i>
                                        <span class="ms-1">Saved Address</span>
                                    </a>
                                </li>
                            @endcan
                            <li class="nav-item">
                                <a class="nav-link mb-0 px-0 py-1 " data-bs-toggle="tab" data-bs-target="#awaiting" aria-controls="awaiting" href="javascript:;" role="tab" aria-selected="false">
                                    <i class="material-icons text-lg position-relative">lock</i>
                                    <span class="ms-1">Password</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card-body border card-plain border-radius-lg d-flex mt-2">
                <div class="tab-content">
                    {{-- Tab Pane 1 --}}
                    <div class="tab-pane fade show active" id="todays" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
                        <form action="{{ route('profile.update',Auth::user()->id) }}" method="post">
                            @method('PUT')
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group input-group-static mb-4">
                                        <label>First Name</label>
                                        <input type="text" class="form-control" name="name" id="name" value="{{ Auth::user()->name }}" autocomplete="cc-name" autofocus>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="input-group input-group-static mb-4">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control" name="last_name" id="last_name" value="{{ Auth::user()->last_name }}" autocomplete="cc-name" autofocus>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-group input-group-static mb-4">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" id="name" value="{{ Auth::user()->email }}" autocomplete="cc-name" autofocus>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-group input-group-static mb-4">
                                    <label>Phone Number</label>
                                    <input type="text" class="form-control" name="phone_number" id="name" value="{{ Auth::user()->phone_number }}" autocomplete="cc-name" autofocus>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-sm bg-gradient-success">Update</button>
                            </div>
                        </form>
                    </div>
                    @can('access customer page')
                    {{-- Tab Pane 2 --}}
                    <div class="tab-pane fade" id="address" role="tabpanel" aria-labelledby="address-tab" tabindex="0">
                        <form action="{{ route('profile.address', Auth::user()->id) }}" method="post">
                            @method('PUT')
                            @csrf
                            <div class="row">
                                <div class="input-group input-group-static mb-4">
                                    <label>Address</label>
                                    <input type="text" class="form-control" name="address" id="address" value="{{ Auth::user()->defaultAddress ? Auth::user()->defaultAddress->address : '' }}" autofocus>
                                </div>
                                <div class="input-group input-group-static mb-4">
                                    <label>Town</label>
                                    <input type="text" class="form-control" name="town" id="town" value="{{ Auth::user()->defaultAddress ? Auth::user()->defaultAddress->town : '' }}" autofocus>
                                </div>
                                <div class="input-group input-group-static mb-4">
                                    <label>Province</label>
                                    <input type="text" class="form-control" name="province" id="province" value="{{ Auth::user()->defaultAddress ? Auth::user()->defaultAddress->province : '' }}" autofocus>
                                </div>
                                <div class="input-group input-group-static mb-4">
                                    <label>Postal Code</label>
                                    <input type="text" class="form-control" name="postal" id="postal" value="{{ Auth::user()->defaultAddress ? Auth::user()->defaultAddress->postal : '' }}" autofocus>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-sm bg-gradient-success upt-address">Update</button>
                            </div>
                        </form>
                    </div>
                    @endcan
                    {{-- Tab Pane 3 --}}
                    <div class="tab-pane fade" id="awaiting" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
                        <form action="{{ route('profile.updatePassword',Auth::user()->id) }}" method="post">
                            @method('PUT')
                            @csrf
                            <div class="row">
                                <div class="input-group input-group-outline mb-3">
                                    <label class="form-label">New Password</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="input-group input-group-outline mb-3">
                                    <label class="form-label">Password Confirmation</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-sm bg-gradient-success">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
                </div>
            </div>
        </div>
      </div>
      <div class="row">
        
      </div>
    </div>
  </div>

    <div class="position-fixed top-1 end-1 z-index-sticky">
        @if($errors->any())
            @foreach ($errors->all() as $error)
                <div class="toast fade show p-2 mt-2 bg-white" role="alert" aria-live="assertive" id="dangerToast" aria-atomic="true">
                    <div class="toast-header border-0">
                        <i class="material-icons text-danger me-2">campaign</i>
                        <span class="me-auto text-gradient text-danger font-weight-bold">Aborted</span>
                        <i class="fas fa-times text-md ms-3 cursor-pointer" data-bs-dismiss="toast" aria-label="Close"></i>
                    </div>
                    <hr class="horizontal dark m-0">
                    <div class="toast-body">
                        <strong>{{ $error }}</strong>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
@endsection

@section('javascript')
   
@endsection