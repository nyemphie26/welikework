@extends('layouts.template')

@section('content')
    
<div class="container-fluid px-2 px-md-4">
    <div class="page-header min-height-300 border-radius-xl mt-4 text-center" style="background-image: url('https://i.ibb.co/D9DF9fr/285103713-131268889528987-8857143269623988680-n.jpg');">
    </div>
    <div class="card card-body mx-3 mx-md-4 mt-n6">
      <div class="row">
        <div class="w-100">
          @if (Hash::check('password',Auth::user()->password))
            <div class="alert alert-primary text-white" role="alert">
              <span class="alert-icon"><i class="material-icons">priority_high</i></span>
              <span class="text-sm">Danger! Your account using "password" as default password. Change it immediately <a href="{{ route('profile.index') }}" class="alert-link text-white">here</a> !</span>
            </div>
          @endif
          @if (!Auth::user()->defaultAddress)
            <div class="alert alert-secondary alert-dismissible text-white" role="alert">
              <span class="alert-icon"><i class="material-icons">warning</i></span>
              <span class="text-sm">Warning! Update your default address <a href="{{ route('profile.index') }}" class="alert-link text-white">here</a> !</span>
              <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
        </div>
        {{-- <h4 class="text-red">Update your address first!!!</h4> --}}
      </div>
      <div class="row gx-4 mb-2">
        <div class="col-auto">
          <div class="avatar avatar-xl position-relative">
            <img src="{{ asset('img/bruce-mars.jpg') }}" alt="profile_image" class="w-100 border-radius-lg shadow-sm">
          </div>
        </div>
        <div class="col-auto">
          <div class="h-100">
            <h5 class="mb-1">{{ Auth::user()->name }}&nbsp;<span class="mb-0 font-weight-normal text-sm">{{ Auth::user()->last_name }}</span>
            </h5>
            <p class="mb-0 font-weight-normal text-sm">
              {{ Auth::user()->email }}
            </p>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
          <div class="row p-3">
            <div class="col-6"></div>
            <div class="col-6 text-end">
              <a href="{{ route('works.create') }}">
                <button class="btn bg-gradient-primary" type="button">New Work Request</button>
              </a>
            </div>
          </div>

        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive p-0">
                    <table class="table align-items-center mb-0">
                      <thead>
                        <tr>
                          <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Reffence No.</th>
                          <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Requested Date</th>
                          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Service Type</th>
                          <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Scheduling Date</th>
                          <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Estimate Fees</th>
                          <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Status</th>
                          <th class="text-secondary opacity-7"></th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach ($works as $work)
                            <tr>
                                <td class="align-middle text-center">
                                  <span class="text-secondary text-xs font-weight-bold">{{ $work->reff_no }}</span>
                                </td>
                                <td class="align-middle text-center">
                                  <span class="text-secondary text-xs font-weight-bold">{{ date('M d, Y -- H:i', strtotime($work->schedule->req_date)) }}</span>
                                </td>
                                <td>
                                <div class="d-flex px-2 py-1">
                                    <div class="d-flex flex-column justify-content-center">
                                    <h6 class="mb-0 text-sm">{{ $work->service->service_name }}</h6>
                                    {{-- <p class="text-xs text-secondary mb-0">john@creative-tim.com</p> --}}
                                    </div>
                                </div>
                                </td>
                                <td>
                                <p class="text-xs font-weight-bold mb-0">{{$work->schedule->sch_date ? date('M d, Y -- H:i', strtotime($work->schedule->sch_date)) : '-' }}</p>
                                </td>
                                <td class="align-middle text-center">
                                <span class="text-secondary text-xs font-weight-bold">{{ $work->billing ? '$'.Helper::grandTotal($work->id) : '-' }}</span>
                                </td>
                                <td class="align-middle text-center">
                                <span class="{{ $work->status == 'Rejected' ? 'text-danger' : 'text-secondary text-xs' }} font-weight-bold">{{ Helper::generateWorkStatus($work->status) }}</span>
                                </td>
                                <td class="align-middle">
                                {{-- <button type="button" class="btn btn-link p-0 m-0" data-id="{{ $work->id }}" id="showDetails">
                                    Details
                                </button> --}}
                                <button type="button" class="btn btn-link p-0 m-0" onclick="window.location.href='{{ route('details',$work) }}';">
                                  Details
                                </button>
                                {{-- <a class="btn btn-link pe-3 ps-0 mb-0 ms-auto w-25 w-md-auto" href="javascript:;">Assign</a> --}}
                                </td>
                            </tr>
                          @endforeach
                      </tbody>
                    </table>
                  </div>
          </div>
      </div>
    </div>
  </div>

    <div class="position-fixed top-1 end-1 z-index-sticky">
        @if($errors->any())
            @foreach ($errors->all() as $error)
                <div class="toast fade show p-2 mt-2 bg-white" role="alert" aria-live="assertive" id="dangerToast" aria-atomic="true">
                    <div class="toast-header border-0">
                        <i class="material-icons text-danger me-2">campaign</i>
                        <span class="me-auto text-gradient text-danger font-weight-bold">Aborted</span>
                        <i class="fas fa-times text-md ms-3 cursor-pointer" data-bs-dismiss="toast" aria-label="Close"></i>
                    </div>
                    <hr class="horizontal dark m-0">
                    <div class="toast-body">
                        <strong>{{ $error }}</strong>
                    </div>
                </div>
            @endforeach
        @endif
    </div>


    <!-- Modal -->
    <div class="modal modal-lg fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title font-weight-normal" id="detailsHeader"></h5>
              <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                  <div class="col-lg-3">
                          <div class="input-group input-group-static mb-4">
                              <label >Requested Date</label>
                              <input type="text" class="form-control" id="req_date" disabled>
                          </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="input-group input-group-static mb-4">
                          <label >Requested Time</label>
                          <input type="text" class="form-control" id="req_time" disabled>
                      </div>
                  </div>
                  <div class="col-lg-3">
                          <div class="input-group input-group-static mb-4">
                              <label >Scheduling Date</label>
                              <input type="text" class="form-control" id="sch_date" disabled>
                          </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="input-group input-group-static mb-4">
                          <label >Scheduling Time</label>
                          <input type="text" class="form-control" id="sch_time" disabled>
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-lg-6">
                    <div class="input-group input-group-static mb-4">
                        <label >Works / Pick Up Location</label>
                        <input type="text" class="form-control" id="pickUpLoc" disabled>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="input-group input-group-static mb-4">
                        <label >Drop Off Location</label>
                        <input type="text" class="form-control" id="dropOffLoc" disabled>
                    </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-lg-6">
                      <div class="input-group input-group-static mb-4">
                          <label >Customer's Note</label>
                          <input type="text" class="form-control" id="cust_notes" disabled>
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="input-group input-group-static mb-4">
                          <label >Known Preference</label>
                          <input type="text" class="form-control" id="known_pref" disabled>
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-lg-6">
                    <div class="input-group input-group-static mb-4">
                        <label >Employee</label>
                        <input type="text" class="form-control" id="employees" disabled>
                    </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="input-group input-group-static mb-4">
                        <label >Actual Start</label>
                        <input type="text" class="form-control" id="actual_start" disabled>
                      </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="input-group input-group-static mb-4">
                        <label >Actual End</label>
                        <input type="text" class="form-control" id="actual_end" disabled>
                      </div>
                  </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="input-group input-group-static mb-4">
                    <label>Job Description</label>
                    <textarea class="form-control" rows="3" id="job_desc" spellcheck="false" disabled></textarea>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="input-group input-group-static mb-4">
                    <label>Employee Notes</label>
                    <textarea class="form-control" rows="3" id="employee_notes" spellcheck="false" disabled></textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                  <div class="col-lg-3">
                    <div class="input-group input-group-static mb-4">
                        <label>Total Fees</label>
                        <input type="text" class="form-control" id="total_fees" disabled>
                    </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="input-group input-group-static mb-4">
                        <label >Tax</label>
                        <input type="text" class="form-control" id="tax" disabled>
                      </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="input-group input-group-static mb-4">
                        <label >Tipping Fee</label>
                        <input type="text" class="form-control" id="tipping" disabled>
                      </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="input-group input-group-static mb-4">
                        <label >Grand Total</label>
                        <input type="text" class="form-control" id="grand_total" disabled>
                      </div>
                  </div>
              </div>
            </div>
            <div class="modal-footer justify-content-center">
              <button type="button" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
@endsection


@section('javascript')
    <script>

      $(document).ready(function () {

        $('body').on('click', '#showDetails', function (event) {

            event.preventDefault();
            var id = $(this).data('id');
            $.get('works/'+id, function (data) {
                var header = data.data.reff_no+' -- '+data.serv+' '+data.status;
                console.log(data);
                $('#detailsModal').modal('show');
                $('#req_date').val(data.date);
                $('#req_time').val(data.time);
                $('#job_desc').val(data.data.job_desc);
                $('#employee_notes').val(data.data.employee_notes);
                $('#actual_start').val(data.start);
                $('#actual_end').val(data.stop);
                if (data.sch_date) {
                    $('#sch_date').val(data.sch_date);
                    $('#sch_time').val(data.sch_time);
                }
                $('#pickUpLoc').val(data.address);
                if (data.dropOff) {
                  $('#dropOffLoc').val(data.dropOff);
                }
                if (data.employees) {
                  $('#employees').val(data.employees);
                }
                document.getElementById('detailsHeader').innerHTML = header;
                $('#cust_notes').val(data.data.cust_notes);
                $('#known_pref').val(data.data.known_pref);
                if (data.billing) {
                    $('#total_fees').val('$'+data.billing.total_fees);
                    $('#tax').val('$'+data.billing.tax);
                    $('#tipping').val('$'+data.billing.tipping);
                    $('#grand_total').val('$'+data.grandTotal);
                }
                // $('button.action').attr('data-id',data.data.id);
                // $('.formDetails').attr('action','/works/'+id+'?act='+id);

            })
        });

        $('#detailsModal').on('hidden.bs.modal', function (e) {
          $(this).find("input,textarea").val('').end();
        })
        
      }); 

    </script>
@endsection