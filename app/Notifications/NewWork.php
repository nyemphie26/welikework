<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewWork extends Notification
{
    use Queueable;
    private $newWorkData;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($newWorkData)
    {
        $this->newWorkData = $newWorkData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject($this->newWorkData['subject'])
                    ->greeting($this->newWorkData['title'])
                    ->line($this->newWorkData['body'])
                    ->action($this->newWorkData['workText'], $this->newWorkData['url'])
                    ->line($this->newWorkData['thankyou']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    //set and validate in gmail to use alternatif email from
    //Settings -> Accounts -> Send mail as -> Add another email address you own
}
