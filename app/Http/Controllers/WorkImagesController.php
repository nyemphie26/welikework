<?php

namespace App\Http\Controllers;

use App\Models\Work;
use App\Models\WorkImages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class WorkImagesController extends Controller
{
    public function show(Request $request)
    {
        // return $request->id;
        $work = Work::find($request->id);
        return view('auth.dashboard.workImage', compact('work'));
    }

    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'images' => 'image|file|max:3000',
        ]);
        $work = Work::find($request->id);
        // return $work->reff_no;

        if ($request->file('images')) {
            $img_path = $request->file('images')->store('workImages/'.$work->reff_no);
        };

        DB::transaction(function() use ($request, $img_path){
            WorkImages::create([
                'work_id'   => $request->id,
                'user_id'   => Auth::id(),
                'img_desc'  =>$request->imageDesc,
                'img_path'  => $img_path,
             ]);
        });

        // $redirect = redirect()->route("workImages.show", $request->id);

        return redirect()->back()->with([
            'message'    => "Landing Page Setting(s) Has been updated",
            'alert-type' => 'success',
        ]);
    }


    public function destroy(Request $request)
    {
        // $redirect = redirect()->route("workImages.show", $request->id);

        DB::transaction(function() use ($request){
            WorkImages::destroy($request->id);
        });

        return response()->json([
            'success' => true,
            'message' => 'Successfully deleted photo with id '.$request->id
        ]);
    }
}
