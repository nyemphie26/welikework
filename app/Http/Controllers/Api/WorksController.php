<?php

namespace App\Http\Controllers\Api;

use App\Models\WorkAssign;
use App\Http\Controllers\Controller;
use App\Http\Resources\WorkResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WorksController extends Controller
{
    public function index()
    {
        // $works = WorkAssign::where('user_id',Auth::id())->with(['work.location','work.billing','work.dropOffLocation','work.cust','work.service','work.schedule' => function ($query) 
        //                                                         {
        //                                                             $query->whereDate('sch_date', date('Y-m-d'));
        //                                                         }])
        //                                                 ->get()
        //                                                 ->where('work.status','Ready')
        //                                                 ->where('work.schedule','!=',null);
        // $user = Auth::user();
        // return response()->json([
        //     'id' => $works
        // ], 200);
        // return new WorkResource(true, 'Today Work\'s List', $works);
        // return new PostResource(true, 'List Data Posts', $posts);


        // $works = WorkAssign::where('user_id',Auth::id())->with
        //                             ([
        //                             'work.schedule',
        //                             'work.assigns.user'
        //                             ])
        //                     ->get(['work_id']);
        
        $works = DB::table('work_assigns')
                    ->join('works','work_assigns.work_id','=','works.id')
                    ->join('services','works.service_id','=','services.id')
                    ->join('categories','categories.id','=','services.category_id')
                    ->leftjoin('schedules','works.id','=','schedules.work_id')
                    ->leftjoin('locations','works.id','=','locations.work_id')
                    ->leftjoin('drop_off_locations','works.id','=','drop_off_locations.work_id')
                    ->leftjoin('billings','works.id','=','billings.work_id')
                    ->where('work_assigns.user_id','=',Auth::id())
                    ->select(
                        'works.id',
                        'works.reff_no',
                        'services.service_name as service',
                        'categories.category_name as category',
                        'works.job_desc',
                        'works.cust_notes',
                        'works.known_pref',
                        'works.employee_notes',
                        DB::raw("CONCAT(locations.address,', ',locations.town,', ',locations.province,', ',locations.postal) AS pickUp"),
                        DB::raw("CONCAT(drop_off_locations.address,', ',drop_off_locations.town,', ',drop_off_locations.province,', ',drop_off_locations.postal) AS dropOff"),
                        'billings.tipping',
                        'schedules.sch_date',
                        'works.status'
                        )
                    ->get();
        // json_encode($works['tipping'],JSON_PRESERVE_ZERO_FRACTION);
        return response()->json([
            'data' => $works
        ], 200);
    }

}
