<?php

namespace App\Http\Controllers;

use App\Models\Pricelist;
use App\Models\Setting;
use Illuminate\Http\Request;

class LandingPageController extends Controller
{
    public function index()
    {
        $show = Setting::all()->keyBy('setting_link');
        $pricelists = Pricelist::all();
        // dd ($show);
        
        return view('welcome', compact('show','pricelists'));
    }
    
    public function about()
    {
        $show = Setting::all()->keyBy('setting_link');
        return view('aboutUs', compact('show'));
    }
}
