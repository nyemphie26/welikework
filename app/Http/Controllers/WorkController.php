<?php

namespace App\Http\Controllers;

use App\Helpers\generateReffNo as HelpersGenerateReffNo;
use App\Helpers\generateReffNo\generateReffNo as GenerateReffNoGenerateReffNo;
use App\Models\User;
use App\Models\Work;
use App\Models\Status;
use App\Helpers\Helper;
use App\Models\Billing;
use App\Models\Service;
use App\Models\Category;
use App\Models\Location;
use App\Models\Schedule;
use App\Models\WorkAssign;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\DefaultAddress;
use App\Models\DropOffLocation;
use App\Notifications\NewWork;
use generateReffNo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;

use function PHPUnit\Framework\isEmpty;

class WorkController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasRole('admin') )
        {
            $works = Work::where('status','New')->get();
            return view('auth.works.incoming', compact('works'));
        }
        else
        {
            return redirect('dashboard');
        }
    }

    public function accepted()
    {
        if(Auth::user()->hasRole('admin') )
        {
            $works = Work::where('status','Approved')->get();
            $employees = User::role('employee')->get();
            $assignedWorks = Work::where('status', 'Ready')->orWhere('status', 'Not Ready')->with('assigns','cust')->get();
    
            return view('auth.works.accepted', compact('works','employees','assignedWorks'));
        }
        else
        {
            return redirect('dashboard');
        }
    }
    
    public function finishedWorks()
    {
        if(Auth::user()->hasRole('admin') )
        {
            $works = Work::where('status','Finished')->get();
            // return $works;
            return view('auth.works.finished', compact('works'));
        }
        else
        {
            return redirect('dashboard');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $users = User::role('user')->get();
        // $cust = $users->hasRole('user');
        // return $users;
        $savedAddress = Auth::user()->defaultAddress;
        return view('auth.works.edit-add', compact('users','categories','savedAddress'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::transaction(function() use ($request){
                $user = User::find($request->user);
    
                if (isset($request->newCust)) {
                    //new Customer
                    $validated = $request->validate([
                        'name' => 'required|alpha|max:25',
                        'last_name' => 'required|alpha|max:25',
                        'phone_number' => 'required|numeric|max_digits:25',
                        'email' => 'required|string|email|unique:users',
                    ]);
    
                    $user = User::create([
                        'name' => $validated['name'],
                        'last_name' => $validated['last_name'],
                        'phone_number' => $validated['phone_number'],
                        'email' => $validated['email'],
                        'password' => Hash::make('password'),
                    ]);
            
                    $user->assignRole('user');
                    $cust_id = $user->id;
    
                    if (isset($request->saveAddress)) {
                        //if saveAddress on -> save pickUpLoc
                        DefaultAddress::create([
                            'user_id'   => $cust_id,
                            'address'   => $request->pickUpLoc,
                            'town'      => $request->pickUpTown,
                            'province'  => $request->pickUpProv,
                            'postal'    => $request->pickUpPostal
                        ]);
                    } 
                    elseif (isset($request->saveAddress2)) {
                        //if saveAddress2 on -> save dropOffLoc
                        DefaultAddress::create([
                            'user_id'   => $cust_id,
                            'address'   => $request->dropOffLoc,
                            'town'      => $request->dropOffTown,
                            'province'  => $request->dropOffProv,
                            'postal'    => $request->dropOffPostal
                        ]);
                    } 
                } 
                else {
                    if (Auth::user()->hasRole('user')) {
                        // input by customer
                        $cust_id = Auth::id();
                    } 
                    else {
                        // existing Customer input by admin
                        $cust_id = $request->cust_id;
                    }
                    
    
                }
                
                //generate refference number
                $generateReffNo = new HelpersGenerateReffNo;
                $reffNo = $generateReffNo->newReffNo($cust_id);
                
                $work = Work::create([
                    'reff_no'    => $reffNo,
                    'service_id' => $request->service_id,
                    'user_id'    => $cust_id,
                    'cust_notes' => $request->cust_notes,
                    'known_pref' => $request->known_pref,
                    // 'status_id'  => $workStatus,   
                ]);
                Location::create([
                    'work_id'   => $work->id,
                    'address'   => $request->pickUpLoc,
                    'town'      => $request->pickUpTown,
                    'province'  => $request->pickUpProv,
                    'postal'    => $request->pickUpPostal,
                ]);
    
                if (isset($request->dropOffLoc)) {
                    // drop off location is enabled and have value
                    DropOffLocation::create([
                        'work_id'   => $work->id,
                        'address'   => $request->dropOffLoc,
                        'town'      => $request->dropOffTown,
                        'province'  => $request->dropOffProv,
                        'postal'    => $request->dropOffPostal,
                    ]);
                }
    
                Schedule::create([
                    'work_id'   => $work->id,
                    'req_date'  => $request->req_date,
                ]);
            });
        } catch (\Throwable $th) {
            if (Auth::user()->hasRole('user')) {
                $redirect = redirect()->route("dashboard");
            } 
            else {
                $redirect = redirect()->route("works.index");
            }
            return $redirect->with([
                'message'    => "Request Work is failed. Message : ".$th,
                'alert-type' => 'error',
            ]);


        }

        if (Auth::user()->hasRole('user')) {
            // input by customer
            $user = Auth::user()->email;
            $newWorkData = [
                'subject' => 'New Work Requests',
                'title' => 'Hello admin',
                'body' => 'You\'ve got new work requests from '.$user,
                'workText' => 'Click here to see the requests',
                'url' => url('/works'),
                'thankyou' => 'Check immadiately to respond your customer'
            ];

            $admin = User::role('admin')->get();
            Notification::send($admin, new NewWork($newWorkData));

            $redirect = redirect()->route("dashboard");
        } 
        else {
            // existing Customer input by admin
            $redirect = redirect()->route("works.index");
        }

        return $redirect->with([
            'message'    => "New Work Request Has been added",
            'alert-type' => 'success',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $employees = $request->employees;
        $self = Auth::id();
        //delete self assigned
        if (($key = array_search($self, $employees)) !== false) {
            unset($employees[$key]);
        }

        $assignedEmpl = User::whereIn('id',$employees)->get();
        // return response()->json([
        //     'success' => true,
        //     'message' => $assignedEmpl
        // ]);
        
        try {
            db::transaction(function() use ($request){
                Schedule::where('work_id', $request->work)->update(['sch_date'=>$request->sch_date]);
                Work::where('id', $request->work)->update(['job_desc'=>$request->job_desc]);
                //delete old employee
                WorkAssign::where('work_id',$request->work)->delete();
                //assign new employee
                foreach ($request->employees as $key => $value) {
                    WorkAssign::create([
                        'work_id' => $request->work,
                        'user_id' => $value
                    ]);
                }
            });

            //send email notification
            $newWorkData = [
                'subject' => 'New Assigned Work',
                'title' => 'Hello,',
                'body' => 'You\'ve got New Work',
                'workText' => 'Click here to see the requests',
                'url' => url('/dashboard'),
                'thankyou' => 'Have a nice day. Enjoy your work!'
            ];


        } catch (\Exception $th) {
            return response()->json([
                'success' => false,
                'message' => $th
            ]);
        }

        // Notification::send($assignedEmpl, new NewWork($newWorkData));
        
        return response()->json([
            'success' => true,
            'message' => 'Work has been updated'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Work $work)
    {
        // if ($request->status == 'assign') {
            
        //     return response()->json([
        //         'success' => true,
        //         'message' => $request->employees
        //     ]);
        // }
        try
        {
            DB::transaction(function() use ($request, $work){
                if ($request->status == 'accept') {
                    Work::find($work->id)->update(['status'=>'Approved']);
                    Billing::create([
                        'work_id'       => $work->id,
                        'total_fees'    => $request->fees,
                        'tax'           => Helper::tax($request->fees),
                    ]);
                }
                if ($request->status == 'pending') {
                    //update status as pending
                }
                if ($request->status == 'reject') {
                    //update status as rejected
                    Work::find($work->id)->update(['status'=>'Rejected']);
                }
                if ($request->status == 'assign') {
                    //update Status and add job desc
                    Work::find($work->id)->update(['status'=>'Ready', 'job_desc' => $request->job_desc]);
                    //add scheduling date
                    Schedule::where('work_id', $work->id)->update(['sch_date' => $request->sch_date]);
                    //assign employee
                    foreach ($request->employees as $key => $value) {
                        WorkAssign::create([
                            'work_id' => $work->id,
                            'user_id' => $value
                        ]);
                    }
                }
            });
        }
        catch (\Exception $e)
        {
            return response()->json([
                'success' => false,
                'message' => $e
            ]);

        }
        
        return response()->json([
            'success' => true,
            'message' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function destroy(Work $work)
    {
        //
    }

    public function getDetails($id)
    {
        $data = Work::find($id);
        
        //location config
        $address = $data->location->address;
        $town = $data->location->town ? ', '.$data->location->town : '';
        $prov = $data->location->province ? ', '.$data->location->province : '';
        $postal = $data->location->postal ? ' ('.$data->location->postal.')' : '';
        $loc = $address.$town.$prov.$postal;
        $dropOff = null;
        if ($data->dropOffLocation) {
            $address1 = $data->dropOffLocation->address;
            $town1 = $data->dropOffLocation->town ? ', '.$data->dropOffLocation->town : '';
            $prov1 = $data->dropOffLocation->province ? ', '.$data->dropOffLocation->province : '';
            $postal1 = $data->dropOffLocation->postal ? ' ('.$data->dropOffLocation->postal.')' : '';
            $dropOff = $address1.$town1.$prov1.$postal1;
        }

        // schedule config
        $sch_date = null;
        $sch_time = null;
        if ($data->schedule->sch_date) {
            $sch_date = date('M d, Y',strtotime($data->schedule->sch_date));
            $sch_time = date('H:i',strtotime($data->schedule->sch_date));
        }
        $start_date = null;
        $stop_date = null;
        if ($data->schedule->actual_start) {
            $start_date = date('M d, Y - H:i',strtotime($data->schedule->actual_start));
            $stop_date = date('M d, Y - H:i',strtotime($data->schedule->actual_stop));
        }

        //billing config
        $billing = null;
        $grandTotal = null;
        if($data->billing)
        {
            $billing = $data->billing;
            $grandTotal = Helper::grandTotal($data->id);
        }

        //employee config
        $employee = null;
        $employeeId = null;
        $assigned = WorkAssign::where('work_id',$id)->with('work','user')->get();
        if ($assigned->isNotEmpty()) {
            foreach ($assigned as $key => $value) {
                $employees[] = $value->user->name.' '.$value->user->last_name;
                $employeesId[] = $value->user->id;
            }
            $employee = implode(', ', $employees);
            $employeeId = implode(',',$employeesId);
        }

        //image array
        $img = $data->images;
        // return $employee;
        
        // $assign = WorkAssign::where('work_id', $data->id)->with('employee')->get();
        // if ($data->assigns) {
        //     $employees = implode(', ', $assign->employee->name);
        // }
        

        if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('employee') ){
            return response()->json([
                'data'          => $data,
                'user'          => $data->cust,
                'date'          => date('M d, Y',strtotime($data->schedule->req_date)),
                'time'          => date('H:i',strtotime($data->schedule->req_date)),
                'serv'          => $data->service->service_name,
                'cat'           => $data->service->category->category_name,
                'address'       => $loc,
                'dropOff'       => $dropOff,
                'sch_date'      => $sch_date,
                'sch_time'      => $sch_time,
                'start'         => $start_date,
                'stop'          => $stop_date,
                'employees'     => $employee,
                'employeesId'   => $employeeId,
                'billing'       => $billing,
                'grandTotal'    => $grandTotal,
                'status'        => Helper::generateWorkStatus($data->status),
                'images'        => $img
            ]);
        }
        else
        {
            if($data->user_id == auth()->id()){
                return response()->json([
                    'data'          => $data,
                    'user'          => $data->cust,
                    'date'          => date('M d, Y',strtotime($data->schedule->req_date)),
                    'time'          => date('H:i',strtotime($data->schedule->req_date)),
                    'sch_date'      => $sch_date,
                    'sch_time'      => $sch_time,
                    'start'         => $start_date,
                    'stop'          => $stop_date,
                    'serv'          => $data->service->service_name,
                    'address'       => $loc,
                    'dropOff'       => $dropOff,
                    'billing'       => $billing,
                    'employees'     => $employee,
                    'grandTotal'    => $grandTotal,
                    'status'        => Helper::generateWorkStatus($data->status)
                ]);
            }
            return "Only the owner can see this data.";

        }
    }

    public function changeWorkStatus(Request $request)
    {
        if ($request->status == '1') {
            $newStatus = 'Not Ready';
        }
        else{
            $newStatus = 'Ready';
        }
        Work::where('id', $request->id)->update(['status' => $newStatus]);
        return response()->json([
            'success'=>'Status has been changed'
        ]);
    }

    public function finish(Request $request)
    {
        try {
            db::transaction(function() use ($request){
                Schedule::where('work_id',$request->id)->update(['actual_start'=>$request->start, 'actual_stop'=>$request->stop]);
                Billing::where('work_id',$request->id)->update(['tipping'=>$request->tipping, 'status'=>'Paid']);
                Work::find($request->id)->update(['employee_notes'=>$request->notes,'status'=>'Finished']);
            });
        } catch (\Exception $th) {
            return response()->json([
                'success' => false,
                'message' => $th
            ]);
        }
        
        return response()->json([
            'success' => true,
            'message' => 'Work Done'
        ]);
    }

    public function fetchService(Request $request)
    {        
        $data['services'] = Service::where("category_id", $request->id)->get();

        return response()->json($data);
    }

    public function getSavedAddress(Request $request)
    {
        $data = DefaultAddress::where("user_id", $request->id)->first();
        return response()->json(['data'=>$data]);
        
    }

    public function addPhoto(Request $request)
    {

        try {
            //code...
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $request->files
        ]);
    }

}
