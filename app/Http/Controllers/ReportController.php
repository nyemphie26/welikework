<?php

namespace App\Http\Controllers;

use App\Exports\WorkExport;
use App\Models\Work;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    public function index()
    {
        // $works = Work::where('status',['Finished', 'Not Ready'])->get();
        // return $works;
        return view('auth.report.index');
    }

    public function getReport(Request $request)
    {
        $start      = $request->start;
        $end        = $request->end;
        $value      = $request->status;
        $data = null;

        if($value == 'finished')
        {
            $data   = $this->getFinishedWorks($start,$end);
        }
        elseif ($value == 'unfinished')
        {
            $data   = $this->getUnFinishedWorks($start,$end);
        }
        elseif ($value == 'rejected')
        {
            $data   = $this->getRejectedWorks($start,$end);
        }


        // $data = $request->status;
        
        return response()->json($data);
    }

    public function getFinishedWorks($start_date, $end_date)
    {
        $works = Work::with(['cust','schedule','service','billing'])->get()->where('status', 'Finished')->whereBetween('schedule.actual_stop',[$start_date, $end_date]);
        return $works;
    }

    public function getUnFinishedWorks($start_date, $end_date)
    {
        $works = Work::with('schedule')->get()->where('status', 'Ready')->whereBetween('schedule.actual_start',[$start_date, $end_date]);
        return $works;
    }

    public function getRejectedWorks($start_date, $end_date)
    {
        $works = Work::with('schedule')->get()->where('status', 'Rejected')->whereBetween('schedule.req_date',[$start_date, $end_date]);
        return $works;
    }

    public function exportExcel($start, $end)
    {
        // return $start;
        $from = date('M d, Y', strtotime($start));
        $to = date('M d, Y', strtotime($end));
        $fileName = 'WorkReport-'.$from.'to'.$to;

        return Excel::download(new WorkExport($start,$end), $fileName.'.xlsx');
    }
}
