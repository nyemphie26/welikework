<?php

namespace App\Http\Controllers;

use App\Models\DefaultAddress;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $user = User::where()
        return view('auth.profile');
    }

    public function updateAddress(Request $request, $id)
    {
        $validated = $request->validate([
            'address' => 'required|string',
            'town' => 'required|string|max:35',
            'province' => 'required|string|max:45',
            'postal' => 'required|string|max:10',
        ]);
        // return $validated;

        DB::transaction(function() use ($validated, $id){
            $existing = DefaultAddress::where('user_id', $id)->first();
            if($existing){
                $existing->update($validated);
            }
            else{
                $validated['user_id'] = $id;
                DefaultAddress::create($validated);
            }
        });

        $redirect = redirect()->route("profile.index");

        return $redirect->with([
            'message'    => "Address Has been updated",
            'alert-type' => 'success',
        ]);
    }

    public function update(Request $request, $id)
    {

        $validated = $request->validate([
            'name' => 'required|alpha|max:25',
            'last_name' => 'required|alpha|max:25',
            'phone_number' => 'required|numeric|max_digits:25',
            'email' => 'required|string|email',
        ]);
        // return $validated;

        DB::transaction(function() use ($validated, $id){
            User::where('id', $id)->update($validated);
        });

        $redirect = redirect()->route("profile.index");

        return $redirect->with([
            'message'    => "Profile Has been updated",
            'alert-type' => 'success',
        ]);
    }

    public function updatePassword(Request $request, $id)
    {
        $validated = $request->validate([
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        // return $validated;

        DB::transaction(function() use ($request, $id){
            User::where('id', $id)->update(['password' => bcrypt($request->password)]);
        });

        $redirect = redirect()->route("profile.index");

        return $redirect->with([
            'message'    => "Profile Has been updated",
            'alert-type' => 'success',
        ]);
    }
}
