<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    public function index()
    {
        $employees = User::role('employee')->get();
        // $employees = User::find('1');
        // return $employees->();
        return view('auth.employees.index', compact('employees'));
    }

    public function store(Request $request)
    {
        // return $request;
        $validated = $request->validate([
            'name' => 'required|alpha|max:25',
            'last_name' => 'required|alpha|max:25',
            'phone_number' => 'required|numeric|max_digits:25',
            'email' => 'required|string|email|unique:users',
        ]);
        // return $validated;

        DB::transaction(function() use ($validated){
            $user = User::create([
                'name' => $validated['name'],
                'last_name' => $validated['last_name'],
                'phone_number' => $validated['phone_number'],
                'email' => $validated['email'],
                'password' => Hash::make('12345678'),
            ]);
    
            $user->assignRole('employee');
        });

        $redirect = redirect()->route("employees.index");

        return $redirect->with([
            'message'    => "Employee Has been added",
            'alert-type' => 'success',
        ]);
    }

    public function changeStatus(Request $request)
    {
        // return $request;
        // $verifikasi = LembarVerifikasiStaseMahasiswa::find($request->kriteria);
        // $verifikasi->id_lembar_verifikasi_stase = $request->status;
        // $user->save();
        
        // $this->authorize('preceptorAkademik');

        // $verifikasi = LembarVerifikasiStaseMahasiswa::updateOrCreate(
        //     ['id_lembar_verifikasi_stase' => $request->id_verifikasi, 'id_mahasiswa' => $request->id_mahasiswa],
        //     ['status' => $request->status, 'id_preceptor' => $request->id_preceptor]
        // );
            // $status = User::where('id', $request->user)->update(['status' => $request->status]);
            $user = User::find($request->user);
            if ($request->status == 0) {
                $user->removeRole('admin');
                $message = $user->name.' now is Admin';
            } else {
                $user->assignRole('admin');
                $message = $user->name.' is no longer as Admin';
            }
            
            return response()->json(['status'=> $request->status,'success'=> $message]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = User::find($id);
        
        $rules = [
            'name' => 'required|alpha|max:25',
            'last_name' => 'required|alpha|max:25',
            'phone_number' => 'required|numeric|max_digits:25',
        ];
        if($request->email != $employee->email)
        {
            $rules['email'] = 'required|string|email|unique:users';
        }
        
        $validated = $request->validate($rules);

        DB::transaction(function() use ($validated, $id){
            User::where('id',$id)->update($validated);
        });

        $redirect = redirect()->route("employees.index");

        return $redirect->with([
            'message'    => "Employee Has been Updated",
            'alert-type' => 'success',
        ]);
    }
}
