<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Pricelist;
use App\Models\Service;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PhpParser\Node\Expr\Print_;

class SettingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $services = Pricelist::all();
        $tabpills = Setting::all()->groupBy('group');
        // ddd ($tabpills);
        // return $tabpills->key;
        // $settings = Setting::
        return view('auth.landing.index', compact('categories', 'tabpills', 'services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting, $id)
    {
        $service = Pricelist::find($id);

        return view('auth.landing.service', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        // return $request;
        $path = '';
        $file = false;
        foreach ($request->except('_token') as $k =>$v) {
            // Setting::where('setting_link', $k)->update(['value'=>$v]);
            if(strpos($k, 'old_') === false && strpos($k, '_img') !== false)
            {
                $file = true;
                $reqName = $k;
                $oldImagePath = 'old_'.$reqName;
                if($request->$oldImagePath){
                    Storage::delete($request->$oldImagePath);
                }
                $path = $request->file($reqName)->store('LandingPageImages');
            }
        } 


        DB::transaction(function() use ($request,$path){
            foreach ($request->except('_token') as $k =>$v) {
                if(strpos($k, 'old_') === false && strpos($k, '_img') !== false)
                {
                    Setting::where('setting_link', $k)->update(['value'=>$path]);
                }
                else
                {
                    Setting::where('setting_link', $k)->update(['value'=>$v]);
                }
            } 
        });

        $redirect = redirect()->route("admin.landing");

        return $redirect->with([
            'message'    => "Landing Page Setting(s) Has been updated",
            'alert-type' => 'success',
        ]);
    }

    public function storeService(Request $request)
    {
        // return $request;
        // $fileImg = ($request->file('image')) ? 'file' : 'zero' ;
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'body' => 'required',
            'image' => 'image|file|max:3000',
            'footer' => 'required|max:255'
        ]);

        if ($request->file('image')) {
            $validatedData['image'] = $request->file('image')->store('LandingPageImages');
        };

        DB::transaction(function() use ($validatedData){
            // Pricelist::create([
            //     'title' => $request->title,
            //     'body' => $request->body,
            //     'footer' => $request->footer,
            // ]);
            Pricelist::create($validatedData);
        });

        $redirect = redirect()->route("admin.landing");

        return $redirect->with([
            'message'    => "Landing Page Setting(s) Has been updated",
            'alert-type' => 'success',
        ]);
    }

    public function updateService(Request $request, $id)
    {
        
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'body' => 'required',
            'image' => 'image|file|max:3000',
            'footer' => 'required|max:255'
        ]);

        if ($request->file('image')) {
            if ($request->oldImage) {
                Storage::delete($request->oldImage);
            }
            $validatedData['image'] = $request->file('image')->store('LandingPageImages');
        };

        // return $request;
        DB::transaction(function() use ($validatedData, $id){
            // $data = [
            //     "title"     => $request->title,
            //     "body"      => $request->body,
            //     "footer"    => $request->footer,
            // ];
            Pricelist::where('id', $id)->update($validatedData);
        });

        $redirect = redirect()->route("admin.landing");

        return $redirect->with([
            'message'    => "Service Has been updated",
            'alert-type' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // return $id;
        $service = Pricelist::find($id);

        DB::transaction(function() use ($service){
            if ($service->image) {
                Storage::delete($service->image);
            }
            Pricelist::find($service->id)->delete(); 
        });

        $redirect = redirect()->route("admin.landing");

        return $redirect->with([
            'message'    => "Service Has been deleted",
            'alert-type' => 'danger',
        ]);
    }
}
