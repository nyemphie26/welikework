<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CustomersController extends Controller
{
    public function index()
    {
        $customers =    User::role('user')
                        ->withCount([
                            'works as All',
                            'works as Approved' => function (Builder $query){
                                $query->where('status','=','Approved');
                            },
                            'works as Rejected' => function (Builder $query){
                                $query->where('status','=','Rejected');
                            },
                        ])
                        ->get();
        // return $customers;
        return view('auth.customers.index', compact('customers'));
    }
    
    public function store(Request $request)
    {
        // return $request;
        $validated = $request->validate([
            'name' => 'required|alpha|max:25',
            'last_name' => 'required|alpha|max:25',
            'phone_number' => 'required|numeric|max_digits:25',
            'email' => 'required|string|email|unique:users',
        ]);
        // return $validated;

        DB::transaction(function() use ($validated){
            $user = User::create([
                'name' => $validated['name'],
                'last_name' => $validated['last_name'],
                'phone_number' => $validated['phone_number'],
                'email' => $validated['email'],
                'password' => Hash::make('password'),
            ]);
    
            $user->assignRole('user');
        });

        $redirect = redirect()->route("customers.index");

        return $redirect->with([
            'message'    => "Customer Has been added",
            'alert-type' => 'success',
        ]);
    }

    public function update(Request $request, $id)
    {
        $cust = User::find($id);
        
        $rules = [
            'name' => 'required|alpha|max:25',
            'last_name' => 'required|alpha|max:25',
            'phone_number' => 'required|numeric|max_digits:25',
        ];
        if($request->email != $cust->email)
        {
            $rules['email'] = 'required|string|email|unique:users';
        }
        
        $validated = $request->validate($rules);

        DB::transaction(function() use ($validated, $id){
            User::where('id',$id)->update($validated);
        });

        $redirect = redirect()->route("customers.index");

        return $redirect->with([
            'message'    => "Customer Has been Updated",
            'alert-type' => 'success',
        ]);
    }
}
