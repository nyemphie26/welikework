<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        $services = Service::all();
        // return view('auth.ServicesCategories.index', compact('categories','services'));
        return view('auth.ServicesCategories.index', compact('categories','services'));
    }
    public function store(Request $request)
    {
        // return $request;
        
        DB::transaction(function() use ($request){

            $storedCategory = Category::where('category_name',$request->category_name)->first();
            
            if($storedCategory == null){

                // $storedCategory = $request->category_name;
                
                $storedCategory = new Category();
                $storedCategory->category_name = $request->category_name;
                $storedCategory->save();
                $storedCategory->id;
            }

            Service::create([
                'service_name'      => $request->service_name,
                'category_id'       => $storedCategory->id
            ]);
        });

        $redirect = redirect()->route("services.index");

        return $redirect->with([
            'message'    => "Service has been Added",
            'alert-type' => 'success',
        ]);
    }

    public function show($id)
    {
        $categories = Category::all();
        $service = Service::find($id);
        return view('auth.ServicesCategories.edit',compact('service','categories'));
    }

    public function update(Request $request, $id)
    {
        // return $id;

        DB::transaction(function() use ($request,$id){

            $storedCategory = Category::where('category_name',$request->category_name)->first();
            
            if($storedCategory == null){

                $storedCategory = new Category();
                $storedCategory->category_name = $request->category_name;
                $storedCategory->save();
                $storedCategory->id;
            }

            // Service::create([
            //     'service_name'      => $request->service_name,
            //     'category_id'       => $storedCategory->id
            // ]);
            $data = [
                'service_name'      => $request->service_name,
                'category_id'       => $storedCategory->id
            ];
            Service::find($id)->update($data);
        });

        $redirect = redirect()->route("services.index");

        return $redirect->with([
            'message'    => "Service has been Updated",
            'alert-type' => 'success',
        ]);
    }

    public function storeCategory(Request $request)
    {
        // return $request;
        
        DB::transaction(function() use ($request){

            Category::create([
                'category_name'      => $request->category_name
            ]);
        });

        $redirect = redirect()->route("services.index");

        return $redirect->with([
            'message'    => "Category has been Added",
            'alert-type' => 'success',
        ]);
    }

    public function updateCategory(Request $request, $id)
    {
        // return $id;

        DB::transaction(function() use ($request,$id){
            $data = [
                'category_name' => $request->category_name
            ];
            Category::find($id)->update($data);
        });

        $redirect = redirect()->route("services.index");

        return $redirect->with([
            'message'    => "Category has been Updated",
            'alert-type' => 'success',
        ]);
    }
}
