<?php

namespace App\Http\Controllers;

use App\Models\DropOffLocation;
use App\Models\Location;
use App\Models\User;
use App\Models\Work;
use App\Models\WorkAssign;
use App\Notifications\NewWork;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->user()->can('access customer page'))
        {
            $works = Auth::user()->works;
            return view('customerPage.index', compact('works'));
        }
        
        else
        {
            $date = date('Y-m-d');
            
            //get today works
            $todayWorks = WorkAssign::where('user_id',Auth::id())->with(['work.schedule' => function ($query) 
                                                                            {
                                                                                $query->whereDate('sch_date', date('Y-m-d'));
                                                                            }
                                                                        ])
                                                                ->get()
                                                                ->where('work.status','Ready')
                                                                ->where('work.schedule','!=',null);
            
            //get All Works
            // $works = WorkAssign::where('user_id',Auth::id())->with(['work.schedule' => function ($query) {$query->whereDate('sch_date', '>' ,date('Y-m-d'));}])->get()->where('work.schedule','!=',null);
            $works = WorkAssign::where('user_id',Auth::id())->with(['work.schedule'])
                                                            ->get()
                                                            ->where('work.schedule','!=',null)
                                                            ->where('work.status','!=','Finished');

            //get finished works
            $finishedWorks = WorkAssign::where('user_id',Auth::id())->with('work')
                                                                    ->get()
                                                                    ->where('work.status','Finished');

            return view('auth.dashboard.index', compact('works','todayWorks','finishedWorks'));
        }
    }

    public function show(Work $work)
    {
        $pickUpLoc = Location::where('work_id',$work->id)
                            ->select(
                                DB::raw("
                                    CONCAT(locations.address,
                                    ', ',
                                    locations.town,
                                    ', ',
                                    locations.province,
                                    ', ',
                                    locations.postal) as address")
                                )
                            ->first();
        $dropOffLoc = DropOffLocation::where('work_id',$work->id)
                            ->select(
                                DB::raw("
                                    CONCAT(drop_off_locations.address,
                                    ', ',
                                    drop_off_locations.town,
                                    ', ',
                                    drop_off_locations.province,
                                    ', ',
                                    drop_off_locations.postal) as address")
                                )
                            ->first();
        
        $asgEmployees = null;
        $assigned = WorkAssign::where('work_id',$work->id)->with('work','user')->get();
        if ($assigned->isNotEmpty()) {
            foreach ($assigned as $key => $value) {
                $employees[] = $value->user->name.' '.$value->user->last_name;
            }
            $asgEmployees = implode(', ', $employees);
        }
        
        // return $work->images;
        return view('auth.dashboard.detailsPage', compact('work','pickUpLoc','dropOffLoc','asgEmployees'));
    }

    public function exportToday()
    {
        // return $start;
        $date = date('F d, Y');
        $fileName = 'Work_Lists-'.$date.'.pdf';
        $works = WorkAssign::where('user_id',Auth::id())->with(['work.schedule' => function ($query) 
                                    {
                                        $query->whereDate('sch_date', date('Y-m-d'))->orderBy('sch_date','asc');
                                    },
                                    'work.assigns.user'
                                ])
                            ->get()
                            ->where('work.status','Ready')
                            ->where('work.schedule','!=',null);
        
        $pdf = Pdf::loadView('auth.dashboard.exportToday', ['works'=>$works])->setPaper('a4','landscape');
        return $pdf->download($fileName);
        
        
    }
    
    public function exportWork(Request $request)
    {
        $work = Work::find($request->id);

        $fileName = 'Work - '.$work->reff_no.'('.date('F d, Y - H:i', strtotime($work->schedule->sch_date)).')'.'.pdf';
        
        // return view('auth.works.exportWork', compact('work'));
        $pdf = Pdf::loadView('auth.dashboard.exportWork', ['work'=>$work])->setPaper('a4','landscape');
        return $pdf->download($fileName);

    }

    public function sendEmail()
    {
        $user = Auth::user()->email;
        // return $user;
        $newWorkData = [
            'subject' => 'New Work Requests',
            'title' => 'Hello admin',
            'body' => 'You\'ve got new work requests from '.$user,
            'workText' => 'Click here to see the requests',
            'url' => url('/works'),
            'thankyou' => 'Please review the work requests'
        ];

        // $admin = User::role('admin')->get();
        $admin = User::first();

        // $admin->notify(new NewWork($newWorkData));
        Notification::send($admin, new NewWork($newWorkData));
        // return (new NewWork($newWorkData))->toMail();
    }

    public function assignEmail()
    {
        $newWorkData = [
            'subject' => 'New Assigned Work',
            'title' => 'Hello,',
            'body' => 'You\'ve got New Work',
            'workText' => 'Click here to see the work',
            'url' => url('/dashboard'),
            'thankyou' => 'Have a nice day. Enjoy your work!'
        ];

        // $admin = User::role('admin')->get();
        $admin = User::first();

        // $admin->notify(new NewWork($newWorkData));
        Notification::send($admin, new NewWork($newWorkData));
    }
}
