<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function cust()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }
    
    public function location()
    {
        return $this->hasOne(Location::class);
    }

    public function dropOffLocation()
    {
        return $this->hasOne(DropOffLocation::class);
    }

    public function schedule()
    {
        return $this->hasOne(Schedule::class);
    }

    public function assigns()
    {
        return $this->hasMany(WorkAssign::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function billing()
    {
        return $this->hasOne(Billing::class);
    }

    public function images()
    {
        return $this->hasMany(WorkImages::class);
    }

}
