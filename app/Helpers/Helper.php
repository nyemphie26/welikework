<?php

namespace App\Helpers;

use App\Models\Billing;
use App\Models\Work;

class Helper
{
    public static function generateWorkStatus($status)
    {
        if ($status == 'New') {
            return 'Waiting for Approval';
        }
        if ($status == 'Approved') {
            return 'Waiting for Scheduling';
        }
        if ($status == 'Ready') {
            return $status;
        }
        if ($status == 'Not Ready') {
            return 'Work Postponed';
        }
        if ($status == 'Finished') {
            return 'Work Done';
        }
        if ($status == 'Closed') {
            return 'Work Closed';
        }
        if ($status == 'Rejected') {
            return $status;
        }

    }

    public static function tax($fees)
    {
        $tax = $fees * 0.15;
        return $tax;
    }

    public static function grandTotal($workId)
    {
        $billing = Billing::where('work_id', $workId)->first();
        $grandTotal = $billing->total_fees + $billing->tax + $billing->tipping;

        return $grandTotal;
    }

    public static function generateReffNo($n)
    {
        for($r = ""; $n >= 0; $n = intval($n / 26)-1)
            $r = chr($n%26 + 0x41) . $r;
        
        $random_string = $r.time();
        
        return $random_string;
    }

    
}


?>