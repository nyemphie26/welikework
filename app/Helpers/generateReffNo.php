<?php

namespace App\Helpers;

use App\Models\Work;

class generateReffNo
{
    public function newReffNo($n)
    {
        for($r = ""; $n >= 0; $n = intval($n / 26)-1)
        $r = chr($n%26 + 0x41) . $r;

        $str = random_int(10000000,99999999);
        
        $random_string = $r.$str;

        if ($this->checkReffNo($random_string)) {
            $this->newReffNo($n);
        }
        
        return $random_string;
    }

    public function checkReffNo($number)
    {
        return Work::where('reff_no',$number)->exists();
    }
}