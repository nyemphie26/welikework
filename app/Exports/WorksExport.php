<?php

namespace App\Exports;

use App\Models\Work;
use App\Models\WorkAssign;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;

class WorksExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('auth.works.exportToday', [
            'works' => WorkAssign::where('user_id',Auth::id())->with(['work.schedule' => function ($query) 
                                            {
                                                $query->whereDate('sch_date', date('Y-m-d'));
                                            }
                                        ])
                                ->get()
                                ->where('work.status','Ready')
                                ->where('work.schedule','!=',null)
        ]);
    }
}
