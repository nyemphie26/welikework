<?php

namespace App\Exports;

use App\Models\Work;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class WorkExport implements FromView
{
    protected $start, $end;

    function __construct($start, $end) {
            $this->start = $start;
            $this->end   = $end;
    }

    public function view(): View
    {
        return view('auth.report.monthlyReport', [
            'works' => Work::with(['cust','schedule','service','billing'])->get()->where('status', 'Finished')->whereBetween('schedule.actual_stop',[$this->start, $this->end])
        ]);
    }
}
